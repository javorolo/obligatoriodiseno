﻿using System.Collections.Generic;
using School.Logic;

namespace IConnection {

    public interface IConnectionSubject {

        void AddSubject(Subject subject);

        void ModifySubject(Subject oldSubject, Subject changedSubject);

        void DeleteSubject(Subject subject);

        List<Subject> GetSubjectsList();

        bool IsInSubjectList(Subject oldSubject);

        bool IsEmptyTable();

        int NumberOfSubjectsWithSameName(string name);
        void deleteAll();
    }
}
