﻿using System.Collections.Generic;
using School.Logic;

namespace IConnection {

    public interface IConnectionStudent {

        void AddStudent(Student student);

        void ModifyStudent(Student oldStudent, Student changedStudent);

        void DeleteStudent(Student student);

        List<Student> GetStudentTable();

        bool IsInStudentTable(Student student);

        bool IsEmptyTable();

        void AddActivityToStudent(Student st, Activity ac);

        List<Activity> GetStudentActivities(Student student);
        void deleteAll();
    }
}
