﻿using School.Logic;

namespace IConnection {

    public interface IConnectionPayActivity {

        void AddPayActivity(PayActivity payActivity);

        int CountPays();

        void DeletePayActivity(int studentId);
    }
}
