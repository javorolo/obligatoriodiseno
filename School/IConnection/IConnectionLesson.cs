﻿using System.Collections.Generic;
using School.Logic;

namespace IConnection {

    public interface IConnectionLesson {

        void AddLesson(Lesson lesson);

        void ModifyLesson(Lesson oldLesson, Lesson changedLesson);

        void DeleteLesson(Lesson lesson);

        bool IsEmptyTable();

        List<Lesson> GetLessonList();

        bool IsInLessonTable(Lesson lesson);

        List<Teacher> GetTeachersLesson(Lesson lesson);

        List<Student> GetStudentsLesson(Lesson lesson);

        Subject GetSubjectLesson(Lesson lesson);

        List<Subject> GetSubjectsWithLesson();

        List<Subject> GetTeacherInAllSubjects(Teacher teacher);

        void deleteAll();

        List<Subject> GetStudentsInAllSubjects(Student student);
    }
}
