﻿using School.Logic;

namespace IConnection {

    public interface IConnectionPayFee {

        PayFee GetLastPayFee(Student student);

        void AddPay(PayFee payfee);
    }
}
