﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Logic;

namespace IConnection {

    public interface IConnectionTeacher {

        void AddTeacher(Teacher teacher);

        void ModifyTeacher(Teacher oldTeacher, Teacher changedTeacher);

        void DeleteTeacher(Teacher teacher);

        List<Teacher> GetTeacherTable();

        bool IsInTeacherTable(Teacher teacher);

        bool IsEmptyTable();
        void deleteAll();
    }
}
