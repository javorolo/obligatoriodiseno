﻿using System.Collections.Generic;
using School.Logic;

namespace IConnection {

    public interface IConnectionActivity {

        void AddActivity(Activity activity);

        bool AreSomeActivity();

        List<Activity> GetListOfActivities();

        void ModifyActivit(Activity oldActivity, Activity changedActivity);

        bool IsInActivityTable(Activity activity);

        void DeleteActivity(Activity activity);
        void deleteAll();
    }
}
