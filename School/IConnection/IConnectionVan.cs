﻿using School.Logic;
using System.Collections.Generic;

namespace IConnection {
    public interface IConnectionVan{

        void AddVan(Van van);

        List<Van> GetListOfVans();

        bool AreSomeVans();

        bool IsInVanTable(Van van);

        void ModifyVan(Van oldVan, Van changedVan);

        void DeleteVan(Van van);
        void deleteAll();
    }
}
