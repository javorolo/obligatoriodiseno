﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;

namespace UnitTestPerson {

    [TestClass]
    public class UnitTest1 {

        Activity activity;

        [TestMethod]
        public void TestCreateActivity() {
            activity = new Activity();
            Assert.IsNotNull(activity);
        }

        [TestMethod]
        public void TestGetAttributesActivity() {
            activity = new Activity();
            DateTime date = new DateTime(1900,1,1);
            Assert.AreEqual(activity.Name,"");
            Assert.AreEqual(activity.Id, 0);
            Assert.AreEqual(activity.Date, date);
            Assert.AreEqual(activity.Price, 0);
        }

        [TestMethod]
        public void TestSetAttributesActivity() {
            activity = preloadActivity();
            DateTime date = new DateTime(2017, 8, 8);
            Assert.AreEqual(activity.Name, "Football");
            Assert.AreEqual(activity.Id, 1);
            Assert.AreEqual(activity.Date, date);
            Assert.AreEqual(activity.Price, 0);
        }

        [TestMethod]
        public void TestToStringActivity() {
            activity = preloadActivity();
            string name = "Football";
            Assert.AreEqual(activity.ToString(), name);
        }

        [TestMethod]
        public void TestEqualsActivity() {
            activity = preloadActivity();
            Activity activityTwo = new Activity();
            //Assert.IsFalse(activity.equals(activityTwo));
        }

        [TestMethod]
        public void TestValidateName() {
            activity = preloadActivity();
            Assert.IsTrue(activity.CorrectName());
            activity.Name = "aa";
            Assert.IsFalse(activity.CorrectName());
        }

        [TestMethod]
        public void TestValidatePrice() {
            activity = preloadActivity();
            Assert.IsFalse(activity.CorrectPrice());
            activity.Price = -1;
            Assert.IsFalse(activity.CorrectPrice());
            activity.Price = 1;
            Assert.IsTrue(activity.CorrectPrice());
        }

        private Activity preloadActivity() {
            activity = new Activity();
            activity.Name = "Football";
            activity.Id = 1;
            DateTime date = new DateTime(2017, 8, 8);
            activity.Date = date;
            activity.Price = 0;
            return activity;
        }


    }
}
