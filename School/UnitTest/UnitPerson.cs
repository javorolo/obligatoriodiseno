﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using School.Logic;

namespace UnitTestLogic {

    [TestClass]
    public class UnitPerson {

        Person person = new Person();

        [TestMethod]
        public void TestGetAttributes(){
            Assert.AreEqual(person.Name, String.Empty);
            Assert.AreEqual(person.LastName, String.Empty);
        }

        [TestMethod]
        public void TestSetAttributes() {
            person = PreloadedPerson();
            Assert.AreEqual(person.Name, "Richard");
            Assert.AreEqual(person.LastName, "Monfort");
        }

        [TestMethod]
        public void TestCorrectName() {
            person = PreloadedPerson();
            Assert.IsTrue(person.CorrectName(person.Name));
            Assert.IsTrue(person.CorrectName(person.LastName));
        }

        [TestMethod]
        public void TestIncorrectName() {
            Assert.IsFalse(person.CorrectName(person.Name));
            Assert.IsFalse(person.CorrectName(person.LastName));
        }

        [TestMethod]
        public void TestShortName() {
            person.Name = "d";
            person.LastName = "a";
            Assert.IsFalse(person.CorrectName(person.Name));
            Assert.IsFalse(person.CorrectName(person.LastName));
        }

        [TestMethod]
        public void TestLongName() {
            person.Name = "Maximiliano Ernesto Evaristo I";
            person.LastName = "Eyherarchart Rivoir Carrión Mito";
            Assert.IsFalse(person.CorrectName(person.Name));
            Assert.IsFalse(person.CorrectName(person.LastName));
        }

        [TestMethod]
        public void TestNameWithNumbers() {
            person.Name = "3C";
            person.LastName = "4cH14";
            Assert.IsFalse(person.CorrectName(person.Name));
            Assert.IsFalse(person.CorrectName(person.LastName));
        }

        private Person PreloadedPerson(){
            person.Name = "Richard";
            person.LastName = "Monfort";
            return person;
        }

    }
}
