﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;
using System.Collections.Generic;

namespace UnitTestLogic {

    [TestClass]
    public class UnitTestSubjet {

        Lesson expected = new Lesson();
        Lesson aLesson = new Lesson();

        [TestMethod]
        public void TestGetAttributesSubjet() {
            Assert.AreEqual(expected.Id, 0);
        }

        [TestMethod]
        public void TestSetAttributesSubjet() {
            PreloadedSubjet();
            Assert.AreEqual(expected.Id, 1);
        }

        [TestMethod]
        public void TestEqualsSubjet() {
            PreloadedSubjet();
            PreloadedSubjetaLesson();
            Assert.IsTrue(expected.Equals(aLesson));
        }

        [TestMethod]
        public void TestEqualsSubjetTwo() {
            PreloadedSubjet();
            Lesson lesson  = null;
            Assert.IsFalse(expected.Equals(lesson));
        }

        [TestMethod]
        public void TestToStringSubjet() {
            PreloadedSubjet();
            Assert.AreEqual(expected.ToString(), "1");
        }

        private void PreloadedSubjet() {
            expected.Id = 1;
            expected.Students = new List<Student>();
            expected.Subject = new Subject();
            expected.Teachers = new List<Teacher>();    
        }

        private void PreloadedSubjetaLesson() {
            aLesson.Id = 1;
            aLesson.Students = new List<Student>();
            aLesson.Subject = new Subject();
            aLesson.Teachers = new List<Teacher>();
        }
        

        private List<Teacher> PreloadedListTeacher(int quantity) {
            List<Teacher> teachers = new List<Teacher>();
            for(int since = 1; since <= quantity; since++)
                teachers.Add(new Teacher());
            return teachers;
        }

        private List<Student> PreloadedListStudent(int quantity) {
            List<Student> students = new List<Student>();
            for (int since = 1; since <= quantity; since++)
                students.Add(new Student());
            return students;
        }


    }
}
