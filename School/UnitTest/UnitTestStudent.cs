﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;
using System.Collections.Generic;

namespace UnitTestLogic {


    [TestClass]
    public class UnitTestStudent {

        Student emptyStudent = new Student();
        Student student = new Student();
        Activity activity = new Activity();
        List<Activity> activities = new List<Activity>();

        [TestMethod]
        public void TestGetAttributesStudent() {
            Assert.AreEqual(emptyStudent.Name, String.Empty);
            Assert.AreEqual(emptyStudent.LastName, String.Empty);
            Assert.AreEqual(emptyStudent.Identification, String.Empty);
            Assert.AreEqual(emptyStudent.Activities.Count, 0);
        }

        [TestMethod]
        public void TestSetAttributesStudent() {
            PreloadedStudent();
            activities.Add(activity);
            student.Activities = activities;
            Assert.AreEqual(student.Name, "Richard");
            Assert.AreEqual(student.LastName, "Monfort");
            Assert.AreEqual(student.Identification, "4127627-8");
            Assert.AreEqual(student.Activities.Count, 1);
            Assert.IsTrue(student.Activities.Contains(activity));
        }

        [TestMethod]
        public void TestSetAttributesStudentTwo() {
            PreloadedStudent();
            Assert.AreEqual(student.Name, "Richard");
            Assert.AreEqual(student.LastName, "Monfort");
            Assert.AreEqual(student.Identification, "4127627-8");
            Assert.AreEqual(student.Activities.Count, 0);
        }

        [TestMethod]
        public void TestToString() {
            PreloadedStudent();
            string result = this.student.ToString();
            string expected = "Richard Monfort";
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void TestEquals() {
            PreloadedStudent();
            Student studentNull = null;           
            Assert.IsFalse(student.Equals(studentNull));
        }

        
        private void PreloadedStudent() {
            student.Identification = "4127627-8";
            student.Name = "Richard";
            student.LastName = "Monfort";
            this.activities = new List<Activity>();
        }


    }
}
