﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Controller;
using School.Logic;
using ConnectionDataBase;
using IConnection;

namespace UnitTestPerson {
    [TestClass]
    public class UnitTestControllerPayActivity {

        Student student { get;  set; }
        Activity activity { get; set; }
        IConnectionPayActivity connection;

        [TestInitialize()]
        public void Setup() {
            student = new Student();
            activity = new Activity();
        }

        [TestMethod]
        public void TestNewPayActivity() {
            Assert.IsNotNull(ControllerPayActivity.Instance);
        }

        [TestMethod]
        public void TestCreatePayActivity() {
            ControllerPayActivity controller = ControllerPayActivity.Instance;
            PayActivity payActivity = controller.CreatePayActivity(student, activity);
            Assert.IsNotNull(payActivity);
            Assert.IsTrue(payActivity.Student.Equals(student));
            Assert.IsTrue(payActivity.Activity.Equals(activity));
        }

        [TestMethod]
        public void TestAddPayActivity() {
            ControllerPayActivity controller = ControllerPayActivity.Instance;
            controller.AddPayActivity(student, activity);
            connection = new ConnectionPayActivity();
            int countinBD = connection.CountPays();
            Assert.AreEqual(1, countinBD);
        }

        [TestCleanup()]
        public void Cleanup() {

        }

    }
}
