﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;

namespace UnitTestPerson {
    [TestClass]
    public class UnitTestPayFee {

        private PayFee payFee { get; set; }
        private Student student { get; set; }

        [TestInitialize()]
        public void Initialize() {
            this.student = createStudent();
            this.payFee = createPayFee(student);
        }

        [TestMethod]
        public void TestGetAtributesPayFee() {
            Assert.AreEqual(payFee.Month, 1);
            Assert.AreEqual(payFee.Year, 1990);
            Assert.AreEqual(payFee.Student, student);
        }

        [TestMethod]
        public void TestSetAtributesPayFee() {
            modifyAtributes();
            Assert.AreEqual(payFee.Month, 8);
            Assert.AreEqual(payFee.Year, 2017);
        }

        private void modifyAtributes() {
            this.payFee.Month = 8;
            this.payFee.Year = 2017;
        }

        private PayFee createPayFee(Student student) {
            return new PayFee();
        } 

        private Student createStudent() {
            return new Student();
        }

    }
}
