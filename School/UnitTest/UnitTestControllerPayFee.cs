﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;
using School.Controller;
using IConnection;
using ConnectionDataBase;

namespace UnitTestPerson {
    [TestClass]
    public class UnitTestControllerPayFee {

        private ControllerPayFee controllerPayFee;

        IConnectionPayFee connection;

        [TestInitialize()]
        public void Initialize() {
            controllerPayFee = ControllerPayFee.Instance;
            connection = new ConnectionPayFee();
        }

        [TestMethod]
        public void TestCreateControllerPayFee() {
            Assert.IsNotNull(controllerPayFee);
        }

        [TestCleanup()]
        public void Cleanup() {
            
        }


    }
}
