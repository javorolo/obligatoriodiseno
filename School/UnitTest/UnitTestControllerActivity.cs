﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Controller;
using School.Logic;
using ConnectionDataBase;
using System.Collections.Generic;
using IConnection;

namespace UnitTestPerson {

    [TestClass]
    public class UnitTestControllerActivity {

        Activity activity;
        Activity activityTwo;
        IConnectionActivity connection;

        [TestInitialize()]
        public void Setup() {
            activity = preloadActivity();
            activityTwo = preloadActivityTwo();
        }

        [TestMethod]
        public void TestCreateActivityController() {
            ControllerActivity cActivity = ControllerActivity.Instance;
            Assert.IsNotNull(cActivity);
        }
        
        [TestMethod]
        public void TestAddActivityToList() {
            ControllerActivity.Instance.AddActivity(activity);
            Assert.AreEqual(ControllerActivity.Instance.GetActivitiesList().Count, 1);
            Assert.IsTrue(ControllerActivity.Instance.IsInActivityTable(activity));
        }

        [TestMethod]
        public void TestDeleteActivityToList() {
            ControllerActivity.Instance.AddActivity(activity);
            ControllerActivity.Instance.DeleteActivity(activityTwo);
            Assert.AreEqual(ControllerActivity.Instance.GetActivitiesList().Count, 1);
            Assert.IsFalse(ControllerActivity.Instance.IsInActivityTable(activityTwo));
        }

        [TestMethod]
        public void TestModifyActivityToList() {
            Activity activity = preloadActivity();
            ControllerActivity.Instance.AddActivity(activity);
            Activity other = preloadActivityTwo();
            ControllerActivity.Instance.ModifyActivity(activity, other);
            List<Activity> listActivities = connection.GetListOfActivities();
            Activity uniqueActivity = listActivities[0];            
            Assert.IsTrue(uniqueActivity.Name.Equals(other.Name));
            Assert.IsTrue(uniqueActivity.Price == other.Price);
            Assert.IsTrue(uniqueActivity.Date.Equals(other.Date));
        }

        [TestMethod]
        public void TestValidateActivity() {
            Activity activity = preloadActivity();
            activity.Name = "aa";
            Assert.IsFalse(ControllerActivity.Instance.IsOkNameFormat(activity.Name));
        }

        [TestMethod]
        public void activityWithIncorrectPrice() {
            Activity activity = preloadActivity();
            activity.Price = -1;
            Assert.IsFalse(ControllerActivity.Instance.IsCorrectPrice(activity.Price));
        }

        [TestMethod]
        public void activityWithCorrectPrice() {
            Activity activity = preloadActivity();
            activity.Price = 1;
            Assert.IsTrue(ControllerActivity.Instance.IsCorrectPrice(activity.Price));
        }

        [TestMethod]
        public void activityWithCorrectName() {
            Activity activity = preloadActivity();
            Assert.IsTrue(ControllerActivity.Instance.IsOkNameFormat(activity.Name));
        }

        [TestMethod]
        public void NewActivity() {
            Activity activity = preloadActivity();
            Activity second = ControllerActivity.Instance.NewActivity("Futbol", 100, new DateTime(2018, 1, 1));
            Assert.AreEqual(second.Name, "Futbol");
        }

        [TestMethod]
        public void IsEmptyList() {
            Assert.IsTrue(ControllerActivity.Instance.IsEmptyActivities());
        }

        [TestCleanup()]
        public void Cleanup() {
            connection = new ConnectionActivity();
            connection.deleteAll();
        }

        private static Activity preloadActivity() {
            Activity activity = new Activity();
            activity.Name = "Football";
            activity.Id = 1;
            DateTime date = new DateTime(2017, 8, 8);
            activity.Date = date;
            activity.Price = 1;
            return activity;
        }

        private static Activity preloadActivityTwo() {
            Activity activity = new Activity();
            activity.Name = "Basketball";
            activity.Id = 2;
            DateTime date = new DateTime(2017, 9,9);
            activity.Date = date;
            activity.Price = 100;
            return activity;
        }
    }
}
