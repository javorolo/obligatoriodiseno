﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;
using School.Controller;
using ConnectionDataBase;
using IConnection;

namespace UnitTestPerson {

    [TestClass]
    public class UnitTestControllerLesson {

        ControllerLesson cLesson = ControllerLesson.Instance;
        ControllerTeacher cTeacher = ControllerTeacher.Instance;
        ControllerStudent cStudent = ControllerStudent.Instance;
        ControllerSubject cSubject = ControllerSubject.Instance;
        IConnectionLesson connection;

        [TestInitialize()]
        public void Setup() {

        }

        [TestMethod]
        public void TestAddLessonInList() {
            Lesson lesson = preLoadedLesson();
            cLesson.AddLesson(lesson);
            Assert.IsTrue(cLesson.IsInLessonTable(lesson));
        }

        [TestMethod]
        public void TestTryAddSameLessonInList() {
            Lesson lesson = preLoadedLesson();
            Lesson sameSubject = preLoadedLesson();
            cLesson.AddLesson(lesson);
            cLesson.AddLesson(sameSubject);
            Assert.IsTrue(cLesson.GetLessonList().Count == 1);
        }

        [TestMethod]
        public void TestDeleteLessonFromList() {
            Lesson lesson = preLoadedLesson();
            cLesson.AddLesson(lesson);
            cLesson.DeleteLesson(lesson);
            Assert.IsFalse(cLesson.IsInLessonTable(lesson));
        }

        [TestMethod]
        public void TestLessonWithoutSubject() {
            Lesson lesson = preLoadedLesson();
            Assert.IsTrue(cLesson.GetSubjectLesson(lesson) == null);
        }

        [TestMethod]
        public void TestLessonWithSubject() {
            Lesson lesson = preLoadedLesson();
            lesson.Subject = cSubject.NewSubject("Maths");
            Assert.IsTrue(cLesson.GetSubjectLesson(lesson) != null);
        }

        [TestMethod]
        public void TestLessonWithoutTeacher() {
            Lesson lesson = preLoadedLesson();
            Assert.IsFalse(cLesson.GetTeachersLesson(lesson).Count > 0);
        }

        [TestMethod]
        public void TestLessonWithTeacher() {
            Lesson lesson = preLoadedLesson();
            Teacher teacher = cTeacher.NewTeacher("Oscar", "Tabarez");
            lesson.Teachers.Add(teacher);
            Assert.IsTrue(cLesson.GetTeachersLesson(lesson).Count > 0);
        }

        [TestMethod]
        public void TestLessonWithoutStudent() {
            Lesson lesson = preLoadedLesson();
            ControllerLesson.Instance.AddLesson(lesson);
            Assert.IsFalse(cLesson.GetStudentsLesson(lesson).Count == 0);
        }

        [TestMethod]
        public void TestLessonWithStudent() {
            Lesson lesson = preLoadedLesson();
            Student student = cStudent.NewStudent("Luis", "Suarez", "123");
            lesson.Students.Add(student);
            Assert.IsTrue(cLesson.GetStudentsLesson(lesson).Count > 0);
        }

        [TestCleanup()]
        public void Cleanup() {
            connection = new ConnectionLesson();
            connection.deleteAll();
        }

        private Lesson preLoadedLesson() {
            Lesson lesson = cLesson.NewLesson();
            return lesson;
        }

        private Lesson cloneSubject() {
            Lesson subject = cLesson.NewLesson();
            return subject;
        }

    }

}
