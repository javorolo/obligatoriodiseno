﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;

namespace UnitTestPerson {

    [TestClass]
    public class UnitTestLocation {

        Location expected = new Location();
        Location result = new Location();

        [TestMethod]
        public void TestGetCartesianXWithoutSetting() {
            Assert.AreEqual(expected.CartesianX, 0);
        }

        [TestMethod]
        public void TestGetCartesianX() {
            setLocationExpected();
            Assert.AreEqual(expected.CartesianX, 5);
        }

        [TestMethod]
        public void TestGetCartesianYWithoutSetting() {
            Assert.AreEqual(expected.CartesianY, 0);
        }

        [TestMethod]
        public void TestGetCartesianY() {
            setLocationExpected();
            Assert.AreEqual(expected.CartesianY, -3);
        }

        [TestMethod]
        public void TestEqualsLocations() {
            setLocationExpected();
            setLocationResult();
            this.result.CartesianX = -10;
            Assert.AreNotEqual(expected, result);
        }

        [TestMethod]
        public void TestEqualsLocationsTwo() {
            setLocationExpected();
            setLocationResult();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestGetHashCode() {
            setLocationExpected();
            int expected = this.expected.CartesianX;
            int result = this.expected.GetHashCode();
            Assert.AreEqual(expected, result);
        }

        private void setLocationExpected() {
            expected.CartesianX = 5;
            expected.CartesianY = -3;
        }

        private void setLocationResult() {
            result.CartesianX = 5;
            result.CartesianY = -3;
        }


    }
}
