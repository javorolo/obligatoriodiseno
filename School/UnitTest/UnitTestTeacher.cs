﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;
using System.Collections.Generic;
using ConnectionDataBase;
using IConnection;

namespace UnitTestLogic {
    [TestClass]
    public class UnitTestTeacher {

        List<Teacher> teachers = new List<Teacher>();
        IConnectionTeacher connection;

        [TestInitialize()]
        public void Setup() {

        }

        [TestMethod]
        public void TestSetAttributesTeacher() {
            Teacher expected = setTeacherExpected();
            Assert.AreEqual(expected.Name, "Javier");
            Assert.AreEqual(expected.LastName, "Enrriquez");
        }

        [TestMethod]
        public void TestNumberOfId() {
            Teacher expected = setTeacherExpected();
            Assert.AreEqual(expected.Id, 0);
        }

        [TestMethod]
        public void TestToString() {
            setTeacherExpected();
            Teacher teacher = setTeacherExpected();
            string retult = teacher.ToString();
            string expected = "Javier Enrriquez";
            Assert.AreEqual(expected, retult);
        }

        [TestMethod]
        public void TestEquals() {
            Teacher teacher = setTeacherExpected();
            Teacher teacherNull = null;
            Assert.IsFalse(teacher.Equals(teacherNull));
        }

        [TestMethod]
        public void TestEqualsTwo() {
            Teacher teacher = setTeacherExpected();
            Teacher teacherTwo = setTeacherExpected(); ;
            Assert.IsTrue(teacher.Equals(teacherTwo));
        }


        [TestCleanup()]
        public void Cleanup() {
            connection = new ConnectionTeacher();
            connection.deleteAll();
        }

        private Teacher setTeacherExpected() {
            Teacher expected = new Teacher();
            expected.Name = "Javier";
            expected.LastName = "Enrriquez";
            return expected;
        }
     


    }
}
