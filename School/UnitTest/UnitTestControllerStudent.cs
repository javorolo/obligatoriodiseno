﻿using School.Controller;
using School.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConnectionDataBase;
using IConnection;

namespace UnitTestController {

    [TestClass]
    public class UnitTestControllerStudent {

        ControllerStudent controllerStudent = ControllerStudent.Instance;

        IConnectionStudent connection;

        [TestInitialize()]
        public void Setup() {

        }

        [TestMethod]
        public void TestCreateController() {
            Assert.AreEqual(controllerStudent.GetStudentTable().Count, 0);
            Assert.IsNotNull(controllerStudent);
        }

        [TestMethod]
        public void TestCreateStudent() {
            Student student = preloadStudent();
            controllerStudent.AddStudent(student);
            Assert.IsTrue(controllerStudent.IsInStudentTable(student));
        }

        [TestMethod]
        public void TestDeleteStudent() {
            Student student = preloadStudent();
            controllerStudent.AddStudent(student);
            controllerStudent.DeleteStudent(preloadStudent());
            Assert.IsFalse(controllerStudent.IsInStudentTable(preloadStudent()));
        }

        [TestMethod]
        public void TestModifyStudent() {
            Student student = preloadStudent();
            Student other = otherStudent();
            controllerStudent.AddStudent(student);
            controllerStudent.ModifyStudent(student, other);
            Assert.IsTrue(controllerStudent.IsInStudentTable(other));
        }

        [TestMethod]
        public void TestIdentificationFormatOk() {
            Student student = preloadStudent();
            Assert.IsTrue(controllerStudent.IsIdentificationFormatOk(student));
        }

        [TestMethod]
        public void TestIdentificationWithoutString() {
            Student student = preloadStudent();
            student.Identification = "";
            Assert.IsFalse(controllerStudent.IsIdentificationFormatOk(student));
        }

        [TestMethod]
        public void TestIdentificationWithSameCountOfChars() {
            Student student = preloadStudent();
            student.Identification = "123456789";
            Assert.IsFalse(controllerStudent.IsIdentificationFormatOk(student));
        }

        [TestMethod]
        public void TestIdentificationWithLetters() {
            Student student = preloadStudent();
            student.Identification = "ABCDEFGH-I";
            Assert.IsFalse(controllerStudent.IsIdentificationFormatOk(student));
        }

        [TestMethod]
        public void TestIdentificationChangedDash() {
            Student student = preloadStudent();
            student.Identification = "1234567/8";
            Assert.IsFalse(controllerStudent.IsIdentificationFormatOk(student));
        }

        [TestMethod]
        public void TestNumberFirstStudent() {
            Student student = preloadStudent();
            controllerStudent.AddStudent(student);
            Assert.IsTrue(student.StudentId > 0);
        }

        [TestMethod]
        public void TestNumberSecondStudent() {
            Student student = preloadStudent();
            Student other = otherStudent();
            controllerStudent.AddStudent(student);
            controllerStudent.AddStudent(other);
            Assert.AreEqual(other.StudentId, (student.StudentId +1));
        }

        [TestMethod]
        public void TestNumberSecondStudentAfterDeleteFirst() {
            Student student = preloadStudent();
            Student other = otherStudent();
            controllerStudent.AddStudent(student);
            int idStudent = student.StudentId;
            controllerStudent.DeleteStudent(student);
            controllerStudent.AddStudent(other);
            Assert.AreEqual(other.StudentId, (idStudent + 1));
        }

        [TestMethod]
        public void TestCorrectStudentLocation() {
            Student student = preloadStudent();   
            Assert.IsTrue(controllerStudent.CorrectStudentLocation(student));
        }

        [TestMethod]
        public void TestNegativeStudentLocation() {
            Student student = preloadStudent();
            student.Location.CartesianX = -10;
            Assert.IsTrue(controllerStudent.CorrectStudentLocation(student));
        }

        [TestMethod]
        public void TestOutOfBoundsLocationXNegative() {
            Student student = preloadStudent();
            student.Location.CartesianX = -101;
            Assert.IsFalse(controllerStudent.CorrectStudentLocation(student));
        }

        [TestMethod]
        public void TestOutOfBoundsLocationYNegative() {
            Student student = preloadStudent();
            student.Location.CartesianY = -101;
            Assert.IsFalse(controllerStudent.CorrectStudentLocation(student));
        }

        [TestMethod]
        public void TestOutOfBoundsLocationXPositive() {
            Student student = preloadStudent();
            student.Location.CartesianX = 101;
            Assert.IsFalse(controllerStudent.CorrectStudentLocation(student));
        }

        [TestMethod]
        public void TestOutOfBoundsLocationYPositive() {
            Student student = preloadStudent();
            student.Location.CartesianY = 101;
            Assert.IsFalse(controllerStudent.CorrectStudentLocation(student));
        }

        [TestMethod]
        public void TestLivesInTheSchool() {
            Student student = preloadStudent();
            student.Location.CartesianX = 0;
            student.Location.CartesianY = 0;
            Assert.IsFalse(controllerStudent.CorrectStudentLocation(student));
        }

        [TestMethod]
        public void TestCorrectName() {
            Student student = preloadStudent();            
            Assert.IsTrue(student.CorrectName(student.Name));
            Assert.IsTrue(student.CorrectName(student.LastName));
        }
        [TestMethod]
        public void TestAddActivityToStudent() {
            Student student = preloadStudent();
            Activity activity = new Activity();
            controllerStudent.AddActivityToStudent(student, activity);
            Student studentToBD = controllerStudent.GetStudentTable()[0];
            Assert.IsTrue(studentToBD.Activities.Contains(activity));
        }

        [TestMethod]
        public void TestAddTwoActivitiesToStudent() {
            Student student = preloadStudent();
            Activity activity = new Activity();
            activity.Id = 3;
            Activity activityTwo = new Activity();
            activityTwo.Id = 3;
            controllerStudent.AddActivityToStudent(student, activity);
            controllerStudent.AddActivityToStudent(student, activityTwo);
            Assert.IsTrue(student.Activities.Count == 1);
        }

        [TestCleanup()]
        public void Cleanup() {
            connection = new ConnectionStudent();
            connection.deleteAll();
        }

        private Student preloadStudent() {
            Student student = controllerStudent.NewStudent("Javier", "Enríquez", "4127627-8");
            student.Location.CartesianX = 5;
            student.Location.CartesianY = -4;
            return student;
        }    

        private Student otherStudent() {
            Student other = controllerStudent.NewStudent("Richard", "Monfort", "4556487-9");
            other.Location.CartesianX = 0;
            other.Location.CartesianY = 8;
            return other;
        }

    }
}
