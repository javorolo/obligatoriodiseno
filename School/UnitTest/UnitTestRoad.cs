﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Controller;
using School.Logic;
using System.Collections.Generic;
using System.Data;
using ConnectionDataBase;
using IConnection;

namespace UnitTestPerson {

    [TestClass]
    public class UnitTestRoad {

        static ControllerVan controllerVan= loadVans();
        static ControllerStudent controllerStudent = LoadStudents();
        Location schoolLocation = new Location();
        static ControllerRoad road = ControllerRoad.Instance;

        IConnectionVan connectionVan;
        IConnectionStudent connectionStudent;

        [TestInitialize()]
        public void Setup() {
            connectionStudent = new ConnectionStudent();
            connectionStudent.deleteAll();
            connectionVan = new ConnectionVan();
            connectionVan.deleteAll();
            LoadStudents();
            loadVans();
        }

        [TestMethod]
        public void TestShortesRoute() {
            DataTable shortestRouteResult = road.ShortestRoute();
            string expected = shortestRouteResult.ToString();
            Assert.IsNotNull(shortestRouteResult);
            Assert.AreEqual(shortestRouteResult.Rows.Count,2);
        }  

        private static ControllerStudent LoadStudents() {
            ControllerStudent controllerStudent = ControllerStudent.Instance;
            Student one = controllerStudent.NewStudent("Javier", "Enriquez", "4127627-8");
            one.Location.CartesianX = 1;
            one.Location.CartesianY = 5;
            Student two = controllerStudent.NewStudent("Richard", "Monfort", "4555555-5");
            two.Location.CartesianX = 1;
            two.Location.CartesianY = 6;
            Student tres = controllerStudent.NewStudent("Marcos", "Carrión", "1234567-5");
            tres.Location.CartesianX = 1;
            tres.Location.CartesianY = 7;
            controllerStudent.AddStudent(one);
            controllerStudent.AddStudent(two);
            controllerStudent.AddStudent(tres);
            return controllerStudent;
        }

        private static ControllerVan loadVans() {
            ControllerVan controllerVan = ControllerVan.Instance;
            Van van = controllerVan.NewVan("SAA1234", 2,5);
            Van vanTwo = controllerVan.NewVan("SAA1345", 1,2);
            controllerVan.AddVan(van);
            controllerVan.AddVan(vanTwo);
            return controllerVan;
        }

        [TestCleanup()]
        public void Cleanup() {
            connectionStudent = new ConnectionStudent();
            connectionStudent.deleteAll();
            connectionVan = new ConnectionVan();
            connectionVan.deleteAll();
        }

    }
}
