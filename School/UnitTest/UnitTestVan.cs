﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;
using School.Controller;

namespace UnitTestPerson {

    [TestClass]
    public class UnitVan {

        Van van = new Van();
        static Van expected = PreloadedVan("SAA1234", 5, 10);
        string result = "SAA1234";
        int resultCapacity = 5;

        [TestMethod]
        public void TestMethodGetCapacity() {
            Assert.AreEqual(expected.Capacity, resultCapacity);
        }

        [TestMethod]
        public void TestMethodGetCapacityNotEquals() {
            int result = 10;
            Assert.AreNotEqual(expected.Capacity, result);
        }

        [TestMethod]
        public void TestMethodGetConsumption() {
            int result = 10;
            Assert.AreEqual(expected.Consumption, result);
        }


        [TestMethod]
        public void TestMethodGetBadge() {
            Assert.AreEqual(expected.Badge, result);
        }

        [TestMethod]
        public void TestMethodGetBadgeNotEquals() {
            Assert.AreNotEqual(expected.Badge, van.Badge);
        }

        [TestMethod]
        public void TestMethodEquals() {
            van.Badge = "SAA1234";
            Assert.AreEqual(expected, van);
        }
        
        [TestMethod]
        public void TestMethodNotEquals() {
            van.Badge = "SAB1234";
            Assert.AreNotEqual(expected, van);
        }

        [TestMethod]
        public void TestToString() {
            Van van = PreloadedVan("SBG1818",5,10);
            String expected = "SBG1818";
            String result = van.ToString();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestEquals() {
            Van van = PreloadedVan("SBG1818", 5,10);
            Van vanNull = null;
            Assert.IsFalse(van.Equals(vanNull));
        }

        private static Van PreloadedVan(string badge, int capacity, int consumption) {
            Van van = new Van();
            van.Capacity = capacity;
            van.Badge = badge;
            van.Consumption = consumption;
            return van;
        }
    }
}
