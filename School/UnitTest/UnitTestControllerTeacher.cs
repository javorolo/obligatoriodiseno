﻿using ConnectionDataBase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Controller;
using School.Logic;
using IConnection;

namespace UnitTestPerson {
    [TestClass]
    public class UnitTestControllerTeacher {

        ControllerTeacher controller = ControllerTeacher.Instance;
        IConnectionTeacher connection;


        [TestInitialize()]
        public void Setup() {
            
        }

        [TestMethod]
        public void TestCreateController() {
            Assert.IsTrue(controller.IsEmptyTable());
            Assert.IsNotNull(controller);
        }

        [TestMethod]
        public void TestNewTeacher() {
            Teacher teacher = preloadTeacher();
            Assert.AreEqual(teacher.Name, "Charles");
            Assert.AreEqual(teacher.LastName, "Xavier");
            Assert.IsNotNull(teacher);
        }

        [TestMethod]
        public void TestAddTeacherToTheList() {
            Teacher teacher = preloadTeacher();
            controller.AddTeacher(teacher);
            Assert.IsTrue(controller.IsInTeacherTable(teacher));
        }

        [TestMethod]
        public void TestModifyNameTeacher() {
            Teacher oldTeacher = preloadTeacher();
            string oldName = oldTeacher.Name;
            Teacher newTeacher = preloadOtherTheacher();
            controller.AddTeacher(oldTeacher);
            controller.ModifyTeacher(oldTeacher, newTeacher);
            Assert.IsTrue(controller.GetTeacherTable().Count == 1);
            Assert.AreNotEqual(newTeacher.Name, oldName);
        }

        [TestMethod]
        public void TestDeleteTeacher() {
            Teacher teacher = preloadTeacher();
            controller.AddTeacher(teacher);
            controller.DeleteTeacher(teacher);
            Assert.IsFalse(controller.IsInTeacherTable(teacher));
        }

        [TestMethod]
        public void TestNameIsOk() {
            Teacher teacher = preloadTeacher();
            Assert.IsTrue(controller.CorrectName(teacher));
        }

        [TestMethod]
        public void TestWrongName() {
            Teacher teacher = preloadTeacher();
            teacher.Name = "a";
            Assert.IsFalse(controller.CorrectName(teacher));
        }

        [TestMethod]
        public void TestWrongNameWithNumbers() {
            Teacher teacher = preloadTeacher();
            teacher.Name = "3ST0ES";
            controller.AddTeacher(teacher);
            Assert.IsFalse(controller.CorrectName(teacher));
        }

        [TestCleanup()]
        public void Cleanup() {
            connection = new ConnectionTeacher();
            connection.deleteAll();
        }

        private Teacher preloadTeacher() {
            Teacher teacher = controller.NewTeacher("Charles", "Xavier");
            return teacher;
        }

        private Teacher preloadOtherTheacher() {
            Teacher other = controller.NewTeacher("Oscar Washington", "Tabarez");
            return other;
        }
        
    }
}
