﻿using ConnectionDataBase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Controller;
using School.Logic;
using IConnection;

namespace UnitTestPerson {

    [TestClass]
    public class UnitTestControllerSubject {

        ControllerSubject controller = ControllerSubject.Instance;

        IConnectionSubject connection;

        [TestInitialize()]
        public void Setup() {

        }

        [TestMethod]
        public void TestAddSubjectInTable() {
            Subject subject = preLoadedSubject();
            controller.AddSubject(subject);
            Assert.IsTrue(controller.IsInSubjectTable(subject));
        }

        [TestMethod]
        public void TestTryAddSameSubjectInList() {
            Subject subject = preLoadedSubject();
            Subject sameSubject = preLoadedSubject();
            controller.AddSubject(subject);
            controller.AddSubject(sameSubject);
            Assert.IsTrue(controller.GetSubjectTable().Count == 1);
        }

        [TestMethod]
        public void TestDeleteSubjectFromList() {
            Subject subject = preLoadedSubject();
            controller.AddSubject(subject);
            controller.DeleteSubject(subject);
            Assert.IsFalse(controller.IsInSubjectTable(subject));
        }

        [TestMethod]
        public void TestModifySubject() {
            Subject subject = preLoadedSubject();
            controller.AddSubject(subject);
            controller.ModifySubject(subject, cloneSubject());
            Assert.AreEqual(subject.Name, "Maths");
            Assert.AreEqual(subject.Code, "MAT1");
        }

        [TestMethod]
        public void TestNameCharsOk() {
            Subject subject = preLoadedSubject();
            Assert.IsTrue(controller.ValidNumberOfCharsInName(subject));
        }

        [TestMethod]
        public void TestNameOverMaximumChars() {
            Subject subject = preLoadedSubject();
            subject.Name = "I need write a long string";
            Assert.IsFalse(controller.ValidNumberOfCharsInName(subject));
        }

        [TestMethod]
        public void TestNameUnderMinimumChars() {
            Subject subject = preLoadedSubject();
            subject.Name = "";
            Assert.IsFalse(controller.ValidNumberOfCharsInName(subject));
        }

        [TestMethod]
        public void TestCodeWith4Chars() {
            Subject subject = preLoadedSubject();
            Assert.IsFalse(controller.Have4Chars(subject));
        }

        [TestMethod]
        public void TestCodeWith3Chars() {
            Subject subject = preLoadedSubject();
            subject.Code = "MAT";
            Assert.IsTrue(controller.Have4Chars(subject));
        }

        [TestMethod]
        public void TestCodeCorrectFormat() {
            Subject subject = preLoadedSubject();
            Assert.IsTrue(controller.CorrectCodeFormat(subject));
        }

        [TestMethod]
        public void TestCodeIncorrectFormat() {
            Subject subject = preLoadedSubject();
            subject.Code = "1MAT";
            Assert.IsFalse(controller.CorrectCodeFormat(subject));
        }

        [TestMethod]
        public void TestCodeEmpty() {
            Subject subject = preLoadedSubject();
            subject.Code = "";
            Assert.IsFalse(controller.CorrectCodeFormat(subject));
        }

        [TestMethod]
        public void TestGenerateCode() {
            Subject subject = preLoadedSubject();
            subject.Code = "";
            string generatedCode = controller.CodeGenerator(subject.Name);
            subject.Code = generatedCode;
            Assert.AreEqual(subject.Code, "His1");
        }

        [TestMethod]
        public void TestGenerateCodeWithSameName() {
            Subject subject = preLoadedSubject();
            subject.Code = "";
            controller.AddSubject(subject);
            Subject other = preLoadedSubject();
            other.Code = "";
            controller.AddSubject(other);
            string generatedCode = controller.CodeGenerator(subject.Name);
            subject.Code = generatedCode;
            Assert.AreEqual(subject.Code, "His3");
        }

        [TestCleanup()]
        public void Cleanup() {
            connection = new ConnectionSubject();
            connection.deleteAll();
        }

        private Subject preLoadedSubject() {
            Subject subject = controller.NewSubject("History");
            return subject;
        }

        private Subject cloneSubject() {
            Subject subject = controller.NewSubject("Maths");
            return subject;
        }
    }
}
