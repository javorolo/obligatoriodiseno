﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;

namespace UnitTestPerson {
    [TestClass]
    public class UnitTestPayment {

        private Payment payment { get; set; }
        private Student student { get; set; }

        [TestInitialize()]
        public void Initialize() {
            student = createStudent();
            payment = createPayment(student);
        }

        [TestMethod]
        public void TestbuilderPayment() {
            Assert.IsNotNull(this.payment);
            Assert.IsNotNull(this.payment.Student);
        }



        private Student createStudent() {
            return new Student();
        }

        private Payment createPayment(Student student) {
            return new Payment();
        }
    }
}
