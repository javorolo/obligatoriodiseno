﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;

namespace UnitTestPerson {
    [TestClass]
    public class UnitTestPayActivity {

        private PayActivity payActivity {get; set;}

        [TestMethod]
        public void TestBuilderPayActivity() {
            payActivity = new PayActivity();
            Assert.IsNotNull(this.payActivity.Student);
            Assert.IsNotNull(this.payActivity.Activity);
        }



    }
}
