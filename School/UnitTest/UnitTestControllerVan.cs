﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;
using School.Controller;
using ConnectionDataBase;
using IConnection;

namespace UnitTestPerson {

    [TestClass]
    public class UnitTestControllerVan {

        ControllerVan controller = ControllerVan.Instance;
        IConnectionVan connection;

        [TestInitialize()]
        public void Setup() {

        }

        [TestMethod]
        public void TestAddVanInList() {
            controller.AddVan(preloadVan());         
            Assert.IsTrue(controller.IsInVanTable(preloadVan()));
        }

        [TestMethod]
        public void TestNewVanEquals() {
            Assert.AreEqual(cloneVan(), preloadVan());
        }

        [TestMethod]
        public void TestNewVanNotEquals() {
            Van result = cloneVan();
            result.Badge = "MAA2314";
            Assert.AreNotEqual(preloadVan(), result);
        }

        [TestMethod]
        public void TestModifyBadge() {
            Van expected = preloadVan();
            controller.AddVan(expected);
            Van result = preloadVan();
            result.Badge = "MAA1234";
            controller.ModifyVan(expected, result);
            Assert.AreEqual(expected.Badge, result.Badge);
        }

        [TestMethod]
        public void TestDeleteVan() {
            Van result = cloneVan();
            controller.AddVan(preloadVan());
            controller.AddVan(result);
            controller.DeleteVan(result);
            Assert.AreEqual(controller.GetListOfVans().Count, 1);
        }

        [TestMethod]
        public void TestCapacityOver0() {
            Van expected = cloneVan();
            expected.Capacity = 1;
            Assert.IsTrue(controller.CapacityIsOk(expected));
        }

        [TestMethod]
        public void TestCapacityUnderEqual50() {
            Van expected = cloneVan();
            expected.Capacity = 50;
            Assert.IsTrue(controller.CapacityIsOk(expected));
        }

        [TestMethod]
        public void TestCapacityBetween1And50() {
            Van expected = cloneVan();
            expected.Capacity = 20;
            Assert.IsTrue(controller.CapacityIsOk(expected));
        }

        [TestMethod]
        public void TestCapacityOver50() {
            Van expected = cloneVan();
            expected.Capacity = 51;
            Assert.IsFalse(controller.CapacityIsOk(expected));
        }

        [TestMethod]
        public void TestCapacityUnder1() {
            Van expected = cloneVan();
            expected.Capacity = -5;
            Assert.IsFalse(controller.CapacityIsOk(expected));
        }

        [TestMethod]
        public void TestBadgeSevenChars() {
            Van expected = preloadVan();
            Assert.IsTrue(controller.IsOKBadgeFormat(expected));
        }

        [TestMethod]
        public void TestBadgeEigthChars() {
            Van expected = preloadVan();
            expected.Badge = "SAA12345";
            Assert.IsFalse(controller.IsOKBadgeFormat(expected));
        }

        [TestMethod]
        public void TestBadgeCorrectFormat() {
            Van expected = preloadVan();
            Assert.IsTrue(controller.IsOKBadgeFormat(expected));
        }

        [TestMethod]
        public void TestBadgeIncorrectFormat() {
            Van expected = preloadVan();
            expected.Badge = "1234SAB";
            Assert.IsFalse(controller.IsOKBadgeFormat(expected));
        }

        [TestMethod]
        public void TestBadgeWithoutChars() {
            Van expected = preloadVan();
            expected.Badge = "";
            Assert.IsFalse(controller.IsOKBadgeFormat(expected));
        }

        [TestMethod]
        public void TestIsEmprty() {
            Assert.IsTrue(controller.IsEmptyTable());
        }

        [TestCleanup()]
        public void Cleanup() {
            connection = new ConnectionVan();
            connection.deleteAll();
        }

        private Van preloadVan() {
            Van van = controller.NewVan("SAB1234", 10, 15);
            return van;
        }

        private Van cloneVan() {
            Van van = new Van();
            van.Badge = "SAB1234";
            van.Capacity = 10;
            return van;
        }     
        
    }
}
