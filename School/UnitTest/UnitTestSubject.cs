﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using School.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestPerson {

    [TestClass]
    public class UnitTestSubject {

        Subject expected = new Subject();

        [TestMethod]
        public void TestGetAttributesSubjet() {
            Assert.AreEqual(expected.Name, String.Empty);
            Assert.AreEqual(expected.Code, String.Empty);
        }

        [TestMethod]
        public void TestSetAttributesSubjet() {
            PreloadedSubjet();
            Assert.AreEqual(expected.Name, "Literature");
            Assert.AreEqual(expected.Code, "LIT1");
        }

        [TestMethod]
        public void TestSameSubject() {
            Subject same = new Subject();
            same.Code = "LIT1";
            same.Name = "History";
            PreloadedSubjet();
            Assert.AreEqual(expected, same);
        }

        [TestMethod]
        public void TestEqualsSubject() {
            Subject same = new Subject();
            same.Code = "LIT1";
            same.Name = "Literature";
            PreloadedSubjet();
            Assert.IsTrue(expected.Equals(same));
        }

        [TestMethod]
        public void TestToStringSubject() {
            PreloadedSubjet();
            Assert.AreEqual(expected.ToString(),"Literature");
        }

        private void PreloadedSubjet() {
            expected.Name = "Literature";
            expected.Code = "LIT1";
        }

    }
}
