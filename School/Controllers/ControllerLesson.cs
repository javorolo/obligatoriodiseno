﻿using System.Collections.Generic;
using School.Logic;
using ConnectionDataBase;
using System;
using School.IControllers;
using IConnection;

namespace School.Controller {

    public class ControllerLesson : IControllerLesson {

        IConnectionLesson Connection = new ConnectionLesson();
        private static ControllerLesson instance = null;
        private static readonly object padlock = new object();

        private ControllerLesson() {

        }

        public static ControllerLesson Instance {
            get {
                lock (padlock) {
                    if (instance == null) {
                        instance = new ControllerLesson();
                    }
                    return instance;
                }
            }
        }

        public Lesson NewLesson() {
            return new Lesson();
        }

        public void AddLesson(Lesson lesson) {
            if (!IsInLessonTable(lesson)) {
                Connection.AddLesson(lesson);
            }
        }

        public void ModifyLesson(Lesson oldLesson, Lesson changedLesson) {
            if (IsInLessonTable(oldLesson)) {
                Connection.ModifyLesson(oldLesson, changedLesson);
            }
        }

        public void DeleteLesson(Lesson lesson) {
            if (IsInLessonTable(lesson)) {
                Connection.DeleteLesson(lesson);
            }
        }

        public bool IsEmptyTable() {
            return Connection.IsEmptyTable();
        }
        
        public List<Lesson> GetLessonList() {
            return Connection.GetLessonList();
        }

        public List<Teacher> GetTeachersLesson(Lesson lesson) {
            return Connection.GetTeachersLesson(lesson);
        }

        public List<Student> GetStudentsLesson(Lesson lesson) {
            return Connection.GetStudentsLesson(lesson);
        }

        public Subject GetSubjectLesson(Lesson lesson) {
            return Connection.GetSubjectLesson(lesson);
        }

        public List<Subject> GetSubjectsWithLesson() {
            return Connection.GetSubjectsWithLesson();
        }

        public bool IsInLessonTable(Lesson lesson) {
            return Connection.IsInLessonTable(lesson);
        }

        public List<Subject> GetTeacherInAllSubjects(Teacher teacher) {
            return Connection.GetTeacherInAllSubjects(teacher);
        }

        public List<Subject> GetStudentsInAllSubjects(Student student) {
            return Connection.GetStudentsInAllSubjects(student);
        }
    }
}
