﻿using School.Logic;
using ConnectionDataBase;
using School.IControllers;
using IConnection;

namespace School.Controller {

    public class ControllerPayActivity : IControllerPayActivity {

        IConnectionPayActivity connection = new ConnectionPayActivity();
        private static ControllerPayActivity instance = null;
        private static readonly object padlock = new object();

        private ControllerPayActivity() {

        }

        public static ControllerPayActivity Instance {
            get {
                lock (padlock) {
                    if (instance == null) {
                        instance = new ControllerPayActivity();
                    }
                    return instance;
                }
            }
        }

        public PayActivity CreatePayActivity(Student student, Activity activity) {
            PayActivity payActivity = new PayActivity();
            payActivity.Student = student;
            payActivity.Activity = activity;
            return payActivity;
        }

        public void AddPayActivity(object aStudent, object aActivity) {
            Student student = (Student)aStudent;
            Activity activity = (Activity)aActivity;
            PayActivity payActivity = CreatePayActivity(student, activity);
            connection.AddPayActivity(payActivity);
        }
    }
}
