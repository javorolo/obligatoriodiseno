﻿using School.Logic;
using ConnectionDataBase;
using School.IControllers;
using IConnection;

namespace School.Controller {

    public class ControllerPayFee : IControllerPayFee{

        IConnectionPayFee Connection = new ConnectionPayFee();
        private static ControllerPayFee instance = null;
        private static readonly object padlock = new object();

        private ControllerPayFee() {

        }

        public static ControllerPayFee Instance {
            get {
                lock (padlock) {
                    if (instance == null) {
                        instance = new ControllerPayFee();
                    }
                    return instance;
                }
            }
        }

        public void AddPay(Student student, int month, int year) {
            PayFee payfee = new PayFee();
            payfee.Student = student;
            payfee.Month = month;
            payfee.Year = year;
            Connection.AddPay(payfee);
        }

        public PayFee GetLastPayFee(object aStudent) {
            Student student = (Student)aStudent;
            return Connection.GetLastPayFee(student);
        }
    }
}
