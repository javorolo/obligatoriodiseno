﻿using System.Collections.Generic;
using School.Logic;
using System.Text.RegularExpressions;
using ConnectionDataBase;
using School.IControllers;
using IConnection;
using System.Windows.Forms;

namespace School.Controller {

    public sealed class ControllerVan : IControllerVan {

        const int badgeLength = 7;
        const int maxCapacity = 50;
        const int minCapacity = 1;

        IConnectionVan Connection = new ConnectionVan();
        private static ControllerVan instance = null;
        private static readonly object padlock = new object();

        private ControllerVan() {

        }

        public static ControllerVan Instance {
            get {
                lock (padlock) {
                    if (instance == null) {
                        instance = new ControllerVan();
                    }
                    return instance;
                }
            }
        }

        public Van NewVan(string badge, int capacity, int consumption) {
            Van van = new Van();
            van.Badge = badge;
            van.Capacity = capacity;
            van.Consumption = consumption;
            return van;
        }

        public void AddVan(Van van) {
            Connection.AddVan(van);
        }

        public List<Van> GetListOfVans() {
            return Connection.GetListOfVans();
        }

        public void ModifyVan(Van oldVan, Van changedVan) {
            if (Connection.IsInVanTable(oldVan)) {
                Connection.ModifyVan(oldVan, changedVan);
            }
        }

        public void DeleteVan(Van van) {
            if (Connection.IsInVanTable(van)) {
                Connection.DeleteVan(van);
            }
        }

        public bool IsInVanTable(Van van) {
            return Connection.IsInVanTable(van);
        }

        public bool CapacityIsOk(Van van) {
            return van.Capacity >= minCapacity && van.Capacity <= maxCapacity;
        }

        public bool ConsumptionIsOk(Van van) {
            return van.Consumption >= minCapacity && van.Consumption <= maxCapacity;
        }

        public bool IsOKBadgeFormat(Van van) {
            return AreSevenChars(van) && Are3NumberAnd4Letters(van);
        }

        public bool IsEmptyTable() {
            return Connection.AreSomeVans();
        }

        private bool AreSevenChars(Van van) {
            return van.Badge.Length == badgeLength;
        }

        private bool Are3NumberAnd4Letters(Van van) {
            string pattern = "[A-Z]{3}\\d{4}";
            Regex r = new Regex(pattern, RegexOptions.IgnoreCase);
            Match m = r.Match(van.Badge);
            return m.Success;
        }

    }
}
