﻿using ConnectionDataBase;
using IConnection;
using School.IControllers;
using School.Logic;
using System;
using System.Collections.Generic;

namespace School.Controller {

    public sealed class ControllerActivity : IControllerActivity {

        const int maxPrice = 5000;
        const int minPrice = 0;
        const int minNameLength = 3;

        IConnectionActivity Connection =  new ConnectionActivity();
        private static ControllerActivity instance = null;
        private static readonly object padlock = new object();

        private ControllerActivity() {

        }

        public static ControllerActivity Instance {
            get {
                lock (padlock) {
                    if (instance == null) {
                        instance = new ControllerActivity();
                    }
                    return instance;
                }
            }
        }

        public Activity NewActivity(string name, int price, DateTime date) {
            Activity activity = new Activity();
            activity.Name = name;
            activity.Price = price;
            activity.Date = date;
            return activity;
        }

        public void AddActivity(Activity activity) {
            Connection.AddActivity(activity);
        }

        public void ModifyActivity(Activity oldActivity, Activity changedActivity) {
            if (Connection.IsInActivityTable(oldActivity)) {
                Connection.ModifyActivit(oldActivity, changedActivity);
            }
        }

        public void DeleteActivity(Activity activity) {
            if (Connection.IsInActivityTable(activity)) {
                Connection.DeleteActivity(activity);
            }
        }

        public List<Activity> GetActivitiesList() {
            return Connection.GetListOfActivities();
        }

        public bool IsEmptyActivities() {
            return Connection.AreSomeActivity();
        }

        public bool IsInActivityTable(Activity activity) {
            return Connection.IsInActivityTable(activity);
        }

        public bool IsOkNameFormat(string name) {
            return name.Length > minNameLength;
        }

        public bool IsCorrectPrice(int price) {
            return price > minPrice && price < maxPrice;
        }

        public bool DateIsOk(DateTime date) {
            return date >= DateTime.Today;
        }
        
    }
}
