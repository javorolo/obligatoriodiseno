﻿using ConnectionDataBase;
using School.Logic;
using System;
using System.Collections.Generic;
using System.Data;
using School.IControllers;
using IConnection;

namespace School.Controller {

    public sealed class ControllerRoad : IControllerRoad{

        IConnectionStudent connection = new ConnectionStudent();
        private Location schoolLocation;
        private static ControllerRoad instance = null;
        private static readonly object padlock = new object();

        private ControllerRoad() {
            schoolLocation = new Location();
        }

        public static ControllerRoad Instance {
            get {
                lock (padlock) {
                    if (instance == null) {
                        instance = new ControllerRoad();
                    }
                    return instance;
                }
            }
        }
        
        public DataTable ShortestRoute() {
            string shortesRouteStr = ShortestRouteString();
            List<Student> studentRoute = GetStudents(shortesRouteStr.Split(','));
            DataTable listRoads = ListOfRoadsDataTable(studentRoute);
            return listRoads;
        }

        private DataTable ListOfRoadsDataTable(List<Student> studentRoute) {
            List<Van> vans = ControllerVan.Instance.GetListOfVans();
            DataTable data = new DataTable();
            int[] countRoadsVan = new int[vans.Count];
            List<Double> distances = DistanceFromRoute(studentRoute);
            bool exitToCount = false;
            double distanceVan = 0;
            vans.Sort();
            CreateColumns(data);
            
            while (vans.Count > 0 && !exitToCount) {
                Van van = vans[0];
                int capacity = van.Capacity;
                string road = "" + van.Badge;
                string listOfStudents = "";
                while (studentRoute.Count > 0 && capacity > 0) {
                    listOfStudents = listOfStudents + studentRoute[0].Name + ", ";
                    distanceVan = distanceVan + distances[0];
                    capacity--;
                    studentRoute.Remove(studentRoute[0]);
                    distances.Remove(distances[0]);
                }
                if(capacity == 0 && studentRoute.Count > 0) {
                    distances = DistanceFromRoute(studentRoute);
                }
                if (studentRoute.Count == 0) {
                    exitToCount = true;
                }
                capacity = van.Capacity;
                double consumption = van.Consumption / van.Capacity;
                countRoadsVan[(countRoadsVan.Length - vans.Count)]++;
                data.Rows.Add(road, capacity, consumption, distanceVan, listOfStudents, countRoadsVan[(countRoadsVan.Length - vans.Count)]);
                vans.Remove(vans[0]);
                distanceVan = 0;
                if(studentRoute.Count > 0)
                    distanceVan = distanceVan + DistanceBetweenLocalities(schoolLocation, studentRoute[0].Location);
                if (vans.Count == 0) {
                    vans = ControllerVan.Instance.GetListOfVans();
                }
            }
            return data;
        }

        private void CreateColumns(DataTable data) {
            data.Columns.Add("Van", typeof(string));
            data.Columns.Add("Capacity", typeof(string));
            data.Columns.Add("Student/Consumption", typeof(double));
            data.Columns.Add("Distance", typeof(double));
            data.Columns.Add("Students", typeof(string));
            data.Columns.Add("Roads", typeof(int));
        }

        private List<double> DistanceFromRoute(List<Student> studentRoute) {
            List<double> distances = new List<double>();
            distances.Add(DistanceBetweenLocalities(schoolLocation, studentRoute[0].Location));
            for (int i = 0; i< studentRoute.Count -1; i++) {
                distances.Add(DistanceBetweenLocalities(studentRoute[i].Location, studentRoute[i + 1].Location));
            }
            return distances;
        }

        private List<Student> GetStudents(string[] indices) {
            List<Student> students = new List<Student>();
            List<Student> dataBaseStudents = connection.GetStudentTable();
            Student aStudent = new Student();
            for (int i = 0; i < indices.Length - 1; i++) { 
                aStudent = GetStudentList(dataBaseStudents,indices[i]);
                students.Add(aStudent);
            }
            return students;
        }

        private Student GetStudentList(List<Student> dataBaseStudents, string v) {
            Student studentReturn = new Student();
            for (int i=0; i<dataBaseStudents.Count;i++) {
                if (dataBaseStudents[i].StudentId == Int32.Parse(v)) {
                    studentReturn = dataBaseStudents[i];
                    return studentReturn;
                }
            }
            return studentReturn;
        }

        private string ShortestRouteString() {

            int[] identifications = ControllerStudent.Instance.GetIdentifications();
            int n = identifications.Length;
            int r = identifications.Length;
            List<string> permutations = new List<string>();

            PossiblePermutations(permutations, identifications, "", n, r);

            string shortestRoute = "";
            double minimalDistance = 1000000;
            for (int i = 0; i < permutations.Count; i++) {
                double distance = PermutationDistance(permutations[i]);
                Console.WriteLine("\n" + distance);
                Console.WriteLine(" " + permutations[i]);
                if (distance < minimalDistance) {
                    shortestRoute = permutations[i];
                    minimalDistance = distance;
                }
            }
            return shortestRoute;
        }


        private void PossiblePermutations(List<string> permutations, int[] elem, string act, int n, int r) {
            if (n == 0) {
                permutations.Add(act);
            } else {
                for (int i = 0; i < r; i++) {
                    if (!act.Contains("" + elem[i]))
                        PossiblePermutations(permutations, elem, act + elem[i] + ",", n - 1, r);
                }
            }
        }

        private double PermutationDistance(string combinacion) {
            string[] indices = combinacion.Split(',');
            List<Location> locations = GetLocationsIndeces(indices);

            double distance = 0;
            Location current = this.schoolLocation;
            Location next = locations[0];
            distance = distance + DistanceBetweenLocalities(current, next);

            for (int i = 0; i < locations.Count - 1; i++) {
                current = locations[i];
                next = locations[i+1];
                distance = distance + DistanceBetweenLocalities(current, next);
            }
            return distance;
        }

        private List<Location> GetLocationsIndeces(string[] indices) {
            List<Location> locationsReturn = new List<Location>();
            List<Student> students = connection.GetStudentTable();
            if (students.Count == 1) {
                locationsReturn.Add(students[0].Location);
                return locationsReturn;
            }
            for (int i = 0; i < indices.Length - 1; i++) {
                for(int j = 0; j< students.Count -1; j++) {
                    if (students[j].StudentId == Int32.Parse(indices[i])) {
                        locationsReturn.Add(students[j].Location);
                    }
                }
                
            }
            return locationsReturn;
        }

        private double DistanceBetweenLocalities(Location locationA, Location locationB) {
            double distance;
            distance = Math.Sqrt((locationA.CartesianX - locationB.CartesianX)*
                (locationA.CartesianX - locationB.CartesianX) + 
                (locationA.CartesianY - locationB.CartesianY)*
                (locationA.CartesianY - locationB.CartesianY));
            return distance;
        }
    }
}
