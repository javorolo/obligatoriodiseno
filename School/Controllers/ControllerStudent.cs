﻿using System;
using System.Collections.Generic;
using System.Linq;
using School.Logic;
using System.Text.RegularExpressions;
using ConnectionDataBase;
using School.IControllers;
using IConnection;

namespace School.Controller {

    public class ControllerStudent : IControllerStudent{

        const int schoolPosition = 0;
        const int locationLimit = 100;
        const int identificationChars = 9;

        IConnectionPayActivity ConnectionPayActivity = new ConnectionPayActivity();
        IConnectionStudent Connection = new ConnectionStudent();
        private static ControllerStudent instance = null;
        private static readonly object padlock = new object();

        private ControllerStudent() {

        }

        public static ControllerStudent Instance {
            get {
                lock (padlock) {
                    if (instance == null) {
                        instance = new ControllerStudent();
                    }
                    return instance;
                }
            }
        }

        public Student NewStudent(string name, string lastName, string id) {
            Student student = new Student();
            student.Name = name;
            student.LastName = lastName;
            student.Identification = id;
            return student;
        }

        public void AddStudent(Student student) {
            if (!Connection.IsInStudentTable(student)) {
                Connection.AddStudent(student);
            }

        }

        public void ModifyStudent(Student oldStudent, Student changedStudent) {
            if (Connection.IsInStudentTable(oldStudent)) {
                Connection.ModifyStudent(oldStudent, changedStudent);
            }
        }

        public List<Activity> GetActivities(object aStudent) {
            Student student = (Student)aStudent;
            return Connection.GetStudentActivities(student);
        }

        public void DeleteStudent(Student student) {
            if (Connection.IsInStudentTable(student)) {
                ConnectionPayActivity.DeletePayActivity(student.StudentId);
                Connection.DeleteStudent(student);
            }
        }

        public List<Student> GetStudentTable() {
            return Connection.GetStudentTable();
        }

        public bool IsEmptyList() {
            return Connection.IsEmptyTable();
        }

        public bool IsInStudentTable(Student student) {
            return Connection.IsInStudentTable(student);
        }

        public bool IsIdentificationFormatOk(Student student) {
            return NineCharsLong(student) && ValidIdentificationFormat(student);
        }

        public bool CorrectStudentLocation(Student student) {
            return NotInSchool(student) && IsInBounds(student);
        }

        public bool CorrectName(Student student) {
            return student.CorrectName(student.Name) && student.CorrectName(student.LastName);
        }

        public int[] GetIdentifications() {
            List<Student> studentsList = Connection.GetStudentTable();
            int[] listIdentifications = new int[studentsList.Count];
            for (int i = 0; i < studentsList.Count; i++) {
                listIdentifications[i] = Convert.ToInt32(studentsList.ElementAt(i).StudentId);
            }
            return listIdentifications;
        }

        public void AddActivityToStudent(Object student, Object activity) {
            Student st = (Student)student;
            Activity ac = (Activity)activity;
            Connection.AddActivityToStudent(st, ac);
        }

        private bool NineCharsLong(Student student) {
            return student.Identification.Length == identificationChars;
        }

        private bool NotInSchool(Student student) {
            return student.Location.CartesianX != schoolPosition 
                || student.Location.CartesianY != schoolPosition;
        }

        private bool IsInBounds(Student student) {
            return student.Location.CartesianX >= -locationLimit && 
                student.Location.CartesianY >= -locationLimit && 
                student.Location.CartesianX <= locationLimit && 
                student.Location.CartesianY <= locationLimit;
        }

        private bool ValidIdentificationFormat(Student student) {
            string pattern = "[0-9]{7}(-\\d{1})";
            Regex r = new Regex(pattern, RegexOptions.IgnoreCase);
            Match m = r.Match(student.Identification);
            return m.Success;
        }    
    }
}
