﻿using System.Collections.Generic;
using School.Logic;
using ConnectionDataBase;
using School.IControllers;
using IConnection;

namespace School.Controller {

    public sealed class ControllerTeacher : IControllerTeacher {

        private static ControllerTeacher instance = null;
        private static readonly object padlock = new object();
        IConnectionTeacher Connection = new ConnectionTeacher();

        private ControllerTeacher() {

        }

        public static ControllerTeacher Instance {
            get {
                lock (padlock) {
                    if (instance == null) {
                        instance = new ControllerTeacher();
                    }
                    return instance;
                }
            }
        }

        public Teacher NewTeacher(string name, string lastName) {
            Teacher teacher = new Teacher();
            teacher.Name = name;
            teacher.LastName = lastName;
            return teacher;
        }

        public void AddTeacher(Teacher teacher) {
            Connection.AddTeacher(teacher);
        }

        public void ModifyTeacher(Teacher oldTeacher, Teacher changedTeacher) {
            if (Connection.IsInTeacherTable(oldTeacher)) {
                Connection.ModifyTeacher(oldTeacher, changedTeacher);
            }
        }

        public void DeleteTeacher(Teacher teacher) {
            if (Connection.IsInTeacherTable(teacher)) {
                Connection.DeleteTeacher(teacher);
            }
        }

        public List<Teacher> GetTeacherTable() {
            return Connection.GetTeacherTable();
        }

        public bool IsEmptyTable() {
            return Connection.IsEmptyTable();
        }

        public bool IsInTeacherTable(Teacher teacher) {
            return Connection.IsInTeacherTable(teacher);
        }

        public bool CorrectName(Teacher teacher) {
            return teacher.CorrectName(teacher.Name) && teacher.CorrectName(teacher.LastName);
        }

    }
}
