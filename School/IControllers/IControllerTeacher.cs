﻿using School.Logic;
using System.Collections.Generic;

namespace School.IControllers {

    public interface IControllerTeacher {

        Teacher NewTeacher(string name, string lastName);

        void AddTeacher(Teacher teacher);

        void ModifyTeacher(Teacher oldTeacher, Teacher changedTeacher);

        void DeleteTeacher(Teacher teacher);

        List<Teacher> GetTeacherTable();

        bool IsEmptyTable();

        bool IsInTeacherTable(Teacher teacher);

        bool CorrectName(Teacher teacher);

    }
}
