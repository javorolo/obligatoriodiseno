﻿using School.Logic;
using System.Collections.Generic;

namespace School.IControllers {
    public interface IControllerSubject {

        Subject NewSubject(string name);

        void AddSubject(Subject subject);

        void ModifySubject(Subject oldSubject, Subject changedSubject);

        void DeleteSubject(Subject subject);

        List<Subject> GetSubjectTable();

        bool ValidNumberOfCharsInName(Subject subject);

        bool Have4Chars(Subject subject);

        bool IsEmptyList();

        bool CorrectCodeFormat(Subject subject);

        string CodeGenerator(string name);
    }
}
