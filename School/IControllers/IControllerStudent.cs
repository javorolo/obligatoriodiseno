﻿using School.Logic;
using System;
using System.Collections.Generic;

namespace School.IControllers {

    public interface IControllerStudent {

        Student NewStudent(string name, string lastName, string id);

        void AddStudent(Student student);

        void ModifyStudent(Student oldStudent, Student changedStudent);

        List<Activity> GetActivities(object aStudent);

        void DeleteStudent(Student student);

        List<Student> GetStudentTable();

        bool IsEmptyList();

        bool IsInStudentTable(Student student);

        bool IsIdentificationFormatOk(Student student);

        bool CorrectStudentLocation(Student student);

        bool CorrectName(Student student);

        int[] GetIdentifications();

        void AddActivityToStudent(Object student, Object activity);
    }
}
