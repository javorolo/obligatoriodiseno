﻿using School.Logic;
using System;
using System.Collections.Generic;

namespace School.IControllers {

    public interface IControllerActivity {

        void AddActivity(Activity activity);

        List<Activity> GetActivitiesList();

        void DeleteActivity(Activity activity);

        void ModifyActivity(Activity oldActivity, Activity changedActivity);

        bool IsEmptyActivities();

        Activity NewActivity(string name, int price, DateTime date);

        bool IsOkNameFormat(string name);

        bool IsCorrectPrice(int price);

        bool DateIsOk(DateTime date);
        
    }
}
