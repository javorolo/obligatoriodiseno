﻿using School.Logic;
using System.Collections.Generic;

namespace School.IControllers {

    public interface IControllerVan {

        Van NewVan(string badge, int capacity, int consumption);

        void AddVan(Van van);

        List<Van> GetListOfVans();

        void ModifyVan(Van oldVan, Van changedVan);

        void DeleteVan(Van van);

        bool IsInVanTable(Van van);

        bool CapacityIsOk(Van van);

        bool IsOKBadgeFormat(Van van);

        bool IsEmptyTable();

        bool ConsumptionIsOk(Van van);
    }
}
