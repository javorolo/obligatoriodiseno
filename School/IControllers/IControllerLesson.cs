﻿using School.Logic;
using System.Collections.Generic;

namespace School.IControllers {

    public interface IControllerLesson {

        Lesson NewLesson();

        void AddLesson(Lesson lesson);

        void ModifyLesson(Lesson oldLesson, Lesson changedLesson);

        void DeleteLesson(Lesson lesson);

        bool IsEmptyTable();

        List<Lesson> GetLessonList();

        List<Teacher> GetTeachersLesson(Lesson lesson);

        List<Student> GetStudentsLesson(Lesson lesson);

        Subject GetSubjectLesson(Lesson lesson);

        List<Subject> GetSubjectsWithLesson();

        bool IsInLessonTable(Lesson lesson);

        List<Subject> GetTeacherInAllSubjects(Teacher teacher);

        List<Subject> GetStudentsInAllSubjects(Student student);
    }
}
