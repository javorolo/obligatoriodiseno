﻿using School.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.IControllers {
    public interface IControllerPayActivity {

        PayActivity CreatePayActivity(Student student, Activity activity);
        void AddPayActivity(object aStudent, object aActivity);

    }
}
