﻿using System.Data;

namespace School.IControllers {

    public interface IControllerRoad {

        DataTable ShortestRoute();
    }
}
