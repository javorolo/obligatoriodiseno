﻿using School.Logic;

namespace School.IControllers {

    public interface IControllerPayFee {

        PayFee GetLastPayFee(object aStudent);

        void AddPay(Student student, int month, int year);
    }
}
