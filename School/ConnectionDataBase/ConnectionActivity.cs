﻿using System.Collections.Generic;
using System.Linq;
using School.Logic;
using IConnection;

namespace ConnectionDataBase {
    public class ConnectionActivity : IConnectionActivity {

        public void AddActivity(Activity activity) {
            using (SchoolContext ctx = new SchoolContext()) {
                ctx.Activities.Add(activity);
                ctx.SaveChanges();
            }
        }

        public void ModifyActivit(Activity oldActivity, Activity changedActivity) {
            Activity myActivity = null;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Activity> query = from activity in context.Activities
                                              select activity;
                foreach (Activity activity in query) {
                    if (activity.Id == oldActivity.Id) {
                        myActivity = activity;
                    }
                }
            }
            using (SchoolContext context = new SchoolContext()) {
                if (myActivity != null) {
                    context.Activities.Attach(myActivity);
                    myActivity.Name = changedActivity.Name;
                    myActivity.Price = changedActivity.Price;
                    myActivity.Date = changedActivity.Date;
                }
                context.SaveChanges();
            }
        }

        public void DeleteActivity(Activity activity) {
            using (SchoolContext context = new SchoolContext()) {
                var itemToRemove = context.Activities.SingleOrDefault(a => a.Id == activity.Id);
                if (itemToRemove != null) {
                    context.Activities.Remove(itemToRemove);
                    context.SaveChanges();
                }
            }
        }

        public bool AreSomeActivity() {
            using (SchoolContext context = new SchoolContext()) {
                Activity first = context.Activities.FirstOrDefault<Activity>();
                try {
                    return first.Equals(null);
                } catch {
                    return true;
                }
            }
        }

        public bool IsInActivityTable(Activity activity) {
            using (SchoolContext context = new SchoolContext()) {
                return context.Activities.Any(a => a.Id == activity.Id);
            }
        }

        public List<Activity> GetListOfActivities() {
            List<Activity> activities = new List<Activity>();
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Activity> query = from activity in context.Activities
                                         select activity;
                foreach (Activity activity in query) {
                    activities.Add(activity);
                }
            }
            return activities;
        }

        public void deleteAll() {
            using (SchoolContext context = new SchoolContext()) {
                var all = from activity in context.Activities select activity;
                context.Activities.RemoveRange(all);
                context.SaveChanges();
            }
        }

    }
}
