﻿using System.Collections.Generic;
using System.Linq;
using School.Logic;
using IConnection;

namespace ConnectionDataBase {

    public class ConnectionVan : IConnectionVan {

        public void AddVan( Van van) {
            using (SchoolContext ctx = new SchoolContext()) {
                ctx.Vans.Add(van);
                ctx.SaveChanges();
            }
        }

        public void ModifyVan(Van oldVan, Van changedVan) {
            Van myVan = null;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Van> query = from van in context.Vans
                                         where van.Badge == changedVan.Badge
                                         select van;
                myVan = query.FirstOrDefault();
                if (myVan != null) {
                    context.Vans.Attach(myVan);
                    myVan.Badge = changedVan.Badge;
                    myVan.Capacity = changedVan.Capacity;
                    myVan.Consumption = changedVan.Consumption;
                }
                context.SaveChanges();
            }
        }

        public void DeleteVan(Van van) {
            using (SchoolContext context = new SchoolContext()) {
                var itemToRemove = context.Vans.SingleOrDefault(v => v.Id == van.Id);
                if (itemToRemove != null) {
                    context.Vans.Remove(itemToRemove);
                    context.SaveChanges();
                }
            }
        }

        public List<Van> GetListOfVans() {
            List<Van> vans = new List<Van>();
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Van> query = from van in context.Vans
                                         select van;
                foreach (Van van in query) {
                    vans.Add(van);
                }
            }
            return vans;
        }

        public bool AreSomeVans() {
            using (SchoolContext context = new SchoolContext()) {
                Van first = context.Vans.FirstOrDefault<Van>();
                try {
                    return first.Equals(null);
                }
                catch {
                    return true;
                }
            }
        }

        public bool IsInVanTable(Van van) {
            using (SchoolContext context = new SchoolContext()) {
                return context.Vans.Any(v => v.Badge == van.Badge);
            }
        }

        public void deleteAll() {
            using (SchoolContext context = new SchoolContext()) {
                var all = from van in context.Vans select van;
                context.Vans.RemoveRange(all);
                context.SaveChanges();
            }
        }
    }
}
