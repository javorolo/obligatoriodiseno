﻿using System.Collections.Generic;
using System.Linq;
using School.Logic;
using IConnection;

namespace ConnectionDataBase {

    public class ConnectionTeacher : IConnectionTeacher {

        public void AddTeacher(Teacher teacher) {
            using (SchoolContext ctx = new SchoolContext()) {
                ctx.Teachers.Add(teacher);
                ctx.SaveChanges();
            }
        }

        public void ModifyTeacher(Teacher oldTeacher, Teacher changedTeacher) {
            Teacher aTeacher = null;

            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Teacher> query = from teacher in context.Teachers
                                             select teacher;
                foreach (Teacher teacher in query) {
                    if(teacher.Id == oldTeacher.Id) {
                        aTeacher = teacher;
                    }
                }
            }
            using (SchoolContext context = new SchoolContext()) {
                if(aTeacher != null) {
                    context.Teachers.Attach(aTeacher);
                    aTeacher.Name = changedTeacher.Name;
                    aTeacher.LastName = changedTeacher.LastName;
                }
                context.SaveChanges();
            }
            
        }

        public void DeleteTeacher(Teacher teacher) {
            using (SchoolContext context = new SchoolContext()) {
                var TeacherToRemove = context.Teachers.SingleOrDefault(v => v.Id == teacher.Id);
                if (TeacherToRemove != null) {
                    context.Teachers.Remove(TeacherToRemove);
                }
                context.SaveChanges();
            }
        }

        public List<Teacher> GetTeacherTable() {
            List<Teacher> teachers = new List<Teacher>();
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Teacher> query = from teacher in context.Teachers
                                             select teacher;
                foreach (Teacher teacher in query) {
                    teachers.Add(teacher);
                }
            }
            return teachers;
        }

        public bool IsEmptyTable() {
            using (SchoolContext context = new SchoolContext()) {
                Teacher first = context.Teachers.FirstOrDefault<Teacher>();
                try {
                    return first.Equals(null);
                } catch {
                    return true;
                }
            }
        }

        public bool IsInTeacherTable(Teacher teacher) {
            using (SchoolContext context = new SchoolContext()) {
                return context.Teachers.Any(v => v.Id == teacher.Id);
            }
        }

        public void deleteAll() {
            using (SchoolContext context = new SchoolContext()) {
                var all = from teacher in context.Teachers select teacher;
                context.Teachers.RemoveRange(all);
                context.SaveChanges();
            }
        }
    }
}
