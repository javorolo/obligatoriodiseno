﻿using System.Collections.Generic;
using System.Linq;
using School.Logic;
using IConnection;

namespace ConnectionDataBase {

    public class ConnectionStudent : IConnectionStudent {

        public void AddStudent(Student student) {
            using (SchoolContext ctx = new SchoolContext()) {
                ctx.Students.Add(student);
                ctx.SaveChanges();
            }
        }

        public void Attach(SchoolContext context, Student student) {
            foreach (Activity activity in student.Activities) {
                context.Activities.Attach(activity);
            }
        }

        public void ModifyStudent(Student oldStudent, Student changedStudent) {
            Student aStudent = null;

            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Student> query = from student in context.Students
                                             select student;
                foreach (Student student in query) {
                    if (student.Identification == oldStudent.Identification) {
                        aStudent = student;
                    }
                }
            }
            using (SchoolContext context = new SchoolContext()) {
                if(aStudent != null) {
                    context.Students.Attach(aStudent);
                    aStudent.Name = changedStudent.Name;
                    aStudent.LastName = changedStudent.LastName;
                    aStudent.Identification = changedStudent.Identification;
                    aStudent.Location.CartesianX = changedStudent.Location.CartesianX;
                    aStudent.Location.CartesianY = changedStudent.Location.CartesianY;
                }
                context.SaveChanges();
            }
        }

        public void DeleteStudent(Student student) {
            using (SchoolContext context = new SchoolContext()) {
                var studentToRemove = context.Students.SingleOrDefault
                    (v => v.Identification == student.Identification);
                if (studentToRemove != null) {
                    context.Students.Remove(studentToRemove); 
                }
                context.SaveChanges();
            }
        }

        public List<Student> GetStudentTable() {
            List<Student> students = new List<Student>();
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Student> query = from student in context.Students
                                             select student;
                foreach (Student student in query) {
                    students.Add(student);
                }
            }
            return students;
        }

        public bool IsInStudentTable(Student student) {
            using (SchoolContext context = new SchoolContext()) {
                return context.Students.Any(v => v.Identification == student.Identification);
            }
        }

        public bool IsEmptyTable() {
            using (SchoolContext context = new SchoolContext()) {
                Student first = context.Students.FirstOrDefault();
                try {
                    return first.Equals(null);
                } catch {
                    return true;
                }
            }
        }

        public void AddActivityToStudent(Student st, Activity ac) {
            Student aStudent = null;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Student> query = from student in context.Students
                                             select student;
                foreach (Student student in query) {
                    if (student.Identification == st.Identification) {
                        aStudent = student;
                    }
                }
                aStudent.Activities.Add(ac);
                Attach(context, aStudent);
                context.SaveChanges();
            }
        }

        public List<Activity> GetStudentActivities(Student aStudent) {
            Student studentInBD = null;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Student> query = from student in context.Students
                                             select student;
                foreach (Student student in query) {
                    if (student.Identification == aStudent.Identification) {
                        studentInBD = student;
                    }
                }
                return studentInBD.Activities;
            }
        }

        public void deleteAll() {
            using (SchoolContext context = new SchoolContext()) {
                var all = from student in context.Students select student;
                context.Students.RemoveRange(all);
                context.SaveChanges();
            }
        }
    }
}
