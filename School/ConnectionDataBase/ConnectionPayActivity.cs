﻿using School.Logic;
using IConnection;

namespace ConnectionDataBase {

    public class ConnectionPayActivity : IConnectionPayActivity {

        public void AddPayActivity(PayActivity payActivity) {
            using (SchoolContext connection = new SchoolContext()) {
                string sqlPayActivity = SqlInsertPayActivity(payActivity);
                connection.Database.ExecuteSqlCommand(sqlPayActivity);
                connection.SaveChanges();
            }
        }

        public void DeletePayActivity(int id) {
            using (SchoolContext connection = new SchoolContext()) {
                string sqlDeletePayActivity = SqlDeletePayActivity(id);
                connection.Database.ExecuteSqlCommand(sqlDeletePayActivity);
                connection.SaveChanges();
            }
        }

        public int CountPays() {
            using (SchoolContext connection = new SchoolContext()) {
                string sqlPayActivity = SqlCountsPayActivities();
                return connection.Database.ExecuteSqlCommand(sqlPayActivity);
            }
        }

        private string SqlInsertPayActivity(PayActivity payActivity) {
            return "INSERT dbo.PayActivities (Activity_Id,Student_StudentId) VALUES("
                + payActivity.Activity.Id.ToString() + "," + payActivity.Student.StudentId.ToString() + ")";
        }

        private string SqlCountsPayActivities() {
            return "SELECT COUNT(*) FROM dbo.PayActivities";
        }

        private string SqlDeletePayActivity(int id) {
            return "DELETE dbo.PayActivities FROM dbo.PayActivities WHERE Student_StudentId = "
                + id.ToString();
        }
    }
}
