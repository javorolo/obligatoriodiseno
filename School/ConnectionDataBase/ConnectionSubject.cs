﻿using System.Collections.Generic;
using School.Logic;
using System.Linq;
using System;
using IConnection;

namespace ConnectionDataBase {

    public class ConnectionSubject : IConnectionSubject {

        public void AddSubject(Subject subject) {
            using (SchoolContext ctx = new SchoolContext()) {
                ctx.Subjects.Add(subject);
                ctx.SaveChanges();
            }
        }

        public void ModifySubject(Subject oldSubject, Subject changedSubject) {
            Subject aSubject = null;

            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Subject> query = from subject in context.Subjects
                                             where subject.Id == oldSubject.Id
                                             select subject;
                aSubject = query.FirstOrDefault();
                if (aSubject != null) {
                    context.Subjects.Attach(aSubject);
                    aSubject.Name = changedSubject.Name;
                    aSubject.Code = changedSubject.Code;
                }
                context.SaveChanges();
            }
        }

        public void DeleteSubject(Subject subject) {
            using (SchoolContext context = new SchoolContext()) {
                var subjectToRemove = context.Subjects.SingleOrDefault(v => v.Id == subject.Id);
                if (subjectToRemove != null) {
                    context.Subjects.Remove(subjectToRemove);
                    context.SaveChanges();
                }
            }
        }

        public List<Subject> GetSubjectsList() {
            List<Subject> subjects = new List<Subject>();
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Subject> query = from subject in context.Subjects
                                             select subject;
                foreach (Subject subject in query) {
                    subjects.Add(subject);
                }
            }
            return subjects;
        }

        public bool IsInSubjectList(Subject oldSubject) {
            using (SchoolContext context = new SchoolContext()) {
                return context.Subjects.Any(v => v.Id == oldSubject.Id);
            }
        }

        public bool IsEmptyTable() {
            using (SchoolContext context = new SchoolContext()) {
                Subject first = context.Subjects.FirstOrDefault<Subject>();
                try {
                    return first.Equals(null);
                } catch {
                    return true;
                }
            }
        }

        public int NumberOfSubjectsWithSameName(string name) {
            int count = 1;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Subject> query = from subject in context.Subjects
                                             select subject;
                for (int i = 0; i < query.Count(); i++) {
                    if (query.ElementAt(i).Name == name) {
                        count++;
                    }
                }
            }
            return count;
        }

        public void deleteAll() {
            using (SchoolContext context = new SchoolContext()) {
                var all = from subject in context.Subjects select subject;
                context.Subjects.RemoveRange(all);
                context.SaveChanges();
            }
        }
    }
}
