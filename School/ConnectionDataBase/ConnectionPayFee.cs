﻿using School.Logic;
using System.Collections.Generic;
using System.Linq;
using IConnection;

namespace ConnectionDataBase {
    public class ConnectionPayFee : IConnectionPayFee {

        public void AddPay(PayFee payfee) {
            using (SchoolContext context = new SchoolContext()) {
                context.PaysFees.Add(payfee);
                context.Students.Attach(payfee.Student);
                context.SaveChanges();
            }
        }

        public PayFee GetLastPayFee(Student aStudent) {
            PayFee payFee = null;
            try { 
            using (SchoolContext connection = new SchoolContext()) {
                IEnumerable<PayFee> query = from pay in connection.PaysFees
                    where aStudent.StudentId == pay.Student.StudentId
                    select pay;
                payFee = query.Last();
                return payFee;
                }
            }
            catch {
                return payFee;
            }
        }

        private string SqlInsertPayActivity(Student student) {
            return "SELECT [Id] FROM [BDSchool].[dbo].[PayFees] WHERE Student_StudentId = "
                + student.StudentId;
        }
    }
}
