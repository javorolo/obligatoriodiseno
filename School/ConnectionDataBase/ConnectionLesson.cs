﻿using System.Collections.Generic;
using System.Linq;
using School.Logic;
using IConnection;
using System;

namespace ConnectionDataBase {

    public class ConnectionLesson : IConnectionLesson {

        public void AddLesson(Lesson lesson) {
            using (SchoolContext ctx = new SchoolContext()) {
                ctx.Lessons.Add(lesson);
                ctx.Subjects.Attach(lesson.Subject);
                AttachStudents(lesson, ctx);
                AttachTeachers(lesson, ctx);
                ctx.SaveChanges();
            }
        } 

        public void ModifyLesson(Lesson oldLesson, Lesson changedLesson) {
            Lesson aLesson = null;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Lesson> query = from lesson in context.Lessons
                                            where lesson.Id == oldLesson.Id
                                            select lesson;
                aLesson = query.FirstOrDefault();
                if (aLesson != null) {
                    context.Lessons.Attach(aLesson);
                    context.Subjects.Attach(changedLesson.Subject);
                    AttachStudents(changedLesson, context);
                    AttachTeachers(changedLesson, context);
                    ModifySTSLesson(aLesson, changedLesson);
                }
                context.SaveChanges();
            }
        }

        public void DeleteLesson(Lesson lesson) {
            using (SchoolContext context = new SchoolContext()) {
                var lessonToRemove = context.Lessons.SingleOrDefault(v => v.Id == lesson.Id);
                if (lessonToRemove != null) {
                    context.Lessons.Remove(lessonToRemove);
                    context.SaveChanges();
                }
            }
        }

        public List<Lesson> GetLessonList() {
            List<Lesson> lessons = new List<Lesson>();
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Lesson> query = from lesson in context.Lessons
                                             select lesson;
                foreach (Lesson lesson in query) {
                    lessons.Add(lesson);
                }
            }
            return lessons;
        }

        public bool IsEmptyTable() {
            using (SchoolContext context = new SchoolContext()) {
                Lesson first = context.Lessons.FirstOrDefault<Lesson>();
                try {
                    return first.Equals(null);
                } catch {
                    return true;
                }
            }
        }

        public bool IsInLessonTable(Lesson lesson) {
            using (SchoolContext context = new SchoolContext()) {
                return context.Lessons.Any(v => v.Id == lesson.Id);
            }
        }

        public List<Teacher> GetTeachersLesson(Lesson aLesson) {
            List<Teacher> teachers = null;
            Lesson lessonID = null;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Lesson> query = from lesson in context.Lessons
                                            select lesson;
                foreach (Lesson lesson in query) {
                    if (lesson.Id == aLesson.Id) {
                        lessonID = lesson;
                    }
                }
                if(lessonID != null) {
                    teachers = lessonID.Teachers;
                }
            }
            return teachers;
        }

        public List<Student> GetStudentsLesson(Lesson aLesson) {
            List<Student> students = null;
            Lesson lessonID = null;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Lesson> query = from lesson in context.Lessons
                                            select lesson;
                foreach (Lesson lesson in query) {
                    if (lesson.Id == aLesson.Id) {
                        lessonID = lesson;
                    }
                }
                if(lessonID != null) {
                    students = lessonID.Students;
                }   
            }
            return students;
        }

        public Subject GetSubjectLesson(Lesson aLesson) {
            Subject subject = null;
            Lesson lessonID = null;
            using (SchoolContext context = new SchoolContext()) {
                IQueryable<Lesson> query = from lesson in context.Lessons
                                            where lesson.Id == aLesson.Id
                                            select lesson;
                lessonID = query.First();
                if (lessonID != null) {
                    subject = lessonID.Subject;
                } 
            }
            return subject;
        }

        public List<Subject> GetSubjectsWithLesson() {
            List<Subject> subjects = new List<Subject>();
            Lesson aLesson = null;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Lesson> query = from lesson in context.Lessons
                                            where lesson.Subject != null
                                            select lesson;
                for(int i = 0; i < query.Count(); i++) {
                    aLesson = query.ElementAt(i);
                    subjects.Add(aLesson.Subject);
                }
            }
            return subjects;
        }

        public List<Subject> GetTeacherInAllSubjects(Teacher teacher) {
            List<Subject> subjectsOfTeacher = new List<Subject>();
            Lesson aLesson = null;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Lesson> query = from lesson in context.Lessons
                                            select lesson;
                for (int i = 0; i < query.Count(); i++) {
                    for(int j = 0; j < query.ElementAt(i).Teachers.Count; j++) {
                        aLesson = query.ElementAt(i);
                        if (aLesson.Teachers.ElementAt(j).Id == teacher.Id) {   
                            subjectsOfTeacher.Add(aLesson.Subject);
                        }
                    }
                }
            }
            return subjectsOfTeacher;
        }

        private void ModifySTSLesson(Lesson aLesson, Lesson changedLesson) {
            aLesson.Subject = changedLesson.Subject;
            aLesson.Students.RemoveRange(0, aLesson.Students.Count - 1);
            aLesson.Students = changedLesson.Students;
            aLesson.Teachers.RemoveRange(0, aLesson.Teachers.Count - 1);
            aLesson.Teachers = changedLesson.Teachers;
        }

        private void AttachStudents(Lesson lesson, SchoolContext ctx) {
            foreach (Student student in lesson.Students) {
                ctx.Students.Attach(student);
            }
        }

        private void AttachTeachers(Lesson lesson, SchoolContext ctx) {
            foreach (Teacher teacher in lesson.Teachers) {
                ctx.Teachers.Attach(teacher);
            }
        }

        public void deleteAll() {
            using (SchoolContext context = new SchoolContext()) {
                var all = from lesson in context.Lessons select lesson;
                context.Lessons.RemoveRange(all);
                context.SaveChanges();
            }
            using (SchoolContext context = new SchoolContext()) {
                var all = from student in context.Students select student;
                context.Students.RemoveRange(all);
                context.SaveChanges();
            }
            using (SchoolContext context = new SchoolContext()) {
                var all = from subject in context.Subjects select subject;
                context.Subjects.RemoveRange(all);
                context.SaveChanges();
            }
        }

        public List<Subject> GetStudentsInAllSubjects(Student student) {
            List<Subject> subjectsOfStudent = new List<Subject>();
            Lesson aLesson = null;
            using (SchoolContext context = new SchoolContext()) {
                IEnumerable<Lesson> query = from lesson in context.Lessons
                                            select lesson;
                for (int i = 0; i < query.Count(); i++) {
                    for (int j = 0; j < query.ElementAt(i).Students.Count; j++) {
                        aLesson = query.ElementAt(i);
                        if (aLesson.Students.ElementAt(j).Equals(student)) {
                            subjectsOfStudent.Add(aLesson.Subject);
                        }
                    }
                }
            }
            return subjectsOfStudent;
        }
    }
}
