﻿using School.Logic;
using System.Data.Entity;


namespace ConnectionDataBase {
    public class SchoolContext : DbContext {
        public SchoolContext() : base() {

        }

        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Van> Vans { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<PayFee> PaysFees { get; set; }
        public DbSet<PayActivity> PayAcivities { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            Database.SetInitializer<SchoolContext>(new CreateDatabaseIfNotExists<SchoolContext>());
            modelBuilder.ComplexType<Location>();
            modelBuilder.Entity<Student>().Property(x => x.Location.CartesianX).HasColumnName("Location X");
            modelBuilder.Entity<Student>().Property(x => x.Location.CartesianY).HasColumnName("Location Y");

            modelBuilder.Entity<Student>()
                .HasMany(s => s.Activities)
                .WithMany()
                .Map(m =>{
                    m.ToTable("StudentActivities");
                    m.MapLeftKey("StudentId");
                    m.MapRightKey("ActivityId");
                });

            modelBuilder.Entity<Lesson>()
               .HasMany(s => s.Students)
               .WithMany()
               .Map(m => m.ToTable("StudentLessons"));

            modelBuilder.Entity<Lesson>()
               .HasMany(s => s.Teachers)
               .WithMany()
               .Map(m => m.ToTable("TeacherLessons"));

            modelBuilder.Entity<Lesson>()
                .HasRequired(l => l.Subject);

            base.OnModelCreating(modelBuilder);
        }
    }

}
