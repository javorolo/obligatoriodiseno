﻿using School.Controller;
using School.Logic;
using System;
using System.Windows.Forms;
using School.IControllers;

namespace UserInterface {

    public partial class PayFeeWindow : Form {

        private IControllerStudent controllerStudent;
        private WindowOfActionsOnStudents windowOfActionsOnStudents;
        private IControllerPayFee controllerPayFee;

        public PayFeeWindow(WindowOfActionsOnStudents wactionsStudents) {
            InitializeComponent();
            controllerPayFee = ControllerPayFee.Instance;
            controllerStudent = ControllerStudent.Instance;
            windowOfActionsOnStudents = wactionsStudents;
            this.fldMonth.Enabled = false;
            this.fldYear.Enabled = false;
            UpdateList();
        }

        private void UpdateList() {
            UpdateListStudents();
            UpdateMonthAndYear();
        }        

        private void UpdateListStudents() {
            this.lstStudents.DataSource = null;
            this.lstStudents.DataSource = controllerStudent.GetStudentTable();
        }

        private void UpdateMonthAndYear() {
            var student = this.lstStudents.SelectedItem;
            PayFee lastPayFee = controllerPayFee.GetLastPayFee(student);
            try { 
                if (!lastPayFee.Equals(null)) {
                    SetMonthAndYear(lastPayFee);
                }
            }
            catch {
                this.fldMonth.Text = "1";
                this.fldYear.Text = "2017";
            }
        }

        private void SetMonthAndYear(PayFee lastPayFee) {
            int month = (lastPayFee.Month + 1);
            int year = lastPayFee.Year;
            if (month == 13) {
                month = 1;
                year++;
            }
            this.fldMonth.Text = month.ToString();
            this.fldYear.Text = year.ToString();
        }

        private void LBstudents_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateMonthAndYear();
        }

        private void Bpay_Click(object sender, EventArgs e) {
            Student student = lstStudents.SelectedItem as Student;
            int month = Int32.Parse(this.fldMonth.Text);
            int year = Int32.Parse(this.fldYear.Text);
            controllerPayFee.AddPay(student,month,year);
            MessageBox.Show("The payment was made successfully");
            UpdateMonthAndYear();
        }

        private void Breturn_Click(object sender, EventArgs e) {
            this.Dispose();
            windowOfActionsOnStudents.Show();
        }
    }
}
