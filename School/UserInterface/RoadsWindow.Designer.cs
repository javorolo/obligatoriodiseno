﻿namespace UserInterface {
    partial class RoadsWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.BTreturn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CBorder = new System.Windows.Forms.ComboBox();
            this.LabelOrderBy = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // BTreturn
            // 
            this.BTreturn.Location = new System.Drawing.Point(25, 262);
            this.BTreturn.Name = "BTreturn";
            this.BTreturn.Size = new System.Drawing.Size(100, 23);
            this.BTreturn.TabIndex = 1;
            this.BTreturn.Text = "Return";
            this.BTreturn.UseVisualStyleBackColor = true;
            this.BTreturn.Click += new System.EventHandler(this.BTreturn_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(25, 50);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(715, 173);
            this.dataGridView1.TabIndex = 2;
            // 
            // CBorder
            // 
            this.CBorder.FormattingEnabled = true;
            this.CBorder.Items.AddRange(new object[] {
            "Ascendant distance",
            "Descendant distance",
            "Ascendant travels made",
            "Descendant travels made"});
            this.CBorder.Location = new System.Drawing.Point(75, 23);
            this.CBorder.Name = "CBorder";
            this.CBorder.Size = new System.Drawing.Size(162, 21);
            this.CBorder.TabIndex = 4;
            this.CBorder.SelectedIndexChanged += new System.EventHandler(this.CBorder_SelectedIndexChanged);
            // 
            // LabelOrderBy
            // 
            this.LabelOrderBy.AutoSize = true;
            this.LabelOrderBy.Location = new System.Drawing.Point(22, 26);
            this.LabelOrderBy.Name = "LabelOrderBy";
            this.LabelOrderBy.Size = new System.Drawing.Size(47, 13);
            this.LabelOrderBy.TabIndex = 5;
            this.LabelOrderBy.Text = "Order by";
            // 
            // RoadsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 300);
            this.Controls.Add(this.LabelOrderBy);
            this.Controls.Add(this.CBorder);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.BTreturn);
            this.Name = "RoadsWindow";
            this.Text = "RoadsWindow";
            this.Load += new System.EventHandler(this.RoadsWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BTreturn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox CBorder;
        private System.Windows.Forms.Label LabelOrderBy;
    }
}