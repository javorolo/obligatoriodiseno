﻿using School.Controller;
using School.Logic;
using System;
using System.Windows.Forms;
using School.IControllers;

namespace UserInterface {

    public partial class SubjectWindow : Form {

        private IControllerSubject controller;
        private WindowMainMenu windowMainMenu;

        public SubjectWindow(WindowMainMenu wmenu, String typeOfCall) {
            InitializeComponent();
            controller = ControllerSubject.Instance;
            windowMainMenu = wmenu;
            LoadTextNames(typeOfCall);
            UpdateList();
            ShowItems();
        }

        private void btnAction_Click(object sender, EventArgs e) {
            string name = fldName.Text.ToUpper();

            if (IsCreateCall()) {
                Subject subject = controller.NewSubject(name);
                AddSubject(subject);
            } else if (IsModifyCall()) {
                Subject oldSubject = lstSubject.SelectedItem as Subject;
                Subject changedSubject = controller.NewSubject(name);
                ModifySubject(oldSubject, changedSubject);
            } else if (IsDeleteCall()) {
                Subject subject = lstSubject.SelectedItem as Subject;
                DeleteSubject(subject);
            }
            UpdateList();
        }

        private void AddSubject(Subject subject) {
            if (CorrectSubject(subject)) {
                controller.AddSubject(subject);
                MessageBox.Show("The subject has been added into the system");
                ClearFields();
            }
        }

        private void ModifySubject(Subject oldSubject, Subject changedSubject) {
            if (CorrectSubject(changedSubject)) {
                controller.ModifySubject(oldSubject, changedSubject);
                MessageBox.Show("The subject has been changed");
                ClearFields();
            }
        }

        private void DeleteSubject(Subject subject) {
            controller.DeleteSubject(subject);
            MessageBox.Show("The subject has been deleted from the system");
            ClearFields();
            if (controller.IsEmptyList()) {
                CloseWindow();
            }
        }

        private bool CorrectSubject(Subject subject) {
            Subject aSubject = lstSubject.SelectedItem as Subject;
            bool ok = true;
            if (!controller.ValidNumberOfCharsInName(subject)) {
                MessageBox.Show("The name must be between 3 and 25 chars length");
                ok = false;
            } else if (IsModifyCall() && subject.Name == aSubject.Name) {
                MessageBox.Show("Try with other name, champ!");
                ok = false;
            }
            return ok;
        }

        private void LoadTextNames(string typeOfCall) {
            btnAction.Text = typeOfCall;
            lblTittle.Text = typeOfCall;
        }

        private void ShowItems() {
            if (IsCreateCall()) {
                lstSubject.Enabled = false;
                ClearFields();
            } else if (IsModifyCall()) {
                LoadSubjectData();
            } else if (IsDeleteCall()) {
                LoadSubjectData();
                fldName.Enabled = false;
            }
        }

        private void LoadSubjectData() {
            if (lstSubject.SelectedItem != null && !IsCreateCall()) {
                Subject subject = lstSubject.SelectedItem as Subject;
                fldName.Text = subject.Name;
                fldCode.Text = subject.Code;
            }
        }

        private bool IsCreateCall() {
            return btnAction.Text.Equals("Create Subject");
        }

        private bool IsModifyCall() {
            return btnAction.Text.Equals("Modify Subject");
        }

        private bool IsDeleteCall() {
            return btnAction.Text.Equals("Delete Subject");
        }

        private void ClearFields() {
            fldName.Text = "";
            fldCode.Text = "";
        }

        private void UpdateList() {
            this.lstSubject.DataSource = null;
            lstSubject.DataSource = controller.GetSubjectTable();
        }

        private void CloseWindow() {
            this.Hide();
            WindowOfActionsOnSubject window = new WindowOfActionsOnSubject(windowMainMenu);
            window.Show();
        }

        private void btnBack_Click(object sender, EventArgs e) {
            CloseWindow();
        }

        private void lstSubject_SelectedIndexChanged(object sender, EventArgs e) {
            LoadSubjectData();
        }
    }
}
