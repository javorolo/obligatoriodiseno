﻿namespace UserInterface {
    partial class ActivityWindow {

        private System.ComponentModel.IContainer components = null;
        
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.LblName = new System.Windows.Forms.Label();
            this.fldName = new System.Windows.Forms.TextBox();
            this.LblPrice = new System.Windows.Forms.Label();
            this.fldPrice = new System.Windows.Forms.NumericUpDown();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.btnBack = new System.Windows.Forms.Button();
            this.LblDate = new System.Windows.Forms.Label();
            this.lblTittle = new System.Windows.Forms.Label();
            this.btnAction = new System.Windows.Forms.Button();
            this.lstActivities = new System.Windows.Forms.ListBox();
            this.lblActivitiesList = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrice)).BeginInit();
            this.SuspendLayout();
            // 
            // LblName
            // 
            this.LblName.AutoSize = true;
            this.LblName.Location = new System.Drawing.Point(45, 80);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(35, 13);
            this.LblName.TabIndex = 0;
            this.LblName.Text = "Name";
            // 
            // fldName
            // 
            this.fldName.Location = new System.Drawing.Point(86, 77);
            this.fldName.Name = "fldName";
            this.fldName.Size = new System.Drawing.Size(137, 20);
            this.fldName.TabIndex = 1;
            // 
            // LblPrice
            // 
            this.LblPrice.AutoSize = true;
            this.LblPrice.Location = new System.Drawing.Point(45, 118);
            this.LblPrice.Name = "LblPrice";
            this.LblPrice.Size = new System.Drawing.Size(31, 13);
            this.LblPrice.TabIndex = 2;
            this.LblPrice.Text = "Price";
            // 
            // fldPrice
            // 
            this.fldPrice.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.fldPrice.Location = new System.Drawing.Point(86, 116);
            this.fldPrice.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.fldPrice.Name = "fldPrice";
            this.fldPrice.Size = new System.Drawing.Size(137, 20);
            this.fldPrice.TabIndex = 3;
            this.fldPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(86, 159);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(202, 20);
            this.dateTimePicker.TabIndex = 4;
            this.dateTimePicker.Value = new System.DateTime(2017, 11, 20, 0, 0, 0, 0);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(53, 240);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.button1_Click);
            // 
            // LblDate
            // 
            this.LblDate.AutoSize = true;
            this.LblDate.Location = new System.Drawing.Point(45, 165);
            this.LblDate.Name = "LblDate";
            this.LblDate.Size = new System.Drawing.Size(30, 13);
            this.LblDate.TabIndex = 6;
            this.LblDate.Text = "Date";
            // 
            // lblTittle
            // 
            this.lblTittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTittle.Location = new System.Drawing.Point(12, 6);
            this.lblTittle.Name = "lblTittle";
            this.lblTittle.Size = new System.Drawing.Size(484, 36);
            this.lblTittle.TabIndex = 18;
            this.lblTittle.Text = "Activity";
            this.lblTittle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAction
            // 
            this.btnAction.Location = new System.Drawing.Point(182, 240);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(75, 23);
            this.btnAction.TabIndex = 19;
            this.btnAction.Text = "Action";
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.BTaction_Click);
            // 
            // lstActivities
            // 
            this.lstActivities.FormattingEnabled = true;
            this.lstActivities.Location = new System.Drawing.Point(330, 83);
            this.lstActivities.Name = "lstActivities";
            this.lstActivities.Size = new System.Drawing.Size(145, 160);
            this.lstActivities.TabIndex = 20;
            this.lstActivities.SelectedIndexChanged += new System.EventHandler(this.LBactivitiesList_SelectedIndexChanged);
            // 
            // lblActivitiesList
            // 
            this.lblActivitiesList.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActivitiesList.Location = new System.Drawing.Point(330, 53);
            this.lblActivitiesList.Name = "lblActivitiesList";
            this.lblActivitiesList.Size = new System.Drawing.Size(145, 27);
            this.lblActivitiesList.TabIndex = 21;
            this.lblActivitiesList.Text = "Activities List";
            this.lblActivitiesList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ActivityWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 287);
            this.Controls.Add(this.lblActivitiesList);
            this.Controls.Add(this.lstActivities);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.lblTittle);
            this.Controls.Add(this.LblDate);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.fldPrice);
            this.Controls.Add(this.LblPrice);
            this.Controls.Add(this.fldName);
            this.Controls.Add(this.LblName);
            this.Name = "ActivityWindow";
            this.Text = "ActivityWindow";
            ((System.ComponentModel.ISupportInitialize)(this.fldPrice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblName;
        private System.Windows.Forms.TextBox fldName;
        private System.Windows.Forms.Label LblPrice;
        private System.Windows.Forms.NumericUpDown fldPrice;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label LblDate;
        private System.Windows.Forms.Label lblTittle;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.ListBox lstActivities;
        private System.Windows.Forms.Label lblActivitiesList;
    }
}