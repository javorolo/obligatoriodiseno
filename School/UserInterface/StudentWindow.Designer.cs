﻿namespace UserInterface {
    partial class StudentWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblName = new System.Windows.Forms.Label();
            this.fldName = new System.Windows.Forms.TextBox();
            this.fldLastName = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.fldIdentification = new System.Windows.Forms.TextBox();
            this.lblIdentification = new System.Windows.Forms.Label();
            this.btnAction = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblNumber = new System.Windows.Forms.Label();
            this.fldNumber = new System.Windows.Forms.TextBox();
            this.lblLocationX = new System.Windows.Forms.Label();
            this.lblLocationY = new System.Windows.Forms.Label();
            this.fldLocationX = new System.Windows.Forms.NumericUpDown();
            this.fldLocationY = new System.Windows.Forms.NumericUpDown();
            this.lblTittle = new System.Windows.Forms.Label();
            this.lblStudentSubjects = new System.Windows.Forms.Label();
            this.lstStudents = new System.Windows.Forms.ListBox();
            this.lblStudentsList = new System.Windows.Forms.Label();
            this.lstStudentSubjects = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocationX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocationY)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblName.Location = new System.Drawing.Point(51, 136);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(94, 20);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fldName
            // 
            this.fldName.Location = new System.Drawing.Point(151, 136);
            this.fldName.Name = "fldName";
            this.fldName.Size = new System.Drawing.Size(173, 20);
            this.fldName.TabIndex = 1;
            // 
            // fldLastName
            // 
            this.fldLastName.Location = new System.Drawing.Point(151, 181);
            this.fldLastName.Name = "fldLastName";
            this.fldLastName.Size = new System.Drawing.Size(173, 20);
            this.fldLastName.TabIndex = 2;
            // 
            // lblLastName
            // 
            this.lblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.Location = new System.Drawing.Point(51, 181);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(94, 20);
            this.lblLastName.TabIndex = 3;
            this.lblLastName.Text = "LastName";
            this.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fldIdentification
            // 
            this.fldIdentification.Location = new System.Drawing.Point(151, 227);
            this.fldIdentification.Name = "fldIdentification";
            this.fldIdentification.Size = new System.Drawing.Size(173, 20);
            this.fldIdentification.TabIndex = 4;
            // 
            // lblIdentification
            // 
            this.lblIdentification.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdentification.Location = new System.Drawing.Point(51, 227);
            this.lblIdentification.Name = "lblIdentification";
            this.lblIdentification.Size = new System.Drawing.Size(91, 20);
            this.lblIdentification.TabIndex = 5;
            this.lblIdentification.Text = "Identification";
            this.lblIdentification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAction
            // 
            this.btnAction.Location = new System.Drawing.Point(230, 331);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(94, 23);
            this.btnAction.TabIndex = 6;
            this.btnAction.Text = "Action";
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(51, 331);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(94, 23);
            this.btnBack.TabIndex = 7;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblNumber
            // 
            this.lblNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumber.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblNumber.Location = new System.Drawing.Point(51, 97);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(94, 20);
            this.lblNumber.TabIndex = 10;
            this.lblNumber.Text = "Student N°";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fldNumber
            // 
            this.fldNumber.Enabled = false;
            this.fldNumber.Location = new System.Drawing.Point(151, 97);
            this.fldNumber.Name = "fldNumber";
            this.fldNumber.Size = new System.Drawing.Size(173, 20);
            this.fldNumber.TabIndex = 11;
            // 
            // lblLocationX
            // 
            this.lblLocationX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocationX.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblLocationX.Location = new System.Drawing.Point(51, 267);
            this.lblLocationX.Name = "lblLocationX";
            this.lblLocationX.Size = new System.Drawing.Size(85, 20);
            this.lblLocationX.TabIndex = 12;
            this.lblLocationX.Text = "Location X";
            this.lblLocationX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLocationY
            // 
            this.lblLocationY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocationY.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblLocationY.Location = new System.Drawing.Point(205, 267);
            this.lblLocationY.Name = "lblLocationY";
            this.lblLocationY.Size = new System.Drawing.Size(77, 20);
            this.lblLocationY.TabIndex = 14;
            this.lblLocationY.Text = "Location Y";
            this.lblLocationY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fldLocationX
            // 
            this.fldLocationX.Location = new System.Drawing.Point(151, 267);
            this.fldLocationX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.fldLocationX.Name = "fldLocationX";
            this.fldLocationX.Size = new System.Drawing.Size(35, 20);
            this.fldLocationX.TabIndex = 15;
            // 
            // fldLocationY
            // 
            this.fldLocationY.Location = new System.Drawing.Point(288, 270);
            this.fldLocationY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.fldLocationY.Name = "fldLocationY";
            this.fldLocationY.Size = new System.Drawing.Size(36, 20);
            this.fldLocationY.TabIndex = 16;
            // 
            // lblTittle
            // 
            this.lblTittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTittle.Location = new System.Drawing.Point(51, 9);
            this.lblTittle.Name = "lblTittle";
            this.lblTittle.Size = new System.Drawing.Size(605, 55);
            this.lblTittle.TabIndex = 17;
            this.lblTittle.Text = "Student";
            this.lblTittle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStudentSubjects
            // 
            this.lblStudentSubjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStudentSubjects.Location = new System.Drawing.Point(395, 73);
            this.lblStudentSubjects.Name = "lblStudentSubjects";
            this.lblStudentSubjects.Size = new System.Drawing.Size(115, 21);
            this.lblStudentSubjects.TabIndex = 23;
            this.lblStudentSubjects.Text = "Student Subjects";
            this.lblStudentSubjects.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstStudents
            // 
            this.lstStudents.FormattingEnabled = true;
            this.lstStudents.Location = new System.Drawing.Point(530, 97);
            this.lstStudents.Name = "lstStudents";
            this.lstStudents.Size = new System.Drawing.Size(126, 225);
            this.lstStudents.Sorted = true;
            this.lstStudents.TabIndex = 22;
            this.lstStudents.SelectedIndexChanged += new System.EventHandler(this.lstStudents_SelectedIndexChanged);
            // 
            // lblStudentsList
            // 
            this.lblStudentsList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStudentsList.Location = new System.Drawing.Point(527, 73);
            this.lblStudentsList.Name = "lblStudentsList";
            this.lblStudentsList.Size = new System.Drawing.Size(129, 21);
            this.lblStudentsList.TabIndex = 19;
            this.lblStudentsList.Text = "Students List";
            this.lblStudentsList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstStudentSubjects
            // 
            this.lstStudentSubjects.FormattingEnabled = true;
            this.lstStudentSubjects.Location = new System.Drawing.Point(395, 97);
            this.lstStudentSubjects.Name = "lstStudentSubjects";
            this.lstStudentSubjects.Size = new System.Drawing.Size(115, 225);
            this.lstStudentSubjects.TabIndex = 18;
            // 
            // StudentWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 387);
            this.Controls.Add(this.lblStudentSubjects);
            this.Controls.Add(this.lstStudents);
            this.Controls.Add(this.lblStudentsList);
            this.Controls.Add(this.lstStudentSubjects);
            this.Controls.Add(this.lblTittle);
            this.Controls.Add(this.fldLocationY);
            this.Controls.Add(this.fldLocationX);
            this.Controls.Add(this.lblLocationY);
            this.Controls.Add(this.lblLocationX);
            this.Controls.Add(this.fldNumber);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.lblIdentification);
            this.Controls.Add(this.fldIdentification);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.fldLastName);
            this.Controls.Add(this.fldName);
            this.Controls.Add(this.lblName);
            this.Name = "StudentWindow";
            this.Text = "AddStudent";
            ((System.ComponentModel.ISupportInitialize)(this.fldLocationX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocationY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox fldName;
        private System.Windows.Forms.TextBox fldLastName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.TextBox fldIdentification;
        private System.Windows.Forms.Label lblIdentification;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.TextBox fldNumber;
        private System.Windows.Forms.Label lblLocationX;
        private System.Windows.Forms.Label lblLocationY;
        private System.Windows.Forms.NumericUpDown fldLocationX;
        private System.Windows.Forms.NumericUpDown fldLocationY;
        private System.Windows.Forms.Label lblTittle;
        private System.Windows.Forms.Label lblStudentSubjects;
        private System.Windows.Forms.ListBox lstStudents;
        private System.Windows.Forms.Label lblStudentsList;
        private System.Windows.Forms.ListBox lstStudentSubjects;
    }
}