﻿using School.Controller;
using School.Logic;
using System;
using System.Windows.Forms;
using School.IControllers;

namespace UserInterface {

    public partial class TeacherWindow : Form {

        private IControllerTeacher controllerTeacher;
        private IControllerLesson controllerLesson;
        private WindowOfActionsOnTeacher windowAction;

        public TeacherWindow(WindowOfActionsOnTeacher windowAction, string typeOfCall) {
            InitializeComponent();
            this.controllerTeacher = ControllerTeacher.Instance;
            this.controllerLesson = ControllerLesson.Instance;
            this.windowAction = windowAction;
            LoadTextNames(typeOfCall);
            UpdateTeacherList();
            ShowItems();
        }


        private void btnAction_Click(object sender, EventArgs e) {
            string name = fldName.Text.ToUpper();
            string lastName = fldLastName.Text.ToUpper();

            if(IsAddCall()) {
                Teacher teacher = controllerTeacher.NewTeacher(name, lastName);
                AddTeacher(teacher);
            } else if (IsModifyCall()) {
                Teacher oldTeacher = lstTeachers.SelectedItem as Teacher;
                Teacher changedTeacher = controllerTeacher.NewTeacher(name, lastName);
                ModifyTeacher(oldTeacher, changedTeacher);
            } else if (IsDeleteCall()) {
                Teacher teacher = lstTeachers.SelectedItem as Teacher;
                DeleteTeacher(teacher);   
            } 
            UpdateTeacherList();
        }

        private void btnBack_Click(object sender, EventArgs e) {
            CloseThisWindow();
        }

        private void AddTeacher(Teacher teacher) {
            if (CorrectTeacher(teacher)) {
                controllerTeacher.AddTeacher(teacher);
                MessageBox.Show("The teacher " + teacher.ToString() + " has been added into the system");
            }
        }

        private void ModifyTeacher(Teacher oldTeacher, Teacher changedTeacher) {
            if (CorrectTeacher(changedTeacher)) {
                string oldName = oldTeacher.Name;
                string oldLastName = oldTeacher.LastName;
                controllerTeacher.ModifyTeacher(oldTeacher, changedTeacher);
                MessageBox.Show("The teacher " + oldName + " " + oldLastName + 
                    " has been changed to " + changedTeacher.ToString());
            }
        }

        private void DeleteTeacher(Teacher teacher) {
            controllerTeacher.DeleteTeacher(teacher);
            MessageBox.Show("The teacher " + teacher.ToString() + " has been deleted");
            if (controllerTeacher.IsEmptyTable()) { 
                CloseThisWindow();
            }
        }

        private bool CorrectTeacher(Teacher teacher) {
            bool ok = true;
            if (!controllerTeacher.CorrectName(teacher)) {
                MessageBox.Show
                    ("The name and last name must have between 2 and 25 characters without numbers");
                ok = false;
            }
            return ok;
        }

        private void LoadTextNames(string typeOfCall) {
            btnAction.Text = typeOfCall;
            lblTittle.Text = typeOfCall;
        }

        private void ClearFields() {
            fldName.Text = "";
            fldLastName.Text = "";
        }

        private void UpdateTeacherList() {
            lstTeachers.DataSource = null;
            lstTeachers.DataSource = controllerTeacher.GetTeacherTable();
        }

        private void ShowItems() {
            if (IsAddCall()) {
                lstTeachers.Enabled = false;
                lblNumber.Visible = false;
                fldNumber.Visible = false;
                lstTeacherSubjects.Visible = false;
                lblTeacherSubjects.Visible = false;
                ClearFields();
            } else if (IsModifyCall()) {
                LoadTeacherData();
            } else {
                LoadTeacherData();
                fldName.Enabled = false;
                fldLastName.Enabled = false;
            }
        }

        private void LoadTeacherData() {
            if (lstTeachers.SelectedItem != null && !IsAddCall()) {
                Teacher teacher = lstTeachers.SelectedItem as Teacher;
                fldName.Text = teacher.Name;
                fldLastName.Text = teacher.LastName;
                fldNumber.Text = teacher.Id.ToString();
                LoadTeacherSubjects(teacher);
            }
        }

        private void LoadTeacherSubjects(Teacher teacher) {
            lstTeacherSubjects.DataSource = null;
            lstTeacherSubjects.DataSource = controllerLesson.GetTeacherInAllSubjects(teacher);
        }

        private bool IsAddCall() {
            return btnAction.Text == "Create Teacher";
        }

        private bool IsModifyCall() {
            return btnAction.Text == "Modify Teacher";
        }

        private bool IsDeleteCall() {
            return btnAction.Text == "Delete Teacher";
        }

        private void lstTeachers_SelectedIndexChanged(object sender, EventArgs e) {
            LoadTeacherData();
        }

        private void CloseThisWindow() {
            this.Hide();
            WindowOfActionsOnTeacher window = windowAction;
            window.Show();
        }
    }
}
