﻿namespace UserInterface {
    partial class WindowOfActionsOnStudents {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCreateStudent = new System.Windows.Forms.Button();
            this.btnModifyStudent = new System.Windows.Forms.Button();
            this.BTdeleteStudent = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.btnInscriptionToActivity = new System.Windows.Forms.Button();
            this.btnPayFee = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCreateStudent
            // 
            this.btnCreateStudent.Location = new System.Drawing.Point(72, 57);
            this.btnCreateStudent.Name = "btnCreateStudent";
            this.btnCreateStudent.Size = new System.Drawing.Size(181, 32);
            this.btnCreateStudent.TabIndex = 0;
            this.btnCreateStudent.Text = "Create Student";
            this.btnCreateStudent.UseVisualStyleBackColor = true;
            this.btnCreateStudent.Click += new System.EventHandler(this.AddStudentButton_Click);
            // 
            // btnModifyStudent
            // 
            this.btnModifyStudent.Location = new System.Drawing.Point(72, 110);
            this.btnModifyStudent.Name = "btnModifyStudent";
            this.btnModifyStudent.Size = new System.Drawing.Size(181, 32);
            this.btnModifyStudent.TabIndex = 1;
            this.btnModifyStudent.Text = "Modify Student";
            this.btnModifyStudent.UseVisualStyleBackColor = true;
            this.btnModifyStudent.Click += new System.EventHandler(this.modifyStudentButton_Click);
            // 
            // BTdeleteStudent
            // 
            this.BTdeleteStudent.Location = new System.Drawing.Point(72, 163);
            this.BTdeleteStudent.Name = "BTdeleteStudent";
            this.BTdeleteStudent.Size = new System.Drawing.Size(181, 32);
            this.BTdeleteStudent.TabIndex = 2;
            this.BTdeleteStudent.Text = "Delete Student";
            this.BTdeleteStudent.UseVisualStyleBackColor = true;
            this.BTdeleteStudent.Click += new System.EventHandler(this.BTdeleteStudent_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Location = new System.Drawing.Point(72, 325);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(181, 28);
            this.btnReturn.TabIndex = 3;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnInscriptionToActivity
            // 
            this.btnInscriptionToActivity.Location = new System.Drawing.Point(72, 218);
            this.btnInscriptionToActivity.Name = "btnInscriptionToActivity";
            this.btnInscriptionToActivity.Size = new System.Drawing.Size(181, 32);
            this.btnInscriptionToActivity.TabIndex = 4;
            this.btnInscriptionToActivity.Text = "Inscription to activity";
            this.btnInscriptionToActivity.UseVisualStyleBackColor = true;
            this.btnInscriptionToActivity.Click += new System.EventHandler(this.BTInscriptionToActivity_Click);
            // 
            // btnPayFee
            // 
            this.btnPayFee.Location = new System.Drawing.Point(72, 271);
            this.btnPayFee.Name = "btnPayFee";
            this.btnPayFee.Size = new System.Drawing.Size(181, 32);
            this.btnPayFee.TabIndex = 5;
            this.btnPayFee.Text = "Pay Fee";
            this.btnPayFee.UseVisualStyleBackColor = true;
            this.btnPayFee.Click += new System.EventHandler(this.LBPayFee_Click);
            // 
            // WindowOfActionsOnStudents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 377);
            this.Controls.Add(this.btnPayFee);
            this.Controls.Add(this.btnInscriptionToActivity);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.BTdeleteStudent);
            this.Controls.Add(this.btnModifyStudent);
            this.Controls.Add(this.btnCreateStudent);
            this.Name = "WindowOfActionsOnStudents";
            this.Text = "Student menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateStudent;
        private System.Windows.Forms.Button btnModifyStudent;
        private System.Windows.Forms.Button BTdeleteStudent;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Button btnInscriptionToActivity;
        private System.Windows.Forms.Button btnPayFee;
    }
}