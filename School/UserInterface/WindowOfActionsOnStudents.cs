﻿using School.Controller;
using School.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface {
    public partial class WindowOfActionsOnStudents : Form {

        private WindowMainMenu windowMainMenu;

        public WindowOfActionsOnStudents(WindowMainMenu aMainMenu) {
            InitializeComponent();
            windowMainMenu = aMainMenu;
        }


        private void AddStudentButton_Click(object sender, EventArgs e) {
            try {
                this.Hide();
                StudentWindow studentWindow = new StudentWindow(this, "Create Student");
                studentWindow.Show();
            }
            catch (Exception ex) {
                Console.WriteLine("" + ex);
            }
        }

        private void modifyStudentButton_Click(object sender, EventArgs e) {
            if (!ControllerStudent.Instance.IsEmptyList()) {
                StudentWindow studentWindow = new StudentWindow(this, "Modify Student");
                studentWindow.Show();
                this.Hide();
            }
            else {
                MessageBox.Show("There aren't students in the system ");
            }
        }

        private void BTdeleteStudent_Click(object sender, EventArgs e) {
            if (!ControllerStudent.Instance.IsEmptyList()) {
                StudentWindow studentWindow = new StudentWindow(this, "Delete Student");
                studentWindow.Show();
                this.Hide();
            }
            else {
                MessageBox.Show("There aren't students in the system ");
            }

        }

        private void button2_Click(object sender, EventArgs e) {
            this.Dispose();
            windowMainMenu.Show();
        }

        private void BTInscriptionToActivity_Click(object sender, EventArgs e) {
            if (!ControllerStudent.Instance.IsEmptyList()) {
                if (!ControllerActivity.Instance.IsEmptyActivities()) { 
                    IncriptionActivityWindow inscriptionWindow = new IncriptionActivityWindow(this);
                    inscriptionWindow.Show();
                    this.Hide();
                }
                else {
                    MessageBox.Show("There aren't activities in the system ");
                }
            }
            else {
                MessageBox.Show("There aren't students in the system ");
            }
        }

        private void LBPayFee_Click(object sender, EventArgs e){
            try {
                this.Hide();
                PayFeeWindow payFeeWindow = new PayFeeWindow(this);
                payFeeWindow.Show();
            }
            catch (Exception ex) {
                Console.WriteLine("" + ex);
            }
        }
    }
}
