﻿namespace UserInterface {
    partial class WindowMainMenu {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.BTmanageStudents = new System.Windows.Forms.Button();
            this.BTmanageTeachers = new System.Windows.Forms.Button();
            this.btnManageLessons = new System.Windows.Forms.Button();
            this.BTmanageVans = new System.Windows.Forms.Button();
            this.BTroads = new System.Windows.Forms.Button();
            this.btnManageSubjects = new System.Windows.Forms.Button();
            this.BTactivities = new System.Windows.Forms.Button();
            this.lblTittle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BTmanageStudents
            // 
            this.BTmanageStudents.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTmanageStudents.Location = new System.Drawing.Point(12, 89);
            this.BTmanageStudents.Name = "BTmanageStudents";
            this.BTmanageStudents.Size = new System.Drawing.Size(192, 51);
            this.BTmanageStudents.TabIndex = 0;
            this.BTmanageStudents.Text = "Manage Students";
            this.BTmanageStudents.UseVisualStyleBackColor = true;
            this.BTmanageStudents.Click += new System.EventHandler(this.BTmanageStudents_Click);
            // 
            // BTmanageTeachers
            // 
            this.BTmanageTeachers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTmanageTeachers.Location = new System.Drawing.Point(241, 89);
            this.BTmanageTeachers.Name = "BTmanageTeachers";
            this.BTmanageTeachers.Size = new System.Drawing.Size(192, 51);
            this.BTmanageTeachers.TabIndex = 1;
            this.BTmanageTeachers.Text = "Manage Teachers";
            this.BTmanageTeachers.UseVisualStyleBackColor = true;
            this.BTmanageTeachers.Click += new System.EventHandler(this.BTmanageTeachers_Click);
            // 
            // btnManageLessons
            // 
            this.btnManageLessons.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageLessons.Location = new System.Drawing.Point(241, 162);
            this.btnManageLessons.Name = "btnManageLessons";
            this.btnManageLessons.Size = new System.Drawing.Size(192, 51);
            this.btnManageLessons.TabIndex = 2;
            this.btnManageLessons.Text = "Manage Lessons";
            this.btnManageLessons.UseVisualStyleBackColor = true;
            this.btnManageLessons.Click += new System.EventHandler(this.btnManageLessons_Click);
            // 
            // BTmanageVans
            // 
            this.BTmanageVans.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTmanageVans.Location = new System.Drawing.Point(12, 238);
            this.BTmanageVans.Name = "BTmanageVans";
            this.BTmanageVans.Size = new System.Drawing.Size(192, 51);
            this.BTmanageVans.TabIndex = 3;
            this.BTmanageVans.Text = "Manage Vans";
            this.BTmanageVans.UseVisualStyleBackColor = true;
            this.BTmanageVans.Click += new System.EventHandler(this.BTmanageVans_Click);
            // 
            // BTroads
            // 
            this.BTroads.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTroads.Location = new System.Drawing.Point(241, 238);
            this.BTroads.Name = "BTroads";
            this.BTroads.Size = new System.Drawing.Size(192, 51);
            this.BTroads.TabIndex = 4;
            this.BTroads.Text = "Manage Roads";
            this.BTroads.UseVisualStyleBackColor = true;
            this.BTroads.Click += new System.EventHandler(this.BTroads_Click);
            // 
            // btnManageSubjects
            // 
            this.btnManageSubjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageSubjects.Location = new System.Drawing.Point(12, 162);
            this.btnManageSubjects.Name = "btnManageSubjects";
            this.btnManageSubjects.Size = new System.Drawing.Size(192, 51);
            this.btnManageSubjects.TabIndex = 6;
            this.btnManageSubjects.Text = "Manage Subjects";
            this.btnManageSubjects.UseVisualStyleBackColor = true;
            this.btnManageSubjects.Click += new System.EventHandler(this.btnManageSubjects_Click);
            // 
            // BTactivities
            // 
            this.BTactivities.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.BTactivities.Location = new System.Drawing.Point(12, 313);
            this.BTactivities.Name = "BTactivities";
            this.BTactivities.Size = new System.Drawing.Size(192, 51);
            this.BTactivities.TabIndex = 7;
            this.BTactivities.Text = "Manage Activities";
            this.BTactivities.UseVisualStyleBackColor = true;
            this.BTactivities.Click += new System.EventHandler(this.BTactivities_Click);
            // 
            // lblTittle
            // 
            this.lblTittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTittle.Location = new System.Drawing.Point(12, 9);
            this.lblTittle.Name = "lblTittle";
            this.lblTittle.Size = new System.Drawing.Size(421, 53);
            this.lblTittle.TabIndex = 8;
            this.lblTittle.Text = "School Management";
            this.lblTittle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WindowMainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 402);
            this.Controls.Add(this.lblTittle);
            this.Controls.Add(this.BTactivities);
            this.Controls.Add(this.btnManageSubjects);
            this.Controls.Add(this.BTroads);
            this.Controls.Add(this.BTmanageVans);
            this.Controls.Add(this.btnManageLessons);
            this.Controls.Add(this.BTmanageTeachers);
            this.Controls.Add(this.BTmanageStudents);
            this.Name = "WindowMainMenu";
            this.Text = "Main Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BTmanageStudents;
        private System.Windows.Forms.Button BTmanageTeachers;
        private System.Windows.Forms.Button btnManageLessons;
        private System.Windows.Forms.Button BTmanageVans;
        private System.Windows.Forms.Button BTroads;
        private System.Windows.Forms.Button btnManageSubjects;
        private System.Windows.Forms.Button BTactivities;
        private System.Windows.Forms.Label lblTittle;
    }
}