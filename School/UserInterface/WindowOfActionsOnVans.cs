﻿using School.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface {
    public partial class WindowOfActionsOnVans : Form {

        private WindowMainMenu windowMainMenu;

        public WindowOfActionsOnVans(WindowMainMenu wm) {
            InitializeComponent();
            windowMainMenu = wm;
        }

        private void buttonAddVan_Click(object sender, EventArgs e) {
            try {
                this.Hide();
                VanWindow vanWindow = new VanWindow(windowMainMenu, "Create Van");
                vanWindow.Show();
                
            }
            catch (Exception ex) {
                Console.WriteLine("" + ex);
            }

        }

        private void buttonModifyVan_Click(object sender, EventArgs e) {
            if (!ControllerVan.Instance.IsEmptyTable()) {
                VanWindow vanWindow = new VanWindow(windowMainMenu, "Modify Van");
                vanWindow.Show();
                this.Hide();
            }else {
                MessageBox.Show("There aren't vans in the system ");
            }
        }

        private void buttonDeleteVan_Click(object sender, EventArgs e) {
            if (!ControllerVan.Instance.IsEmptyTable()) {
                VanWindow vanWindow = new VanWindow(windowMainMenu, "Delete Van");
                vanWindow.Show();
                this.Hide();
            } else {
                MessageBox.Show("There aren't vans in the system ");
            }
        }

        private void BTreturnToMainMenu_Click(object sender, EventArgs e) {
            this.Dispose();
            windowMainMenu.Show();
        }
    }
}
