﻿namespace UserInterface {
    partial class LessonWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTittle = new System.Windows.Forms.Label();
            this.fldCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnAction = new System.Windows.Forms.Button();
            this.lblStudentsInLessons = new System.Windows.Forms.Label();
            this.lstStudentsInLesson = new System.Windows.Forms.ListBox();
            this.lblStudentsList = new System.Windows.Forms.Label();
            this.lstStudents = new System.Windows.Forms.ListBox();
            this.lstTeachers = new System.Windows.Forms.ListBox();
            this.lblTeachersInLessons = new System.Windows.Forms.Label();
            this.lstTeachersLesson = new System.Windows.Forms.ListBox();
            this.lblTeachers = new System.Windows.Forms.Label();
            this.btnRemoveStudent = new System.Windows.Forms.Button();
            this.btnAddStudent = new System.Windows.Forms.Button();
            this.btnRemoveTeacher = new System.Windows.Forms.Button();
            this.btnAddTeacher = new System.Windows.Forms.Button();
            this.lstLessons = new System.Windows.Forms.ListBox();
            this.lblLessons = new System.Windows.Forms.Label();
            this.fldSubject = new System.Windows.Forms.TextBox();
            this.lblSubject = new System.Windows.Forms.Label();
            this.lstSubjects = new System.Windows.Forms.ListBox();
            this.lblSubjects = new System.Windows.Forms.Label();
            this.btnSelectSubject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTittle
            // 
            this.lblTittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTittle.Location = new System.Drawing.Point(15, 9);
            this.lblTittle.Name = "lblTittle";
            this.lblTittle.Size = new System.Drawing.Size(789, 55);
            this.lblTittle.TabIndex = 18;
            this.lblTittle.Text = "Lesson";
            this.lblTittle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fldCode
            // 
            this.fldCode.Enabled = false;
            this.fldCode.Location = new System.Drawing.Point(15, 124);
            this.fldCode.Name = "fldCode";
            this.fldCode.ReadOnly = true;
            this.fldCode.Size = new System.Drawing.Size(91, 20);
            this.fldCode.TabIndex = 22;
            // 
            // lblCode
            // 
            this.lblCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCode.Location = new System.Drawing.Point(12, 101);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(94, 20);
            this.lblCode.TabIndex = 21;
            this.lblCode.Text = "Code";
            this.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(12, 355);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(91, 23);
            this.btnBack.TabIndex = 24;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnAction
            // 
            this.btnAction.Location = new System.Drawing.Point(117, 355);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(91, 23);
            this.btnAction.TabIndex = 23;
            this.btnAction.Text = "Action";
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // lblStudentsInLessons
            // 
            this.lblStudentsInLessons.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStudentsInLessons.Location = new System.Drawing.Point(358, 64);
            this.lblStudentsInLessons.Name = "lblStudentsInLessons";
            this.lblStudentsInLessons.Size = new System.Drawing.Size(96, 34);
            this.lblStudentsInLessons.TabIndex = 28;
            this.lblStudentsInLessons.Text = "Students In Lessons";
            this.lblStudentsInLessons.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstStudentsInLesson
            // 
            this.lstStudentsInLesson.FormattingEnabled = true;
            this.lstStudentsInLesson.Location = new System.Drawing.Point(358, 101);
            this.lstStudentsInLesson.Name = "lstStudentsInLesson";
            this.lstStudentsInLesson.Size = new System.Drawing.Size(96, 199);
            this.lstStudentsInLesson.Sorted = true;
            this.lstStudentsInLesson.TabIndex = 27;
            // 
            // lblStudentsList
            // 
            this.lblStudentsList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStudentsList.Location = new System.Drawing.Point(481, 75);
            this.lblStudentsList.Name = "lblStudentsList";
            this.lblStudentsList.Size = new System.Drawing.Size(95, 23);
            this.lblStudentsList.TabIndex = 26;
            this.lblStudentsList.Text = "Students";
            this.lblStudentsList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstStudents
            // 
            this.lstStudents.FormattingEnabled = true;
            this.lstStudents.Location = new System.Drawing.Point(481, 101);
            this.lstStudents.Name = "lstStudents";
            this.lstStudents.Size = new System.Drawing.Size(95, 199);
            this.lstStudents.Sorted = true;
            this.lstStudents.TabIndex = 29;
            // 
            // lstTeachers
            // 
            this.lstTeachers.FormattingEnabled = true;
            this.lstTeachers.Location = new System.Drawing.Point(713, 101);
            this.lstTeachers.Name = "lstTeachers";
            this.lstTeachers.Size = new System.Drawing.Size(91, 199);
            this.lstTeachers.Sorted = true;
            this.lstTeachers.TabIndex = 33;
            // 
            // lblTeachersInLessons
            // 
            this.lblTeachersInLessons.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeachersInLessons.Location = new System.Drawing.Point(596, 64);
            this.lblTeachersInLessons.Name = "lblTeachersInLessons";
            this.lblTeachersInLessons.Size = new System.Drawing.Size(94, 34);
            this.lblTeachersInLessons.TabIndex = 32;
            this.lblTeachersInLessons.Text = "Teachers In Lessons";
            this.lblTeachersInLessons.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstTeachersLesson
            // 
            this.lstTeachersLesson.FormattingEnabled = true;
            this.lstTeachersLesson.Location = new System.Drawing.Point(599, 101);
            this.lstTeachersLesson.Name = "lstTeachersLesson";
            this.lstTeachersLesson.Size = new System.Drawing.Size(94, 199);
            this.lstTeachersLesson.Sorted = true;
            this.lstTeachersLesson.TabIndex = 31;
            // 
            // lblTeachers
            // 
            this.lblTeachers.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeachers.Location = new System.Drawing.Point(713, 75);
            this.lblTeachers.Name = "lblTeachers";
            this.lblTeachers.Size = new System.Drawing.Size(88, 23);
            this.lblTeachers.TabIndex = 30;
            this.lblTeachers.Text = "Teachers";
            this.lblTeachers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRemoveStudent
            // 
            this.btnRemoveStudent.Location = new System.Drawing.Point(358, 306);
            this.btnRemoveStudent.Name = "btnRemoveStudent";
            this.btnRemoveStudent.Size = new System.Drawing.Size(96, 23);
            this.btnRemoveStudent.TabIndex = 34;
            this.btnRemoveStudent.Text = "Remove >>";
            this.btnRemoveStudent.UseVisualStyleBackColor = true;
            this.btnRemoveStudent.Click += new System.EventHandler(this.btnRemoveStudent_Click);
            // 
            // btnAddStudent
            // 
            this.btnAddStudent.Location = new System.Drawing.Point(481, 306);
            this.btnAddStudent.Name = "btnAddStudent";
            this.btnAddStudent.Size = new System.Drawing.Size(95, 23);
            this.btnAddStudent.TabIndex = 35;
            this.btnAddStudent.Text = "<< Add";
            this.btnAddStudent.UseVisualStyleBackColor = true;
            this.btnAddStudent.Click += new System.EventHandler(this.btnAddStudent_Click);
            // 
            // btnRemoveTeacher
            // 
            this.btnRemoveTeacher.Location = new System.Drawing.Point(596, 306);
            this.btnRemoveTeacher.Name = "btnRemoveTeacher";
            this.btnRemoveTeacher.Size = new System.Drawing.Size(94, 23);
            this.btnRemoveTeacher.TabIndex = 36;
            this.btnRemoveTeacher.Text = "Remove >>";
            this.btnRemoveTeacher.UseVisualStyleBackColor = true;
            this.btnRemoveTeacher.Click += new System.EventHandler(this.btnRemoveTeacher_Click);
            // 
            // btnAddTeacher
            // 
            this.btnAddTeacher.Location = new System.Drawing.Point(713, 306);
            this.btnAddTeacher.Name = "btnAddTeacher";
            this.btnAddTeacher.Size = new System.Drawing.Size(91, 23);
            this.btnAddTeacher.TabIndex = 37;
            this.btnAddTeacher.Text = "<< Add";
            this.btnAddTeacher.UseVisualStyleBackColor = true;
            this.btnAddTeacher.Click += new System.EventHandler(this.btnAddTeacher_Click);
            // 
            // lstLessons
            // 
            this.lstLessons.FormattingEnabled = true;
            this.lstLessons.Location = new System.Drawing.Point(112, 101);
            this.lstLessons.Name = "lstLessons";
            this.lstLessons.Size = new System.Drawing.Size(96, 199);
            this.lstLessons.Sorted = true;
            this.lstLessons.TabIndex = 38;
            this.lstLessons.SelectedIndexChanged += new System.EventHandler(this.lstLessons_SelectedIndexChanged);
            // 
            // lblLessons
            // 
            this.lblLessons.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLessons.Location = new System.Drawing.Point(113, 75);
            this.lblLessons.Name = "lblLessons";
            this.lblLessons.Size = new System.Drawing.Size(95, 23);
            this.lblLessons.TabIndex = 39;
            this.lblLessons.Text = "Lessons";
            this.lblLessons.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fldSubject
            // 
            this.fldSubject.Enabled = false;
            this.fldSubject.Location = new System.Drawing.Point(15, 191);
            this.fldSubject.Name = "fldSubject";
            this.fldSubject.ReadOnly = true;
            this.fldSubject.Size = new System.Drawing.Size(91, 20);
            this.fldSubject.TabIndex = 40;
            // 
            // lblSubject
            // 
            this.lblSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubject.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblSubject.Location = new System.Drawing.Point(12, 157);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(94, 20);
            this.lblSubject.TabIndex = 41;
            this.lblSubject.Text = "Subject";
            this.lblSubject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lstSubjects
            // 
            this.lstSubjects.FormattingEnabled = true;
            this.lstSubjects.Location = new System.Drawing.Point(236, 101);
            this.lstSubjects.Name = "lstSubjects";
            this.lstSubjects.Size = new System.Drawing.Size(96, 199);
            this.lstSubjects.Sorted = true;
            this.lstSubjects.TabIndex = 42;
            // 
            // lblSubjects
            // 
            this.lblSubjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubjects.Location = new System.Drawing.Point(237, 75);
            this.lblSubjects.Name = "lblSubjects";
            this.lblSubjects.Size = new System.Drawing.Size(95, 23);
            this.lblSubjects.TabIndex = 43;
            this.lblSubjects.Text = "Subjects";
            this.lblSubjects.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSelectSubject
            // 
            this.btnSelectSubject.Location = new System.Drawing.Point(236, 306);
            this.btnSelectSubject.Name = "btnSelectSubject";
            this.btnSelectSubject.Size = new System.Drawing.Size(95, 23);
            this.btnSelectSubject.TabIndex = 44;
            this.btnSelectSubject.Text = "Select";
            this.btnSelectSubject.UseVisualStyleBackColor = true;
            this.btnSelectSubject.Click += new System.EventHandler(this.btnSelectSubject_Click);
            // 
            // LessonWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 407);
            this.Controls.Add(this.btnSelectSubject);
            this.Controls.Add(this.lblSubjects);
            this.Controls.Add(this.lstSubjects);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.fldSubject);
            this.Controls.Add(this.lblLessons);
            this.Controls.Add(this.lstLessons);
            this.Controls.Add(this.btnAddTeacher);
            this.Controls.Add(this.btnRemoveTeacher);
            this.Controls.Add(this.btnAddStudent);
            this.Controls.Add(this.btnRemoveStudent);
            this.Controls.Add(this.lstTeachers);
            this.Controls.Add(this.lblTeachersInLessons);
            this.Controls.Add(this.lstTeachersLesson);
            this.Controls.Add(this.lblTeachers);
            this.Controls.Add(this.lstStudents);
            this.Controls.Add(this.lblStudentsInLessons);
            this.Controls.Add(this.lstStudentsInLesson);
            this.Controls.Add(this.lblStudentsList);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.fldCode);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.lblTittle);
            this.Name = "LessonWindow";
            this.Text = "SubjectWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTittle;
        private System.Windows.Forms.TextBox fldCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.Label lblStudentsInLessons;
        private System.Windows.Forms.ListBox lstStudentsInLesson;
        private System.Windows.Forms.Label lblStudentsList;
        private System.Windows.Forms.ListBox lstStudents;
        private System.Windows.Forms.ListBox lstTeachers;
        private System.Windows.Forms.Label lblTeachersInLessons;
        private System.Windows.Forms.ListBox lstTeachersLesson;
        private System.Windows.Forms.Label lblTeachers;
        private System.Windows.Forms.Button btnRemoveStudent;
        private System.Windows.Forms.Button btnAddStudent;
        private System.Windows.Forms.Button btnRemoveTeacher;
        private System.Windows.Forms.Button btnAddTeacher;
        private System.Windows.Forms.ListBox lstLessons;
        private System.Windows.Forms.Label lblLessons;
        private System.Windows.Forms.TextBox fldSubject;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.ListBox lstSubjects;
        private System.Windows.Forms.Label lblSubjects;
        private System.Windows.Forms.Button btnSelectSubject;
    }
}