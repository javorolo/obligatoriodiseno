﻿using School.Controller;
using School.Logic;
using System;
using System.Windows.Forms;
using School.IControllers;

namespace UserInterface {

    public partial class StudentWindow : Form {

        private IControllerStudent controllerStudent;
        private IControllerLesson controllerLesson;
        private WindowOfActionsOnStudents windosActionsStudents;

        public StudentWindow(WindowOfActionsOnStudents windowStudent, string typeOfCall) {
            InitializeComponent();
            controllerStudent = ControllerStudent.Instance;
            controllerLesson = ControllerLesson.Instance;
            windosActionsStudents = windowStudent;
            LoadTextNames(typeOfCall);           
            UpdateStudentList();
            ShowItems();
        }


        private void btnBack_Click(object sender, EventArgs e) {
            CloseWindow();
        }

        private void btnAction_Click(object sender, EventArgs e) {
            string name = fldName.Text.ToUpper();
            string lastName = fldLastName.Text.ToUpper();
            string id = fldIdentification.Text;
            int cartesianX = (int)fldLocationX.Value;
            int cartesianY = (int)fldLocationY.Value;

            if (IsAddCall()) {
                Student student = controllerStudent.NewStudent(name, lastName, id);
                LoadLocation(student, cartesianX, cartesianY);
                AddStudent(student);
            }
            else if (IsModifyCall()) {
                Student oldStudent = lstStudents.SelectedItem as Student;
                Student changedStudent = controllerStudent.NewStudent(name, lastName, id);
                LoadLocation(changedStudent, cartesianX, cartesianY);
                ModifyStudent(oldStudent, changedStudent);
                
            }
            else if (IsDeleteCall()) {
                Student student = lstStudents.SelectedItem as Student;
                DeleteStudent(student);
            }
            UpdateStudentList();
        }

        private void ClearFields() {
            this.fldNumber.Text = "";
            this.fldName.Text = "";
            this.fldLastName.Text = "";
            this.fldIdentification.Text = "";
            this.fldLocationX.Value = 0;
            this.fldLocationY.Value = 0;
        }

        private void AddStudent(Student aStudent) {
            if (CorrectStudent(aStudent)) {
                controllerStudent.AddStudent(aStudent);
                ClearFields();
                MessageBox.Show("The student has been added into the system");
            }
        }

        private void ModifyStudent(Student oldStudent, Student changedStudent) {
            if (CorrectStudent(changedStudent)) {
                string oldName = oldStudent.Name;
                string oldLastName = oldStudent.LastName;
                controllerStudent.ModifyStudent(oldStudent, changedStudent);
                ClearFields();
                MessageBox.Show("The student "+ oldName + " " + oldLastName + " has been changed");
            }
        }

        private void DeleteStudent(Student student) {
            controllerStudent.DeleteStudent(student);
            MessageBox.Show("The student has been deleted");
            ClearFields();
            if (controllerStudent.IsEmptyList()) {
                CloseWindow();
            }     
        }

        private void LoadStudentData() {
            if (lstStudents.SelectedItem != null) {
                Student student = lstStudents.SelectedItem as Student;
                fldNumber.Text = student.StudentId.ToString();
                fldName.Text = student.Name;
                fldLastName.Text = student.LastName;
                fldIdentification.Text = student.Identification;
                fldLocationX.Value = student.Location.CartesianX;
                fldLocationY.Value = student.Location.CartesianY;
                loadStudentSubjects(student);
            }
        }

        private void loadStudentSubjects(Student student) {
            lstStudentSubjects.DataSource = null;
            lstStudentSubjects.DataSource = controllerLesson.GetStudentsInAllSubjects(student);
        }

        private void UpdateStudentList() {
            this.lstStudents.DataSource = null;
            lstStudents.DataSource = controllerStudent.GetStudentTable();
        }

        private void LoadTextNames(string typeOfCall) {
            btnAction.Text = typeOfCall;
            lblTittle.Text = typeOfCall;
        }

        private void ShowItems() {
            lstStudentSubjects.Enabled = false;
            if (IsAddCall()) {
                lblNumber.Visible = false;
                fldNumber.Visible = false;
                lstStudents.Enabled = false;
                lstStudentSubjects.Visible = false;
                lblStudentSubjects.Visible = false;
                ClearFields();
            } else if (IsModifyCall()) {
                LoadStudentData();
            } else if (IsDeleteCall()){
                LoadStudentData();
                fldIdentification.Enabled = false;
                fldName.Enabled = false;
                fldLastName.Enabled = false;
                fldLocationX.Enabled = false;
                fldLocationY.Enabled = false;
            }
        }

        private bool CorrectStudent(Student aStudent) {            
            bool isOk = true;
            if (!IdentificationFormat(aStudent)) {
                this.fldIdentification.Text = "";
                isOk = false;
            } else if (SameStudent(aStudent)) {
                this.fldIdentification.Text = "";
                isOk = false;
            } else if (!CorrectName(aStudent)) {
                this.fldName.Text = "";
                this.fldLastName.Text = "";
                isOk = false;
            } else if (!CorrectPosition(aStudent)) {
                isOk = false;
                fldLocationX.Value = 0;
                fldLocationY.Value = 0;
            }
            return isOk;
        }

        private bool IdentificationFormat(Student student) {
            bool isOk = true;
            if (!controllerStudent.IsIdentificationFormatOk(student)) {
                MessageBox.Show("The format of the identification must be: 1234567-8 formar for example");
                isOk = false;
            }
            return isOk;
        }

        private bool SameStudent(Student student) {
            bool isOk = false;
            if (controllerStudent.IsInStudentTable(student) && IsAddCall()) {
                MessageBox.Show("The student " + student.Identification.ToString() +
                    " already exists in the system");
                isOk = true;
            }
            return isOk;
        }

        private bool CorrectName(Student aStudent) {
            bool isOk = true;
            if (!controllerStudent.CorrectName(aStudent)) {
                MessageBox.Show("The name and last name must be " +
                    "have between 2 and 25 characters, without numbers");
                isOk = false;
            }
            return isOk;
        }

        private bool CorrectPosition(Student student) {
            bool isOk = true;
            if (!controllerStudent.CorrectStudentLocation(student)) {
                MessageBox.Show("The X and Y location must be between -100 and 100. " +
                    "Also should not be in the position of the school (0,0)");
                isOk = false;
            }
            return isOk;
        }

        private void LoadLocation(Student student, int cartesianX, int cartesianY) {
            student.Location.CartesianX = cartesianX;
            student.Location.CartesianY = cartesianY;
        }

        private bool IsAddCall() {
            return btnAction.Text.Equals("Create Student");
        }

        private bool IsModifyCall() {
            return btnAction.Text.Equals("Modify Student");
        }

        private bool IsDeleteCall() {
            return btnAction.Text.Equals("Delete Student");
        }

        private void lstStudents_SelectedIndexChanged(object sender, EventArgs e) {
            if (!IsAddCall()){
                LoadStudentData();
            }    
        }

        private void CloseWindow() {
            this.Hide();
            WindowOfActionsOnStudents wstudent = windosActionsStudents;
            wstudent.Show();
        }
    }
}
