﻿using School.Controller;
using System;
using System.Windows.Forms;
using School.IControllers;

namespace UserInterface {

    public class Program {

        [STAThread]
         static void Main(string[] args) {

            IControllerVan controllerVan = ControllerVan.Instance;
            IControllerStudent controllerStudent = ControllerStudent.Instance;
            IControllerLesson controllerLesson = ControllerLesson.Instance;
            IControllerTeacher controllerTeacher = ControllerTeacher.Instance;
            IControllerSubject controllerSubject = ControllerSubject.Instance;
            IControllerActivity controllerActivity = ControllerActivity.Instance;
            IControllerPayActivity controllerPayActivity = ControllerPayActivity.Instance;
            IControllerPayFee controllerPayFee = ControllerPayFee.Instance;
            IControllerRoad road = ControllerRoad.Instance;
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new WindowMainMenu());
            
        }
    }
}
