﻿using School.Controller;
using School.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using School.IControllers;

namespace UserInterface {

    public partial class IncriptionActivityWindow : Form {

        private IControllerActivity controllerActivity;
        private IControllerStudent controllerStudent;
        private IControllerPayActivity controllerPayActivity;
        private WindowOfActionsOnStudents wActionsStudent;

        public IncriptionActivityWindow(WindowOfActionsOnStudents actionsStudent) {
            InitializeComponent();
            controllerActivity = ControllerActivity.Instance;
            controllerStudent = ControllerStudent.Instance;
            controllerPayActivity = ControllerPayActivity.Instance;
            wActionsStudent = actionsStudent;
            this.LBStudentActivities.Enabled = false;
            UpdateList();
            RefreshActivities();
        }

        private void BTaddActivity_Click(object sender, EventArgs e) {
            if (AreSelectedItems()) {
                var student = this.LBstudents.SelectedItem;
                var activity = this.LBactivities.SelectedItem;
                controllerStudent.AddActivityToStudent(student, activity);
                controllerPayActivity.AddPayActivity(student, activity);
                MessageBox.Show("The student " + student.ToString() + " has been inscripted in the activity " + activity.ToString());
                CloseWindow();
            } else {
                MessageBox.Show("First, you must select a student and activity to inscribe into an activity");
            }
        }

        private void UpdateList() {
            UpdateListStudents();
            UpdateListStudentActivities();
            UpdateListActivities();
        }

        private void RefreshActivities() {
            List<Activity> studentActivities = (List<Activity>)this.LBStudentActivities.DataSource;
            List<Activity> activities = (List<Activity>)this.LBactivities.DataSource;
            for (int i = 0; i < studentActivities.Count; i++) {
                if (activities.Contains(studentActivities.ElementAt(i))) {
                    activities.Remove(studentActivities.ElementAt(i));
                }
            }
            this.LBactivities.DataSource = null;
            this.LBactivities.DataSource = activities;
        }



        private void UpdateListStudentActivities() {
            this.LBStudentActivities.DataSource = null;
            var student = this.LBstudents.SelectedItem;
            this.LBStudentActivities.DataSource = controllerStudent.GetActivities(student);
        }

        private void UpdateListActivities() {
            this.LBactivities.DataSource = null;
            this.LBactivities.DataSource = controllerActivity.GetActivitiesList();
        }

        private void UpdateListStudents() {
            this.LBstudents.DataSource = null;
            this.LBstudents.DataSource = controllerStudent.GetStudentTable();
        }

        private bool AreSelectedItems() {
            var student = this.LBstudents.SelectedItem;
            var activity = this.LBactivities.SelectedItem;
            return (student != null) && (activity != null);
        }

        private void LBactivities_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateListStudentActivities();
            UpdateListActivities();
            RefreshActivities();
        }

        private void BTreturn_Click(object sender, EventArgs e) {
            CloseWindow();
        }

        private void CloseWindow() {
            this.Hide();
            wActionsStudent.Show();
        }

    }
}
