﻿namespace UserInterface {
    partial class VanWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.lblBadge = new System.Windows.Forms.Label();
            this.lblCapacity = new System.Windows.Forms.Label();
            this.fldCapacity = new System.Windows.Forms.NumericUpDown();
            this.lblVansList = new System.Windows.Forms.Label();
            this.lblTittle = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnAction = new System.Windows.Forms.Button();
            this.fldBadge = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lstVans = new System.Windows.Forms.ListBox();
            this.fldConsumption = new System.Windows.Forms.NumericUpDown();
            this.lblConsumption = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fldCapacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldConsumption)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBadge
            // 
            this.lblBadge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBadge.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBadge.Location = new System.Drawing.Point(33, 98);
            this.lblBadge.Name = "lblBadge";
            this.lblBadge.Size = new System.Drawing.Size(94, 20);
            this.lblBadge.TabIndex = 14;
            this.lblBadge.Text = "Badge";
            this.lblBadge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCapacity
            // 
            this.lblCapacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCapacity.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCapacity.Location = new System.Drawing.Point(33, 135);
            this.lblCapacity.Name = "lblCapacity";
            this.lblCapacity.Size = new System.Drawing.Size(94, 20);
            this.lblCapacity.TabIndex = 12;
            this.lblCapacity.Text = "Capacity";
            this.lblCapacity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fldCapacity
            // 
            this.fldCapacity.Location = new System.Drawing.Point(133, 138);
            this.fldCapacity.Name = "fldCapacity";
            this.fldCapacity.Size = new System.Drawing.Size(53, 20);
            this.fldCapacity.TabIndex = 16;
            this.fldCapacity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblVansList
            // 
            this.lblVansList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVansList.Location = new System.Drawing.Point(287, 74);
            this.lblVansList.Name = "lblVansList";
            this.lblVansList.Size = new System.Drawing.Size(151, 21);
            this.lblVansList.TabIndex = 21;
            this.lblVansList.Text = "Van List";
            this.lblVansList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTittle
            // 
            this.lblTittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTittle.Location = new System.Drawing.Point(36, 9);
            this.lblTittle.Name = "lblTittle";
            this.lblTittle.Size = new System.Drawing.Size(402, 55);
            this.lblTittle.TabIndex = 22;
            this.lblTittle.Text = "Van";
            this.lblTittle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(36, 248);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(94, 23);
            this.btnBack.TabIndex = 24;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnAction
            // 
            this.btnAction.Location = new System.Drawing.Point(166, 248);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(94, 23);
            this.btnAction.TabIndex = 23;
            this.btnAction.Text = "Action";
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // fldBadge
            // 
            this.fldBadge.Location = new System.Drawing.Point(133, 98);
            this.fldBadge.Name = "fldBadge";
            this.fldBadge.Size = new System.Drawing.Size(53, 20);
            this.fldBadge.TabIndex = 25;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // lstVans
            // 
            this.lstVans.FormattingEnabled = true;
            this.lstVans.Location = new System.Drawing.Point(287, 98);
            this.lstVans.Name = "lstVans";
            this.lstVans.Size = new System.Drawing.Size(151, 173);
            this.lstVans.TabIndex = 26;
            this.lstVans.SelectedIndexChanged += new System.EventHandler(this.lstVans_SelectedIndexChanged);
            // 
            // fldConsumption
            // 
            this.fldConsumption.Location = new System.Drawing.Point(133, 177);
            this.fldConsumption.Name = "fldConsumption";
            this.fldConsumption.Size = new System.Drawing.Size(53, 20);
            this.fldConsumption.TabIndex = 27;
            this.fldConsumption.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblConsumption
            // 
            this.lblConsumption.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.lblConsumption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsumption.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblConsumption.Location = new System.Drawing.Point(33, 177);
            this.lblConsumption.Name = "lblConsumption";
            this.lblConsumption.Size = new System.Drawing.Size(94, 20);
            this.lblConsumption.TabIndex = 28;
            this.lblConsumption.Text = "Consumption";
            this.lblConsumption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // VanWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 296);
            this.Controls.Add(this.lblConsumption);
            this.Controls.Add(this.fldConsumption);
            this.Controls.Add(this.lstVans);
            this.Controls.Add(this.fldBadge);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.lblTittle);
            this.Controls.Add(this.lblVansList);
            this.Controls.Add(this.fldCapacity);
            this.Controls.Add(this.lblBadge);
            this.Controls.Add(this.lblCapacity);
            this.Name = "VanWindow";
            this.Text = "VanWindow";
            ((System.ComponentModel.ISupportInitialize)(this.fldCapacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldConsumption)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblBadge;
        private System.Windows.Forms.Label lblCapacity;
        private System.Windows.Forms.NumericUpDown fldCapacity;
        private System.Windows.Forms.Label lblVansList;
        private System.Windows.Forms.Label lblTittle;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.TextBox fldBadge;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ListBox lstVans;
        private System.Windows.Forms.NumericUpDown fldConsumption;
        private System.Windows.Forms.Label lblConsumption;
    }
}