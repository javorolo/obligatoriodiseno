﻿using School.Controller;
using System;
using System.Windows.Forms;

namespace UserInterface {
    public partial class WindowOfActionsOnSubject : Form {

        private WindowMainMenu mainMenu;

        public WindowOfActionsOnSubject(WindowMainMenu wm) {
            InitializeComponent();
            mainMenu = wm;
        }

        private void btnCreateSubject_Click(object sender, EventArgs e) {
            try {
                this.Hide();
                SubjectWindow subjectWindow = new SubjectWindow(mainMenu, "Create Subject");
                subjectWindow.Show();

            } catch (Exception ex) {
                Console.WriteLine("" + ex);
            }
        }

        private void btnModifySubject_Click(object sender, EventArgs e) {
            if (!ControllerSubject.Instance.IsEmptyList()) {
                SubjectWindow subjectWindow = new SubjectWindow(mainMenu, "Modify Subject");
                subjectWindow.Show();
                this.Hide();
            } else {
                MessageBox.Show("There aren't subjects in the system ");
            }
        }

        private void btnDeleteSubject_Click(object sender, EventArgs e) {
            if (!ControllerSubject.Instance.IsEmptyList()) {
                SubjectWindow subjectWindow = new SubjectWindow(mainMenu, "Delete Subject");
                subjectWindow.Show();
                this.Hide();
            } else {
                MessageBox.Show("There aren't subjects in the system ");
            }
        }

        private void btnReturn_Click(object sender, EventArgs e) {
            this.Dispose();
            mainMenu.Show();
        }
    }
}
