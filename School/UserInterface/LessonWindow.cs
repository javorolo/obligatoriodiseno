﻿using School.Controller;
using School.Logic;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using School.IControllers;

namespace UserInterface {

    public partial class LessonWindow : Form {

        private WindowOfActionsOnLessons windowActionsSubject;
        private IControllerLesson controllerLesson;
        private IControllerStudent controllerStudent;
        private IControllerTeacher controllerTeacher;
        private IControllerSubject controllerSubject;

        private List<Teacher> allTeachers;
        private List<Student> allStudents;
        private List<Subject> allSubjects;

        private List<Teacher> teachersInLesson = new List<Teacher>();
        private List<Student> studentsInLesson = new List<Student>();
        private List<Teacher> diferenceTeachers = new List<Teacher>();
        private List<Student> diferenceStudents = new List<Student>();
        private List<Subject> diferenceSubject = new List<Subject>();
        private List<Subject> subjectWithLesson = new List<Subject>();
        private Subject subjectInLesson = null;

        public LessonWindow(WindowOfActionsOnLessons windowSubject, string typeOfCall) {
            InitializeComponent();
            windowActionsSubject = windowSubject;
            controllerLesson = ControllerLesson.Instance;
            controllerStudent = ControllerStudent.Instance;
            controllerTeacher = ControllerTeacher.Instance;
            controllerSubject = ControllerSubject.Instance;
            allTeachers = controllerTeacher.GetTeacherTable();
            allStudents = controllerStudent.GetStudentTable();
            allSubjects = controllerSubject.GetSubjectTable();
            subjectWithLesson = controllerLesson.GetSubjectsWithLesson();
            diferenceStudents = StudentsWithoutLesson(studentsInLesson, allStudents);
            diferenceTeachers = TeachersWithoutLesson(teachersInLesson, allTeachers);
            diferenceSubject = SubjectsWithoutLesson(subjectWithLesson, allSubjects);
            LoadTextNames(typeOfCall);
            SetForms();
            LoadLists();
        }

        private void btnAction_Click(object sender, EventArgs e) {
            if (IsCreateCall()) {
                Lesson lesson = controllerLesson.NewLesson();
                SetLessonAttributes(lesson);
                AddLesson(lesson);
            } else if (IsModifyCall()) {
                Lesson oldLesson = lstLessons.SelectedItem as Lesson;
                Lesson changedLesson = controllerLesson.NewLesson();
                SetLessonAttributes(changedLesson);
                ModifyLesson(oldLesson, changedLesson);
            } else {
                Lesson toDelete = lstLessons.SelectedItem as Lesson;
                DeleteLesson(toDelete);
            }

        }
 

        private void btnBack_Click(object sender, EventArgs e) {
            CloseWindow();
        }

        private void SetLessonAttributes(Lesson lesson) {
            lesson.Students = studentsInLesson;
            lesson.Teachers = teachersInLesson;
            lesson.Subject = subjectInLesson;
        }

        private void AddLesson(Lesson lesson) {
            if (!controllerLesson.IsInLessonTable(lesson) && CorrectLesson(lesson)) {
                controllerLesson.AddLesson(lesson);
                ResetLists();
                LoadLists();
                CleanLessonsLists();
                MessageBox.Show("The lesson has been added into the system");
                if (NoAvailableSubjects()) {
                    CloseWindow();
                }
            }
        }

        private void ModifyLesson(Lesson oldLesson, Lesson changedLesson) {
            if (controllerLesson.IsInLessonTable(oldLesson) && CorrectLesson(changedLesson)) {
                controllerLesson.ModifyLesson(oldLesson, changedLesson);
                ResetLists();
                CleanLessonsLists();
                LoadLists();
                MessageBox.Show("The lesson has been changed into the system");
            }
        }

        private void DeleteLesson(Lesson lesson) {
            if (controllerLesson.IsInLessonTable(lesson)) {
                controllerLesson.DeleteLesson(lesson);
                ResetLists();
                LoadLists();
                MessageBox.Show("The lesson has been delete from the system");
                if (controllerLesson.IsEmptyTable()) {
                    CloseWindow();
                }
            }
        }

        private bool CorrectLesson(Lesson lesson) {
            bool ok = true;
            if (lesson.Subject == null) {
                MessageBox.Show("You must select a subject to create a lesson");
                ok = false;
            } else if (lesson.Students.Count == 0) {
                MessageBox.Show("You must select one student at least to create a lesson");
                ok = false;
            } else if (lesson.Teachers.Count == 0) {
                MessageBox.Show("You must select one teacher at least to create a lesson");
                ok = false;
            }
            return ok;
        }

        private void ResetLists() {
            teachersInLesson = new List<Teacher>();
            studentsInLesson = new List<Student>();
            subjectInLesson = null;
            diferenceStudents = StudentsWithoutLesson(studentsInLesson, allStudents);
            diferenceTeachers = TeachersWithoutLesson(teachersInLesson, allTeachers);
        }

        private void CleanLessonsLists() {
            lstStudentsInLesson.DataSource = null;
            lstTeachersLesson.DataSource = null;
            fldSubject.Text = "";
        }

        private void LoadTextNames(string typeOfCall) {
            btnAction.Text = typeOfCall;
            lblTittle.Text = typeOfCall;
        }

        private void LoadLists() {
            LoadStudentsList();
            LoadTeachersList();
            LoadSubjectsList();
            LoadLessonsList();
        }

        private void LoadStudentsList() {
            lstStudents.DataSource = null;
            lstStudents.DataSource = diferenceStudents;
        }

        private void LoadLessonsList() {
            lstLessons.DataSource = null;
            lstLessons.DataSource = controllerLesson.GetLessonList();
        }

        private void LoadTeachersList() {
            lstTeachers.DataSource = null;
            lstTeachers.DataSource = diferenceTeachers;
        }

        private void LoadSubjectsList() {
            lstSubjects.DataSource = null;
            lstSubjects.DataSource = diferenceSubject;
        }

        private void LoadStudentsLesson() {
            lstStudentsInLesson.DataSource = null;
            lstStudentsInLesson.DataSource = studentsInLesson;
        }

        private void LoadTeachersLesson() {
            lstTeachersLesson.DataSource = null;
            lstTeachersLesson.DataSource = teachersInLesson;
        }

        private void btnSelectSubject_Click(object sender, EventArgs e) {
            if(lstSubjects.SelectedItem as Subject != null) {
                ChangeSubjectInLesson();
                fldSubject.Text = subjectInLesson.ToString();
                LoadSubjectsList();
            } 
        }

        private void ChangeSubjectInLesson() {
            if (subjectInLesson == null) {
                subjectInLesson = lstSubjects.SelectedItem as Subject;
                diferenceSubject.Remove(subjectInLesson);
                subjectWithLesson.Add(subjectInLesson);
            } else {
                Subject toKeep = subjectInLesson;
                subjectInLesson = lstSubjects.SelectedItem as Subject;
                diferenceSubject.Add(toKeep);
                diferenceSubject.Remove(subjectInLesson);
                subjectWithLesson.Add(subjectInLesson);
                subjectWithLesson.Remove(toKeep);
            }
        }

        private void btnAddStudent_Click(object sender, EventArgs e) {
            if(lstStudents.SelectedItem as Student != null) {
                studentsInLesson.Add(lstStudents.SelectedItem as Student);
                diferenceStudents.Remove(lstStudents.SelectedItem as Student);
                LoadStudentsList();
                LoadStudentsLesson();
            }
        }

        private void btnRemoveStudent_Click(object sender, EventArgs e) {
            if(lstStudentsInLesson.SelectedItem as Student != null) {
                diferenceStudents.Add(lstStudentsInLesson.SelectedItem as Student);
                studentsInLesson.Remove(lstStudentsInLesson.SelectedItem as Student);
                LoadStudentsList();
                LoadStudentsLesson();
            } 
        }

        private void btnAddTeacher_Click(object sender, EventArgs e) {
            if (lstTeachers.SelectedItem as Teacher != null) {
                teachersInLesson.Add(lstTeachers.SelectedItem as Teacher);
                diferenceTeachers.Remove(lstTeachers.SelectedItem as Teacher);
                LoadTeachersList();
                LoadTeachersLesson();
            }
        }

        private void btnRemoveTeacher_Click(object sender, EventArgs e) {
            if (lstTeachersLesson.SelectedItem as Teacher != null) {
                diferenceTeachers.Add(lstTeachersLesson.SelectedItem as Teacher);
                teachersInLesson.Remove(lstTeachersLesson.SelectedItem as Teacher);
                LoadTeachersList();
                LoadTeachersLesson();
            }
        }

        private void lstLessons_SelectedIndexChanged(object sender, EventArgs e) {
            if (!IsCreateCall()) {
                LoadLessonData();
            }  
        }

        private void LoadLessonData() {
            if(lstLessons.SelectedItem != null && !IsCreateCall()) {
                Lesson lesson = lstLessons.SelectedItem as Lesson;
                fldCode.Text = lesson.Id.ToString();
                LoadLessonInformation(lesson);
            }
        }

        private void LoadLessonInformation(Lesson lesson) {
            LoadSubjectInALesson(lesson);
            LoadTeachersInALesson(lesson);
            LoadStudentsInALesson(lesson);
        }

        private void LoadTeachersInALesson(Lesson lesson) {
            lstTeachersLesson.DataSource = null;
            lstTeachers.DataSource = null;
            teachersInLesson = controllerLesson.GetTeachersLesson(lesson);
            lstTeachersLesson.DataSource = teachersInLesson;
            lstTeachers.DataSource = TeachersWithoutLesson(teachersInLesson, allTeachers);
        }

        private List<Teacher> TeachersWithoutLesson(List<Teacher> teachersInLesson, List<Teacher> allTeachers) {
            foreach (Teacher teacher in allTeachers) {
                if (diferenceTeachers.Contains(teacher)) {
                    diferenceTeachers.Remove(teacher);
                }
                if (!teachersInLesson.Contains(teacher) && !diferenceTeachers.Contains(teacher)) {
                    diferenceTeachers.Add(teacher);
                }
            }
            return diferenceTeachers;
        }

        private List<Subject> SubjectsWithoutLesson(List<Subject> subjectWithLesson, List<Subject> allSubjects) {
            foreach (Subject subject in allSubjects) {
                if (diferenceSubject.Contains(subject)) {
                    diferenceSubject.Remove(subject);
                }
                if (!subjectWithLesson.Contains(subject) && !diferenceSubject.Contains(subject)) {
                    diferenceSubject.Add(subject);
                }
            }
            return diferenceSubject;
        }

        private void LoadStudentsInALesson(Lesson lesson) {
            lstStudentsInLesson.DataSource = null;
            lstStudents.DataSource = null;
            studentsInLesson = controllerLesson.GetStudentsLesson(lesson);
            lstStudentsInLesson.DataSource = studentsInLesson;
            lstStudents.DataSource = StudentsWithoutLesson(studentsInLesson, allStudents);
        }

        private List<Student> StudentsWithoutLesson(List<Student> studentsInLesson, List<Student> allStudents) {
            foreach (Student student in allStudents) {
                if (diferenceStudents.Contains(student)) {
                    diferenceStudents.Remove(student);
                }
                if (!studentsInLesson.Contains(student) && !diferenceStudents.Contains(student)) {
                    diferenceStudents.Add(student);
                }
            }
            return diferenceStudents;
        }

        private void LoadSubjectInALesson(Lesson lesson) {
            fldSubject.Text = null;
            fldCode.Text = null;
            lstSubjects.DataSource = null;
            subjectInLesson = controllerLesson.GetSubjectLesson(lesson);
            fldSubject.Text = subjectInLesson.ToString();
            fldCode.Text = lesson.Id.ToString();
            lstSubjects.DataSource = SubjectsWithoutLesson(subjectWithLesson, allSubjects);
        }

        private bool IsCreateCall() {
            return btnAction.Text == "Create Lesson";
        }

        private bool IsModifyCall() {
            return btnAction.Text == "Modify Lesson";
        }

        private bool IsDeleteCall() {
            return btnAction.Text == "Delete Lesson";
        }

        private void SetForms() {
            if (IsCreateCall()) {
                lstLessons.Enabled = false;
            } else if (IsDeleteCall()) {
                btnAddStudent.Enabled = false;
                btnAddTeacher.Enabled = false;
                btnRemoveStudent.Enabled = false;
                btnRemoveTeacher.Enabled = false;
                btnSelectSubject.Enabled = false;
            }
        }

        private bool NoAvailableSubjects() {
            return controllerLesson.GetSubjectsWithLesson().Count ==
                controllerSubject.GetSubjectTable().Count;
        }

        private void CloseWindow() {
            this.Hide();
            WindowOfActionsOnLessons window = windowActionsSubject;
            window.Show();
        }
    }
}