﻿namespace UserInterface {
    partial class TeacherWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTittle = new System.Windows.Forms.Label();
            this.lstTeachers = new System.Windows.Forms.ListBox();
            this.lblTeachers = new System.Windows.Forms.Label();
            this.fldName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.fldLastName = new System.Windows.Forms.TextBox();
            this.lblNumber = new System.Windows.Forms.Label();
            this.fldNumber = new System.Windows.Forms.TextBox();
            this.lblTeacherSubjects = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnAction = new System.Windows.Forms.Button();
            this.lstTeacherSubjects = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblTittle
            // 
            this.lblTittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTittle.Location = new System.Drawing.Point(62, 9);
            this.lblTittle.Name = "lblTittle";
            this.lblTittle.Size = new System.Drawing.Size(558, 53);
            this.lblTittle.TabIndex = 0;
            this.lblTittle.Text = "Teacher";
            this.lblTittle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstTeachers
            // 
            this.lstTeachers.FormattingEnabled = true;
            this.lstTeachers.Location = new System.Drawing.Point(505, 99);
            this.lstTeachers.Name = "lstTeachers";
            this.lstTeachers.Size = new System.Drawing.Size(115, 225);
            this.lstTeachers.TabIndex = 1;
            this.lstTeachers.SelectedIndexChanged += new System.EventHandler(this.lstTeachers_SelectedIndexChanged);
            // 
            // lblTeachers
            // 
            this.lblTeachers.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeachers.Location = new System.Drawing.Point(505, 75);
            this.lblTeachers.Name = "lblTeachers";
            this.lblTeachers.Size = new System.Drawing.Size(115, 21);
            this.lblTeachers.TabIndex = 2;
            this.lblTeachers.Text = "Teachers List";
            this.lblTeachers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fldName
            // 
            this.fldName.Location = new System.Drawing.Point(192, 144);
            this.fldName.Name = "fldName";
            this.fldName.Size = new System.Drawing.Size(129, 20);
            this.fldName.TabIndex = 3;
            // 
            // lblName
            // 
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(53, 144);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(100, 20);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLastName
            // 
            this.lblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.Location = new System.Drawing.Point(53, 189);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(100, 20);
            this.lblLastName.TabIndex = 5;
            this.lblLastName.Text = "Last Name";
            this.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fldLastName
            // 
            this.fldLastName.Location = new System.Drawing.Point(192, 189);
            this.fldLastName.Name = "fldLastName";
            this.fldLastName.Size = new System.Drawing.Size(129, 20);
            this.fldLastName.TabIndex = 6;
            // 
            // lblNumber
            // 
            this.lblNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumber.Location = new System.Drawing.Point(53, 99);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(100, 20);
            this.lblNumber.TabIndex = 7;
            this.lblNumber.Text = "Number";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fldNumber
            // 
            this.fldNumber.Location = new System.Drawing.Point(192, 99);
            this.fldNumber.Name = "fldNumber";
            this.fldNumber.ReadOnly = true;
            this.fldNumber.Size = new System.Drawing.Size(129, 20);
            this.fldNumber.TabIndex = 10;
            // 
            // lblTeacherSubjects
            // 
            this.lblTeacherSubjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeacherSubjects.Location = new System.Drawing.Point(380, 62);
            this.lblTeacherSubjects.Name = "lblTeacherSubjects";
            this.lblTeacherSubjects.Size = new System.Drawing.Size(105, 34);
            this.lblTeacherSubjects.TabIndex = 12;
            this.lblTeacherSubjects.Text = "Teacher Subjects";
            this.lblTeacherSubjects.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(56, 301);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(94, 23);
            this.btnBack.TabIndex = 13;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnAction
            // 
            this.btnAction.Location = new System.Drawing.Point(227, 301);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(94, 23);
            this.btnAction.TabIndex = 14;
            this.btnAction.Text = "Action";
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // lstTeacherSubjects
            // 
            this.lstTeacherSubjects.FormattingEnabled = true;
            this.lstTeacherSubjects.Location = new System.Drawing.Point(380, 99);
            this.lstTeacherSubjects.Name = "lstTeacherSubjects";
            this.lstTeacherSubjects.Size = new System.Drawing.Size(105, 225);
            this.lstTeacherSubjects.TabIndex = 15;
            // 
            // TeacherWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 364);
            this.Controls.Add(this.lstTeacherSubjects);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.lblTeacherSubjects);
            this.Controls.Add(this.fldNumber);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.fldLastName);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.fldName);
            this.Controls.Add(this.lblTeachers);
            this.Controls.Add(this.lstTeachers);
            this.Controls.Add(this.lblTittle);
            this.Name = "TeacherWindow";
            this.Text = "TeacherWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTittle;
        private System.Windows.Forms.ListBox lstTeachers;
        private System.Windows.Forms.Label lblTeachers;
        private System.Windows.Forms.TextBox fldName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.TextBox fldLastName;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.TextBox fldNumber;
        private System.Windows.Forms.Label lblTeacherSubjects;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.ListBox lstTeacherSubjects;
    }
}