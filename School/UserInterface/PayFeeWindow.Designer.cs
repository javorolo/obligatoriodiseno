﻿namespace UserInterface {
    partial class PayFeeWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lstStudents = new System.Windows.Forms.ListBox();
            this.lblMonth = new System.Windows.Forms.Label();
            this.fldMonth = new System.Windows.Forms.TextBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.fldYear = new System.Windows.Forms.TextBox();
            this.btnPay = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.lblStudentsList = new System.Windows.Forms.Label();
            this.lblTittle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lstStudents
            // 
            this.lstStudents.FormattingEnabled = true;
            this.lstStudents.Location = new System.Drawing.Point(20, 93);
            this.lstStudents.Name = "lstStudents";
            this.lstStudents.Size = new System.Drawing.Size(120, 121);
            this.lstStudents.TabIndex = 0;
            this.lstStudents.SelectedIndexChanged += new System.EventHandler(this.LBstudents_SelectedIndexChanged);
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.Location = new System.Drawing.Point(189, 108);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(37, 13);
            this.lblMonth.TabIndex = 2;
            this.lblMonth.Text = "Month";
            // 
            // fldMonth
            // 
            this.fldMonth.Location = new System.Drawing.Point(232, 105);
            this.fldMonth.Name = "fldMonth";
            this.fldMonth.Size = new System.Drawing.Size(110, 20);
            this.fldMonth.TabIndex = 3;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(189, 154);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(29, 13);
            this.lblYear.TabIndex = 4;
            this.lblYear.Text = "Year";
            // 
            // fldYear
            // 
            this.fldYear.Location = new System.Drawing.Point(232, 151);
            this.fldYear.Name = "fldYear";
            this.fldYear.Size = new System.Drawing.Size(110, 20);
            this.fldYear.TabIndex = 5;
            // 
            // btnPay
            // 
            this.btnPay.Location = new System.Drawing.Point(232, 230);
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new System.Drawing.Size(110, 27);
            this.btnPay.TabIndex = 6;
            this.btnPay.Text = "Pay";
            this.btnPay.UseVisualStyleBackColor = true;
            this.btnPay.Click += new System.EventHandler(this.Bpay_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Location = new System.Drawing.Point(20, 230);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(104, 27);
            this.btnReturn.TabIndex = 7;
            this.btnReturn.Text = "Back";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.Breturn_Click);
            // 
            // lblStudentsList
            // 
            this.lblStudentsList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStudentsList.Location = new System.Drawing.Point(20, 64);
            this.lblStudentsList.Name = "lblStudentsList";
            this.lblStudentsList.Size = new System.Drawing.Size(120, 21);
            this.lblStudentsList.TabIndex = 20;
            this.lblStudentsList.Text = "Students List";
            this.lblStudentsList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTittle
            // 
            this.lblTittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTittle.Location = new System.Drawing.Point(18, 9);
            this.lblTittle.Name = "lblTittle";
            this.lblTittle.Size = new System.Drawing.Size(324, 55);
            this.lblTittle.TabIndex = 21;
            this.lblTittle.Text = "Student Pay Fee";
            this.lblTittle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PayFeeWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 282);
            this.Controls.Add(this.lblTittle);
            this.Controls.Add(this.lblStudentsList);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.btnPay);
            this.Controls.Add(this.fldYear);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.fldMonth);
            this.Controls.Add(this.lblMonth);
            this.Controls.Add(this.lstStudents);
            this.Name = "PayFeeWindow";
            this.Text = "PayFeeWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstStudents;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.TextBox fldMonth;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.TextBox fldYear;
        private System.Windows.Forms.Button btnPay;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Label lblStudentsList;
        private System.Windows.Forms.Label lblTittle;
    }
}