﻿namespace UserInterface {
    partial class WindowOfActionsOnSubject {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCreateSubject = new System.Windows.Forms.Button();
            this.btnModifySubject = new System.Windows.Forms.Button();
            this.btnDeleteSubject = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCreateSubject
            // 
            this.btnCreateSubject.Location = new System.Drawing.Point(78, 46);
            this.btnCreateSubject.Name = "btnCreateSubject";
            this.btnCreateSubject.Size = new System.Drawing.Size(132, 33);
            this.btnCreateSubject.TabIndex = 0;
            this.btnCreateSubject.Text = "Create Subject";
            this.btnCreateSubject.UseVisualStyleBackColor = true;
            this.btnCreateSubject.Click += new System.EventHandler(this.btnCreateSubject_Click);
            // 
            // btnModifySubject
            // 
            this.btnModifySubject.Location = new System.Drawing.Point(78, 110);
            this.btnModifySubject.Name = "btnModifySubject";
            this.btnModifySubject.Size = new System.Drawing.Size(132, 32);
            this.btnModifySubject.TabIndex = 1;
            this.btnModifySubject.Text = "Modify Subject";
            this.btnModifySubject.UseVisualStyleBackColor = true;
            this.btnModifySubject.Click += new System.EventHandler(this.btnModifySubject_Click);
            // 
            // btnDeleteSubject
            // 
            this.btnDeleteSubject.Location = new System.Drawing.Point(78, 166);
            this.btnDeleteSubject.Name = "btnDeleteSubject";
            this.btnDeleteSubject.Size = new System.Drawing.Size(132, 33);
            this.btnDeleteSubject.TabIndex = 2;
            this.btnDeleteSubject.Text = "Delete Subject";
            this.btnDeleteSubject.UseVisualStyleBackColor = true;
            this.btnDeleteSubject.Click += new System.EventHandler(this.btnDeleteSubject_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Location = new System.Drawing.Point(78, 229);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(132, 33);
            this.btnReturn.TabIndex = 3;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // WindowOfActionsOnSubject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 296);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.btnDeleteSubject);
            this.Controls.Add(this.btnModifySubject);
            this.Controls.Add(this.btnCreateSubject);
            this.Name = "WindowOfActionsOnSubject";
            this.Text = "WindowOfActionsOnSubject";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateSubject;
        private System.Windows.Forms.Button btnModifySubject;
        private System.Windows.Forms.Button btnDeleteSubject;
        private System.Windows.Forms.Button btnReturn;
    }
}