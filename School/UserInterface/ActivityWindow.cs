﻿using School.Controller;
using School.IControllers;
using School.Logic;
using System;
using System.Windows.Forms;

namespace UserInterface {

    public partial class ActivityWindow : Form {

        private WindowOfActionsOnActivities windowOfActionsOnActivities;
        private IControllerActivity controllerActivity;

        public ActivityWindow(WindowOfActionsOnActivities waa, string typeOfCall) {
            InitializeComponent();
            windowOfActionsOnActivities = waa;
            controllerActivity = ControllerActivity.Instance;
            LoadTextNames(typeOfCall);
            UpdateActivityData();
            ShowItems();
        }

        private void LoadTextNames(string typeOfCall) {
            this.lblTittle.Text = typeOfCall;
            this.btnAction.Text = typeOfCall;
        }

        private void button1_Click(object sender, EventArgs e) {
            this.Dispose();
            windowOfActionsOnActivities.Show();
        }

        private void BTaction_Click(object sender, EventArgs e) {
            string name = this.fldName.Text.ToUpper();
            int price = Decimal.ToInt32(this.fldPrice.Value);
            DateTime date = this.dateTimePicker.Value.Date;

            if (IsAddCall()) {
                Activity activity = controllerActivity.NewActivity(name,price,date);
                AddActivity(activity);
            }
            else if (IsModifyCall()) {
                Activity oldActivity = this.lstActivities.SelectedItem as Activity;
                Activity changedActivity = controllerActivity.NewActivity(name,price,date);
                ModifyActivity(oldActivity, changedActivity);
            }
            else if (IsDeleteCall()) {
                Activity activity = lstActivities.SelectedItem as Activity;
                DeleteActivity(activity);
            }
            UpdateActivityData();
        }

        private void CloseWindow() {
            this.Hide();
            windowOfActionsOnActivities.Show();
        }

        private void AddActivity(Activity activity) {
            if (CorrectActivity(activity)) {
                controllerActivity.AddActivity(activity);
                MessageBox.Show("The activity has been added into the system");
                ClearFields();
            }
        }

        private void ModifyActivity(Activity oldActivity, Activity changedActivity) {
            if (CorrectActivity(changedActivity)) {
                controllerActivity.ModifyActivity(oldActivity, changedActivity);
                MessageBox.Show("The activity has been changed");
                ClearFields();
            }
        }

        private void DeleteActivity(Activity activity) {
            controllerActivity.DeleteActivity(activity);
            MessageBox.Show("The activity has been deleted");
            if (controllerActivity.IsEmptyActivities()) {
                CloseWindow();
            }
        }


        private bool CorrectActivity(Activity activity) {
            bool ok = true;
            if (!controllerActivity.IsOkNameFormat(activity.Name)) {
                MessageBox.Show("The name of activity must be have more than 3 chars");
                this.fldName.Text = "";
                ok = false;
            }
            else if (!controllerActivity.IsCorrectPrice(activity.Price)) {
                MessageBox.Show("The price must be between 1 and 5000");
                this.fldPrice.Value = 0;
                ok = false;
            }
            else if (!controllerActivity.DateIsOk(activity.Date)) {
                MessageBox.Show("The activity date must be today, or a future date");
                this.dateTimePicker.Value = DateTime.Today;
                ok = false;
            }
            return ok;
        }

        private void ClearFields() {
            this.fldName.Text = "";
            this.fldPrice.Value = 0;
            this.dateTimePicker.Value = DateTime.Today;
        }

        private void LoadActivityData() {
            if (lstActivities.SelectedItem != null) {
                Activity activity = lstActivities.SelectedItem as Activity;
                this.fldName.Text = activity.Name;
                this.fldPrice.Value = activity.Price;
                this.dateTimePicker.Value = activity.Date;
            }
        }

        private void UpdateActivityData() {
            this.lstActivities.DataSource = null;
            this.lstActivities.DataSource = controllerActivity.GetActivitiesList();
        }

        private void ShowItems() {
            
            if (IsAddCall()) {
                lstActivities.Enabled = false;
                this.lstActivities.Enabled = false;
                ClearFields();
            }
            else if (IsModifyCall()) {
                UpdateActivityData(); 
            }
            else if (IsDeleteCall()) {
                UpdateActivityData();
                this.fldPrice.Enabled = false;
                this.dateTimePicker.Enabled = false;
                this.fldName.Enabled = false;
            }
        }

        private void LBactivitiesList_SelectedIndexChanged(object sender, EventArgs e) {
            if (!IsAddCall()) {
                LoadActivityData();
            } 
        }

        private bool IsAddCall() {
            return this.btnAction.Text.Equals("Create Activity");
        }

        private bool IsModifyCall() {
            return this.btnAction.Text.Equals("Modify Activity");
        }

        private bool IsDeleteCall() {
            return this.btnAction.Text.Equals("Delete Activity");
        }
    }
}
