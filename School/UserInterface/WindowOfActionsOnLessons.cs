﻿using System;
using System.Windows.Forms;
using School.Controller;

namespace UserInterface {
    public partial class WindowOfActionsOnLessons : Form {

        private WindowMainMenu windowMenu;

        public WindowOfActionsOnLessons(WindowMainMenu wm){
            InitializeComponent();
            windowMenu = wm;
        }

        private void btnCreateLesson_Click(object sender, EventArgs e) {
            if(AbleToCreate()) {
                this.Hide();
                LessonWindow lessonWindow = new LessonWindow(this, "Create Lesson");
                lessonWindow.Show();
            } else if (EmptyLists()) {
                MessageBox.Show("Before create a lesson, you must create a subject, a teacher and a student at least");
            } else if (!FreeSubject()) {
                MessageBox.Show("All subjects are already assigned to a lesson");
            }
        }

        private void btnModifyLesson_Click(object sender, EventArgs e) {
            if (!ControllerLesson.Instance.IsEmptyTable() && FreeSubject()) {
                LessonWindow lessonWindow = new LessonWindow
                     (this, "Modify Lesson");
                lessonWindow.Show();
                this.Hide();
            } else if (!FreeSubject()) {
                MessageBox.Show("All subjects are already assigned to a lesson");
            } else {
                MessageBox.Show("There aren't created lessons in the system to modify");
            }
        }

        private void btnDeleteLesson_Click(object sender, EventArgs e) {
            if (!ControllerLesson.Instance.IsEmptyTable()) {
                LessonWindow lessonWindow = new LessonWindow
                     (this, "Delete Lesson");
                lessonWindow.Show();
                this.Hide();
            } else {
                MessageBox.Show("There aren't lessons in the system to delete");
            }
        }

        private void btnReturn_Click(object sender, EventArgs e) {
            this.Dispose();
            windowMenu.Show();
        }

        private bool AbleToCreate() {
            return FreeSubject() && !EmptyLists();
        }

        private bool EmptyLists() {
            return ControllerStudent.Instance.IsEmptyList() || ControllerTeacher.Instance.IsEmptyTable()
                || ControllerSubject.Instance.IsEmptyList();
        }

        private bool FreeSubject() {
            return ControllerLesson.Instance.GetSubjectsWithLesson().Count <
                ControllerSubject.Instance.GetSubjectTable().Count;
        }

    }
}
