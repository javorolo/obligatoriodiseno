﻿using School.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface {
    public partial class WindowOfActionsOnActivities : Form {

        private WindowMainMenu mainMenu;

        public WindowOfActionsOnActivities(WindowMainMenu wm) {
            InitializeComponent();
            mainMenu = wm;
        }

        private void BTcreateActivity_Click(object sender, EventArgs e) {
            try {
                this.Hide();
                ActivityWindow activityWindow = new ActivityWindow(this, "Create Activity");
                activityWindow.Show();
            }
            catch (Exception ex) {
                Console.WriteLine("" + ex);
            }
        }

        private void BTmodifyActivity_Click(object sender, EventArgs e) {
            if (!ControllerActivity.Instance.IsEmptyActivities()) {
                this.Hide();
                ActivityWindow activityWindow = new ActivityWindow(this, "Modify Activity");
                activityWindow.Show();
            }
            else {
                MessageBox.Show("There aren't activities in the system ");
            }
        }

        private void BTdeleteActivity_Click(object sender, EventArgs e) {
            if (!ControllerActivity.Instance.IsEmptyActivities()) {
                this.Hide();
                ActivityWindow activityWindow = new ActivityWindow(this, "Delete Activity");
                activityWindow.Show();
            }
            else {
                MessageBox.Show("There aren't activities in the system ");
            }
        }

        private void BTreturn_Click(object sender, EventArgs e) {
            this.Dispose();
            this.mainMenu.Show();
        }
    }
}
