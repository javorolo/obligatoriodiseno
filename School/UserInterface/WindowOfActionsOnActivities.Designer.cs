﻿namespace UserInterface {
    partial class WindowOfActionsOnActivities {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.BTcreateActivity = new System.Windows.Forms.Button();
            this.BTmodifyActivity = new System.Windows.Forms.Button();
            this.BTdeleteActivity = new System.Windows.Forms.Button();
            this.BTreturn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BTcreateActivity
            // 
            this.BTcreateActivity.Location = new System.Drawing.Point(113, 59);
            this.BTcreateActivity.Name = "BTcreateActivity";
            this.BTcreateActivity.Size = new System.Drawing.Size(121, 36);
            this.BTcreateActivity.TabIndex = 0;
            this.BTcreateActivity.Text = "Create Activity";
            this.BTcreateActivity.UseVisualStyleBackColor = true;
            this.BTcreateActivity.Click += new System.EventHandler(this.BTcreateActivity_Click);
            // 
            // BTmodifyActivity
            // 
            this.BTmodifyActivity.Location = new System.Drawing.Point(113, 111);
            this.BTmodifyActivity.Name = "BTmodifyActivity";
            this.BTmodifyActivity.Size = new System.Drawing.Size(121, 36);
            this.BTmodifyActivity.TabIndex = 1;
            this.BTmodifyActivity.Text = "Modify Activity";
            this.BTmodifyActivity.UseVisualStyleBackColor = true;
            this.BTmodifyActivity.Click += new System.EventHandler(this.BTmodifyActivity_Click);
            // 
            // BTdeleteActivity
            // 
            this.BTdeleteActivity.Location = new System.Drawing.Point(113, 167);
            this.BTdeleteActivity.Name = "BTdeleteActivity";
            this.BTdeleteActivity.Size = new System.Drawing.Size(121, 36);
            this.BTdeleteActivity.TabIndex = 2;
            this.BTdeleteActivity.Text = "Delete Activity";
            this.BTdeleteActivity.UseVisualStyleBackColor = true;
            this.BTdeleteActivity.Click += new System.EventHandler(this.BTdeleteActivity_Click);
            // 
            // BTreturn
            // 
            this.BTreturn.Location = new System.Drawing.Point(113, 223);
            this.BTreturn.Name = "BTreturn";
            this.BTreturn.Size = new System.Drawing.Size(121, 36);
            this.BTreturn.TabIndex = 3;
            this.BTreturn.Text = "Return";
            this.BTreturn.UseVisualStyleBackColor = true;
            this.BTreturn.Click += new System.EventHandler(this.BTreturn_Click);
            // 
            // WindowOfActionsOnActivities
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 308);
            this.Controls.Add(this.BTreturn);
            this.Controls.Add(this.BTdeleteActivity);
            this.Controls.Add(this.BTmodifyActivity);
            this.Controls.Add(this.BTcreateActivity);
            this.Name = "WindowOfActionsOnActivities";
            this.Text = "WindowOfActionsOnActivities";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BTcreateActivity;
        private System.Windows.Forms.Button BTmodifyActivity;
        private System.Windows.Forms.Button BTdeleteActivity;
        private System.Windows.Forms.Button BTreturn;
    }
}