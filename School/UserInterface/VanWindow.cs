﻿using School.Controller;
using School.Logic;
using System;
using System.Windows.Forms;
using School.IControllers;

namespace UserInterface {

    public partial class VanWindow : Form {

        private IControllerVan vanController;
        private WindowMainMenu windowMainMenu;

        public VanWindow(WindowMainMenu wmenu, String typeOfCall) {
            InitializeComponent();
            vanController = ControllerVan.Instance;
            windowMainMenu = wmenu;
            LoadTextNames(typeOfCall);
            UpdateList();
            ShowItems();
        }

        private void btnBack_Click(object sender, EventArgs e) {
            CloseWindow();
        }

        private void btnAction_Click(object sender, EventArgs e) {
            string badge = fldBadge.Text.ToUpper();
            int capacity = (int)fldCapacity.Value;
            int consumption = (int)fldConsumption.Value;

            if (IsAddCall()) {
                Van van = vanController.NewVan(badge, capacity, consumption);
                AddVan(van);
            } else if (IsModifyCall()) {
                Van oldVan = lstVans.SelectedItem as Van;
                Van changedVan = vanController.NewVan(badge, capacity, consumption);
                ModifyVan(oldVan, changedVan);
            } else if (IsDeleteCall()) {
                Van van = lstVans.SelectedItem as Van;
                DeleteVan(van);
                if (vanController.IsEmptyTable()) {
                    CloseWindow();
                }
            }
            ClearFields();
            UpdateList();
        }

        private void AddVan(Van van) {
            if (CorrectVan(van, btnAction.Text)) {
                vanController.AddVan(van);
                MessageBox.Show("The van has been added into the system");
            }
        }

        private void ModifyVan(Van oldVan, Van changedVan) {
            if (CorrectVan(changedVan, btnAction.Text)) {
                vanController.ModifyVan(oldVan, changedVan);
                MessageBox.Show("The van has been changed");
            }
        }

        private void DeleteVan(Van van) {
            if (CorrectVan(van, btnAction.Text)) {
                vanController.DeleteVan(van);
                MessageBox.Show("The van has been deleted from the system");
            }
        }

        private bool CorrectVan(Van van, string option) {
            bool ok = true;
            if (!vanController.IsOKBadgeFormat(van)) {
                MessageBox.Show("The format of the badge must be 3 letters and 4 numbers");
                ok = false;
            } else if (!vanController.CapacityIsOk(van)) {
                MessageBox.Show("The capacity must be between 1 and 50");
                ok = false;
            } else if (IsAddCall() && vanController.IsInVanTable(van)) {
                MessageBox.Show("This van was already added to the list, please use another badge");
                ok = false;
            } else if (!vanController.ConsumptionIsOk(van)) {
                MessageBox.Show("The consumption mas be between 1 and 50");
                ok = false;
            }
            return ok;
        }

        private void ClearFields() {
            fldBadge.Text = "";
            fldCapacity.Value = 1;
            fldConsumption.Value = 1;
        }

        private void UpdateList() {
            this.lstVans.DataSource = null;
            lstVans.DataSource = vanController.GetListOfVans();
        }

        private void LoadTextNames(string typeOfCall) {
            btnAction.Text = typeOfCall;
            lblTittle.Text = typeOfCall;
        }

        private void LoadVanData() {
            if(lstVans.SelectedItem != null) {
                Van van = lstVans.SelectedItem as Van;
                fldBadge.Text = van.Badge;
                fldCapacity.Value = van.Capacity;
                fldConsumption.Value = van.Consumption;
            }  
        }

        private void ShowItems() {
            if (IsAddCall()) {
                lstVans.Enabled = false;
                ClearFields();
            } else if (IsModifyCall()) {
                LoadVanData();
            } else if (IsDeleteCall()) {
                LoadVanData();
                fldBadge.Enabled = false;
                fldCapacity.Enabled = false;
                fldConsumption.Enabled = false;
            }
        }

        private bool IsAddCall() {
            return btnAction.Text.Equals("Create Van");
        }

        private bool IsModifyCall() {
            return btnAction.Text.Equals("Modify Van");
        }

        private bool IsDeleteCall() {
            return btnAction.Text.Equals("Delete Van");
        }

        
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e) {
            fldBadge.Text = lstVans.SelectedItem.ToString();  
        }

        private void CloseWindow() {
            this.Hide();
            WindowOfActionsOnVans window = new WindowOfActionsOnVans(windowMainMenu);
            window.Show();
        }

        private void lstVans_SelectedIndexChanged(object sender, EventArgs e) {
            if (!IsAddCall()) {
                LoadVanData();
            }
        }
        
    }
}
