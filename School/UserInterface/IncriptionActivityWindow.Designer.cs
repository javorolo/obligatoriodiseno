﻿namespace UserInterface {
    partial class IncriptionActivityWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.LBstudents = new System.Windows.Forms.ListBox();
            this.LBStudentActivities = new System.Windows.Forms.ListBox();
            this.LBactivities = new System.Windows.Forms.ListBox();
            this.BTaddActivity = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BTreturn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LBstudents
            // 
            this.LBstudents.FormattingEnabled = true;
            this.LBstudents.Location = new System.Drawing.Point(38, 48);
            this.LBstudents.Name = "LBstudents";
            this.LBstudents.Size = new System.Drawing.Size(119, 186);
            this.LBstudents.TabIndex = 0;
            this.LBstudents.SelectedIndexChanged += new System.EventHandler(this.LBactivities_SelectedIndexChanged);
            // 
            // LBStudentActivities
            // 
            this.LBStudentActivities.FormattingEnabled = true;
            this.LBStudentActivities.Location = new System.Drawing.Point(178, 126);
            this.LBStudentActivities.Name = "LBStudentActivities";
            this.LBStudentActivities.Size = new System.Drawing.Size(105, 108);
            this.LBStudentActivities.TabIndex = 1;
            // 
            // LBactivities
            // 
            this.LBactivities.FormattingEnabled = true;
            this.LBactivities.Location = new System.Drawing.Point(315, 48);
            this.LBactivities.Name = "LBactivities";
            this.LBactivities.Size = new System.Drawing.Size(129, 186);
            this.LBactivities.TabIndex = 2;
            // 
            // BTaddActivity
            // 
            this.BTaddActivity.Location = new System.Drawing.Point(315, 254);
            this.BTaddActivity.Name = "BTaddActivity";
            this.BTaddActivity.Size = new System.Drawing.Size(129, 23);
            this.BTaddActivity.TabIndex = 3;
            this.BTaddActivity.Text = "Add Activity";
            this.BTaddActivity.UseVisualStyleBackColor = true;
            this.BTaddActivity.Click += new System.EventHandler(this.BTaddActivity_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Students";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(185, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Activities Student";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(357, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Activities";
            // 
            // BTreturn
            // 
            this.BTreturn.Location = new System.Drawing.Point(38, 254);
            this.BTreturn.Name = "BTreturn";
            this.BTreturn.Size = new System.Drawing.Size(75, 23);
            this.BTreturn.TabIndex = 7;
            this.BTreturn.Text = "Back";
            this.BTreturn.UseVisualStyleBackColor = true;
            this.BTreturn.Click += new System.EventHandler(this.BTreturn_Click);
            // 
            // IncriptionActivityWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 289);
            this.Controls.Add(this.BTreturn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BTaddActivity);
            this.Controls.Add(this.LBactivities);
            this.Controls.Add(this.LBStudentActivities);
            this.Controls.Add(this.LBstudents);
            this.Name = "IncriptionActivityWindow";
            this.Text = "IncriptionActivityWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LBstudents;
        private System.Windows.Forms.ListBox LBStudentActivities;
        private System.Windows.Forms.ListBox LBactivities;
        private System.Windows.Forms.Button BTaddActivity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BTreturn;
    }
}