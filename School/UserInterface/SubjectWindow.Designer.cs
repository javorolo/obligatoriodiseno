﻿namespace UserInterface {
    partial class SubjectWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lstSubject = new System.Windows.Forms.ListBox();
            this.fldName = new System.Windows.Forms.TextBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnAction = new System.Windows.Forms.Button();
            this.lblTittle = new System.Windows.Forms.Label();
            this.lblSubjectsList = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.fldCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lstSubject
            // 
            this.lstSubject.FormattingEnabled = true;
            this.lstSubject.Location = new System.Drawing.Point(263, 120);
            this.lstSubject.Name = "lstSubject";
            this.lstSubject.Size = new System.Drawing.Size(151, 173);
            this.lstSubject.TabIndex = 33;
            this.lstSubject.SelectedIndexChanged += new System.EventHandler(this.lstSubject_SelectedIndexChanged);
            // 
            // fldName
            // 
            this.fldName.Location = new System.Drawing.Point(142, 158);
            this.fldName.Name = "fldName";
            this.fldName.Size = new System.Drawing.Size(94, 20);
            this.fldName.TabIndex = 32;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(12, 270);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(94, 23);
            this.btnBack.TabIndex = 31;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnAction
            // 
            this.btnAction.Location = new System.Drawing.Point(142, 270);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(94, 23);
            this.btnAction.TabIndex = 30;
            this.btnAction.Text = "Action";
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // lblTittle
            // 
            this.lblTittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTittle.Location = new System.Drawing.Point(12, 31);
            this.lblTittle.Name = "lblTittle";
            this.lblTittle.Size = new System.Drawing.Size(402, 55);
            this.lblTittle.TabIndex = 29;
            this.lblTittle.Text = "Subject";
            this.lblTittle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSubjectsList
            // 
            this.lblSubjectsList.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubjectsList.Location = new System.Drawing.Point(263, 96);
            this.lblSubjectsList.Name = "lblSubjectsList";
            this.lblSubjectsList.Size = new System.Drawing.Size(151, 21);
            this.lblSubjectsList.TabIndex = 28;
            this.lblSubjectsList.Text = "Subjects List";
            this.lblSubjectsList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblName
            // 
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblName.Location = new System.Drawing.Point(14, 158);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(94, 20);
            this.lblName.TabIndex = 27;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fldCode
            // 
            this.fldCode.Location = new System.Drawing.Point(142, 120);
            this.fldCode.Name = "fldCode";
            this.fldCode.ReadOnly = true;
            this.fldCode.Size = new System.Drawing.Size(94, 20);
            this.fldCode.TabIndex = 35;
            // 
            // lblCode
            // 
            this.lblCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCode.Location = new System.Drawing.Point(14, 120);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(94, 20);
            this.lblCode.TabIndex = 34;
            this.lblCode.Text = "Code";
            this.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SubjectWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 325);
            this.Controls.Add(this.fldCode);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.lstSubject);
            this.Controls.Add(this.fldName);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnAction);
            this.Controls.Add(this.lblTittle);
            this.Controls.Add(this.lblSubjectsList);
            this.Controls.Add(this.lblName);
            this.Name = "SubjectWindow";
            this.Text = "SubjectWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstSubject;
        private System.Windows.Forms.TextBox fldName;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.Label lblTittle;
        private System.Windows.Forms.Label lblSubjectsList;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox fldCode;
        private System.Windows.Forms.Label lblCode;
    }
}