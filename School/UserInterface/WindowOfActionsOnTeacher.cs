﻿using School.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface {

    public partial class WindowOfActionsOnTeacher : Form {

        private WindowMainMenu mainMenu;

        public WindowOfActionsOnTeacher(WindowMainMenu mainMenu) {
            InitializeComponent();
            this.mainMenu = mainMenu;
        }

        private void btnCreate_Click(object sender, EventArgs e) {
            try {
                this.Hide();
                TeacherWindow teacherWindow = new TeacherWindow(this, "Create Teacher");
                teacherWindow.Show();
           } catch (Exception ex) {
                Console.WriteLine("" + ex);
            }
        }

        private void btnModify_Click(object sender, EventArgs e) {
            if (!ControllerTeacher.Instance.IsEmptyTable()) {
                TeacherWindow teacherWindow = new TeacherWindow(this, "Modify Teacher");
                teacherWindow.Show();
                this.Hide();
            } else {
                MessageBox.Show("There aren't teachers in the system ");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e) {
            if (!ControllerTeacher.Instance.IsEmptyTable()) {
                TeacherWindow teacherWindow = new TeacherWindow(this, "Delete Teacher");
                teacherWindow.Show();
                this.Hide();
            } else {
                MessageBox.Show("There aren't teachers in the system ");
            }
        }

        private void btnReturn_Click(object sender, EventArgs e) {
            this.Dispose();
            mainMenu.Show();
        }

        
    }
}
