﻿using School.Controller;
using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

namespace UserInterface {
    public partial class RoadsWindow : Form {
        
        private WindowMainMenu mainMenu;

        public RoadsWindow(WindowMainMenu aMainMenu) {
            InitializeComponent();
            mainMenu = aMainMenu; 
            DataTable data = ControllerRoad.Instance.ShortestRoute();
            this.dataGridView1.DataSource = data;
            this.CBorder.Text = "Students/Consumption";
            this.dataGridView1.Sort(this.dataGridView1.Columns[2],
                                    ListSortDirection.Ascending);
            this.dataGridView1.Enabled = false;
        }

        private void BTreturn_Click(object sender, EventArgs e) {
            this.Dispose();
            mainMenu.Show();
        }

        private void BTorden_Click(object sender, EventArgs e) {
            this.dataGridView1.Sort(this.dataGridView1.Columns[3], ListSortDirection.Ascending);
        }

        private void CBorder_SelectedIndexChanged(object sender, EventArgs e) {
            if (CBorder.Text == "Ascendant distance")
                this.dataGridView1.Sort(this.dataGridView1.Columns[3], ListSortDirection.Ascending);
            if (CBorder.Text == "Descendant distance")
                this.dataGridView1.Sort(this.dataGridView1.Columns[3], ListSortDirection.Descending);
            if (CBorder.Text == "Ascendant travels made")
                this.dataGridView1.Sort(this.dataGridView1.Columns[5], ListSortDirection.Ascending);
            if (CBorder.Text == "Descendant travels made")
                this.dataGridView1.Sort(this.dataGridView1.Columns[5], ListSortDirection.Descending);
        }

        private void RoadsWindow_Load(object sender, EventArgs e) {

        }
    }
}
