﻿namespace UserInterface {
    partial class WindowOfActionsOnLessons {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCreateLesson = new System.Windows.Forms.Button();
            this.btnModifyLesson = new System.Windows.Forms.Button();
            this.btnDeleteLesson = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCreateLesson
            // 
            this.btnCreateLesson.Location = new System.Drawing.Point(104, 63);
            this.btnCreateLesson.Name = "btnCreateLesson";
            this.btnCreateLesson.Size = new System.Drawing.Size(132, 34);
            this.btnCreateLesson.TabIndex = 0;
            this.btnCreateLesson.Text = "Create Lesson";
            this.btnCreateLesson.UseVisualStyleBackColor = true;
            this.btnCreateLesson.Click += new System.EventHandler(this.btnCreateLesson_Click);
            // 
            // btnModifyLesson
            // 
            this.btnModifyLesson.Location = new System.Drawing.Point(104, 124);
            this.btnModifyLesson.Name = "btnModifyLesson";
            this.btnModifyLesson.Size = new System.Drawing.Size(132, 34);
            this.btnModifyLesson.TabIndex = 1;
            this.btnModifyLesson.Text = "Modify Lesson";
            this.btnModifyLesson.UseVisualStyleBackColor = true;
            this.btnModifyLesson.Click += new System.EventHandler(this.btnModifyLesson_Click);
            // 
            // btnDeleteLesson
            // 
            this.btnDeleteLesson.Location = new System.Drawing.Point(104, 190);
            this.btnDeleteLesson.Name = "btnDeleteLesson";
            this.btnDeleteLesson.Size = new System.Drawing.Size(132, 34);
            this.btnDeleteLesson.TabIndex = 2;
            this.btnDeleteLesson.Text = "Delete Lesson";
            this.btnDeleteLesson.UseVisualStyleBackColor = true;
            this.btnDeleteLesson.Click += new System.EventHandler(this.btnDeleteLesson_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Location = new System.Drawing.Point(104, 253);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(132, 34);
            this.btnReturn.TabIndex = 3;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // WindowOfActionsOnLessons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 323);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.btnDeleteLesson);
            this.Controls.Add(this.btnModifyLesson);
            this.Controls.Add(this.btnCreateLesson);
            this.Name = "WindowOfActionsOnLessons";
            this.Text = "WindowOfActionsOnSubjects";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateLesson;
        private System.Windows.Forms.Button btnModifyLesson;
        private System.Windows.Forms.Button btnDeleteLesson;
        private System.Windows.Forms.Button btnReturn;
    }
}