﻿namespace UserInterface {
    partial class WindowOfActionsOnVans {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.buttonAddVan = new System.Windows.Forms.Button();
            this.buttonModifyVan = new System.Windows.Forms.Button();
            this.buttonDeleteVan = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonAddVan
            // 
            this.buttonAddVan.Location = new System.Drawing.Point(62, 38);
            this.buttonAddVan.Name = "buttonAddVan";
            this.buttonAddVan.Size = new System.Drawing.Size(181, 35);
            this.buttonAddVan.TabIndex = 0;
            this.buttonAddVan.Text = "Create Van";
            this.buttonAddVan.UseVisualStyleBackColor = true;
            this.buttonAddVan.Click += new System.EventHandler(this.buttonAddVan_Click);
            // 
            // buttonModifyVan
            // 
            this.buttonModifyVan.Location = new System.Drawing.Point(62, 104);
            this.buttonModifyVan.Name = "buttonModifyVan";
            this.buttonModifyVan.Size = new System.Drawing.Size(181, 34);
            this.buttonModifyVan.TabIndex = 1;
            this.buttonModifyVan.Text = "Modify Van";
            this.buttonModifyVan.UseVisualStyleBackColor = true;
            this.buttonModifyVan.Click += new System.EventHandler(this.buttonModifyVan_Click);
            // 
            // buttonDeleteVan
            // 
            this.buttonDeleteVan.Location = new System.Drawing.Point(62, 167);
            this.buttonDeleteVan.Name = "buttonDeleteVan";
            this.buttonDeleteVan.Size = new System.Drawing.Size(181, 33);
            this.buttonDeleteVan.TabIndex = 2;
            this.buttonDeleteVan.Text = "Delete Van";
            this.buttonDeleteVan.UseVisualStyleBackColor = true;
            this.buttonDeleteVan.Click += new System.EventHandler(this.buttonDeleteVan_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Location = new System.Drawing.Point(62, 225);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(181, 32);
            this.btnReturn.TabIndex = 3;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.BTreturnToMainMenu_Click);
            // 
            // WindowOfActionsOnVans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 281);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.buttonDeleteVan);
            this.Controls.Add(this.buttonModifyVan);
            this.Controls.Add(this.buttonAddVan);
            this.Name = "WindowOfActionsOnVans";
            this.Text = "WindowOfActionsOnVans";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAddVan;
        private System.Windows.Forms.Button buttonModifyVan;
        private System.Windows.Forms.Button buttonDeleteVan;
        private System.Windows.Forms.Button btnReturn;
    }
}