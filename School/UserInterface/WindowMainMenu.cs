﻿using School.Controller;
using System;
using System.Windows.Forms;

namespace UserInterface {

    public partial class WindowMainMenu : Form {

        public WindowMainMenu() {
            InitializeComponent();
        }

        private void BTmanageStudents_Click(object sender, EventArgs e) {
            this.Hide();
            WindowOfActionsOnStudents window = new WindowOfActionsOnStudents(this);
            window.Show();
        }

        private void BTmanageTeachers_Click(object sender, EventArgs e) {
            this.Hide();
            WindowOfActionsOnTeacher window = new WindowOfActionsOnTeacher(this);
            window.Show();
        }

        private void btnManageLessons_Click(object sender, EventArgs e) {
            this.Hide();
            WindowOfActionsOnLessons window = new WindowOfActionsOnLessons(this);
            window.Show();
        }

        private void BTmanageVans_Click(object sender, EventArgs e) {
            this.Hide();
            WindowOfActionsOnVans windowOfActionsOnVans = new WindowOfActionsOnVans(this);
            windowOfActionsOnVans.Show();
        }

        private void btnManageSubjects_Click(object sender, EventArgs e) {
            this.Hide();
            WindowOfActionsOnSubject windowOfActionsOnSubject = new WindowOfActionsOnSubject(this);
            windowOfActionsOnSubject.Show();
        }

        private void BTactivities_Click(object sender, EventArgs e) {
            this.Hide();
            WindowOfActionsOnActivities windowOfActionsOnActivities = new WindowOfActionsOnActivities(this);
            windowOfActionsOnActivities.Show();
        }

        private void BTroads_Click(object sender, EventArgs e) {
            if (!ControllerVan.Instance.IsEmptyTable() && !ControllerStudent.Instance.IsEmptyList()) { 
            MessageBox.Show("This operation may take a few seconds or minutes depending on the number of students.", 
                "Processing", MessageBoxButtons.OK);
            RoadsWindow roadWindow = new RoadsWindow(this);
            this.Hide();
            roadWindow.Show();
            } else {
                MessageBox.Show("There are no students and vans in the system.", "", MessageBoxButtons.OK);
            }
        }   
    }
}
