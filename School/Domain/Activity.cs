﻿using System;

namespace School.Logic {

    public class Activity {

        const int maxNameLength = 20;
        const int minNameLength = 3;
        const int minPrice = 0;
        const int maxPrice = 10000;


        public DateTime Date { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }

        public Activity() {
            this.Date =new DateTime(1900, 1, 1);
            this.Name = "";
            this.Id = 0;
            this.Price = 0;
        }

        public bool CorrectName() {
            return this.Name.Length >= minNameLength && this.Name.Length < maxNameLength;
        }

        public bool CorrectPrice() {
            return this.Price > minPrice && this.Price < maxPrice;
        }

        public override bool Equals(object obj) {
            if (obj == null)
                return false;
            Activity activity = obj as Activity;
            if ((System.Object) activity == null)
                return false;
            return ((Activity)obj).Name.Equals(this.Name) && ((Activity)obj).Date.Equals(this.Date);
        }

        public override string ToString() {
            return this.Name;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

    }
}
