﻿using System;

namespace School.Logic {

    public class Subject {
        
        public int Id { get; set; }
        public String Name { get; set; }
        public String Code { get; set; }

        public Subject() {
            this.Name = String.Empty;
            this.Code = String.Empty;
        }

        public override bool Equals(object obj) {
            if (obj == null)
                return false;

            Subject subject = obj as Subject;
            if ((System.Object)subject == null)
                return false;

            return ((Subject)obj).Code.Equals(this.Code);
        }

        public override string ToString() {
            return this.Name;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }
}
