﻿using System.Collections.Generic;

namespace School.Logic {

    public class Lesson {

        public virtual Subject Subject { get; set; }
        public virtual List<Student> Students { get; set; }
        public virtual List<Teacher> Teachers { get; set; }
        public int Id { get; set; }

        public Lesson() {
            Students = new List<Student>();
            Teachers = new List<Teacher>();
        }

        public override bool Equals(object obj) {
            if (obj == null)
                return false;
            Lesson lesson = obj as Lesson;
            if ((System.Object)lesson == null)
                return false;

            return ((Lesson)obj).Id.Equals(this.Id);
        }

        public override string ToString() {
            return this.Id.ToString();
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

    }
}
