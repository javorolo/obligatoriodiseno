﻿using System.Collections.Generic;


namespace School.Logic {

    public class Student : Person{

        public int StudentId { get; set; }
        public string Identification { get; set; }
        public virtual List<Activity> Activities { get; set; }
        public virtual Location Location { get; set; }

        public Student(){
            Location = new Location();
            this.Activities = new List<Activity>();
            this.Name = "";
            this.LastName = "";
            this.Identification = "";
            Location.CartesianX = 0;
            Location.CartesianY = 0;
        }

        public override bool Equals(object obj) {
            if (obj == null)
                return false;
            Student student = obj as Student;
            if ((System.Object)student == null)
                return false;
            return ((Student)obj).Identification.Equals(this.Identification);
        }

        public override string ToString() {
            return Name + " " + LastName;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

    }
}
