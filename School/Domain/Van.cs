﻿using System;

namespace School.Logic {

    public class Van : IComparable<Van> {

        public int Id { get; set; }
        public int Capacity { get; set; }
        public string Badge { get; set; }
        public int Consumption { get; set; }

        public Van() {
            this.Capacity = 0;
            this.Badge = "";
            this.Id = 0;
            this.Consumption = 0;
        }

        public override string ToString() {
            return this.Badge;
        }

        public override bool Equals(object obj) {
            if (obj == null) 
                return false;
            Van van = obj as Van;
            if ((System.Object)van == null)
                return false;
            return ((Van)obj).Badge.Equals(this.Badge);
        }

       public int CompareTo(Van other) {
            double percentOther = other.Capacity / other.Consumption;
            double percentThis = this.Capacity / this.Consumption;
            return (int)percentThis -(int)percentOther;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }
}
