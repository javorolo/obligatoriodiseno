﻿using System;
using System.Text.RegularExpressions;

namespace School.Logic{

    public class Person{

        private const int MAXNAMESIZE = 25;
        private const int MINNAMESIZE = 2;

        public String Name { get; set; }
        public string LastName { get; set; }

        public Person(){
            this.Name = String.Empty;
            this.LastName = string.Empty;
        }

        public bool CorrectName(string name) {
            return NameBetween2And30Chars(name) && !NameWithNumbers(name);
        }

        private bool NameBetween2And30Chars(string name) {
            return name.Length >= MINNAMESIZE && name.Length <= MAXNAMESIZE;
        }

        private bool NameWithNumbers(string name) {
            Regex r = new Regex("[0-9]", RegexOptions.IgnoreCase);
            Match m = r.Match(name);
            return m.Success;
        }
    }
}
