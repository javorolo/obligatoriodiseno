﻿
namespace School.Logic {

    public class Location {

        public int CartesianX { get; set; }
        public int CartesianY { get; set; }

        public Location() {
            this.CartesianX = 0;
            this.CartesianY = 0;
        }

        public override bool Equals(object location) {
            var compareLocation = location as Location;
            return compareLocation.CartesianX == this.CartesianX &&
                compareLocation.CartesianY == this.CartesianY;
        }

        public override int GetHashCode() {
            return CartesianX;
        }

    }
}
