﻿

namespace School.Logic {

    public class PayFee : Payment {

        public int Month { get; set; }
        public int Year { get; set; }

        public PayFee() {
            this.Month = 1;
            this.Year = 1990;
            this.Id = 1;
            this.Student = new Student();
        }


    }
}
