﻿

namespace School.Logic {

    public class Payment {

        public Student Student { get; set; } 
        public int Id { get; set; }

        public Payment() {
            this.Student = new Student();
        }

    }
}
