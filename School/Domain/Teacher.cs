﻿

namespace School.Logic {

    public class Teacher : Person{

        public int Id { get; set; }

        public Teacher(){
            this.Name = "";
            this.LastName = "";
            this.Id = 0;
        }

        public override string ToString() {
            return this.Name + " " + this.LastName;
        }

        public override bool Equals(object obj) {
            if (obj == null)
                return false;
            Teacher teacher = obj as Teacher;
            if ((System.Object)teacher == null)
                return false;
            return ((Teacher)obj).Id == this.Id;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

    }
}
